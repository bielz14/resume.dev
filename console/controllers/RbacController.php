<?php

namespace console\controllers;

use Yii;
use yii\console\Controller;
use common\components\rbac\UserRoleRule;

class RbacController extends Controller
{
    public function actionInit()
    {
        $auth = Yii::$app->authManager;

        $rule = new \common\components\rbac\AuthorRule;
        $auth->add($rule);

        $createPost = $auth->createPermission('createResume');
        $createPost->description = 'Create a resume';
        $auth->add($createPost);

        $updateResume = $auth->createPermission('updateResume');
        $updateResume->description = 'Update resume';
        $auth->add($updateResume);

        $updateOwnResume = $auth->createPermission('updateOwnResume');
        $updateOwnResume->description = 'Update own resume';
        $updateOwnPost->ruleName = $rule->name;
        $auth->add($updateOwnResume);

        $user = $auth->createRole('user');

        $admin = $auth->createRole('admin');
        $auth->add($admin);
        $auth->addChild($admin, $updateResume);
        $auth->addChild($admin, $user);

        $auth->assign($user, 1);

        //$auth->assign($user, 2);
        //$auth->assign($admin, 1);
    }
}