<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%contact_details}}`.
 */
class m190410_151941_create_reference_details_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%reference_details}}', [
            'id' => $this->primaryKey(),
            'company_name' => $this->string(),
            'address' => $this->string(),
            'phone_no' => $this->string(),
            'email' => $this->string(),
            'contact_person' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%reference_details}}');
    }
}
