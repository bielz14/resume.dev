<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%ship_security_officer}}`.
 */
class m190408_083651_create_ship_security_officer_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%ship_security_officer}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%ship_security_officer}}');
    }
}
