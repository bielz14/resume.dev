<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%specialised_projects}}`.
 */
class m190408_084317_create_specialised_projects_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%specialised_projects}}', [
            'id' => $this->primaryKey(),
            'exp_from' => $this->string(),
            'exp_to' => $this->string(),
            'comments' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%specialised_projects}}');
    }
}
