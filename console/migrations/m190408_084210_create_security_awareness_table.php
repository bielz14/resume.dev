<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%secuirity_awareness}}`.
 */
class m190408_084210_create_security_awareness_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%security_awareness}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%security_awareness}}');
    }
}
