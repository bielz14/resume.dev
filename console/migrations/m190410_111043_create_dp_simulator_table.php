<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%dp_simulator}}`.
 */
class m190410_111043_create_dp_simulator_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%dp_simulator}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%dp_simulator}}');
    }
}
