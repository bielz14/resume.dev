<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%oil_tanker}}`.
 */
class m190408_083429_create_oil_tanker_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%oil_tanker}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%oil_tanker}}');
    }
}
