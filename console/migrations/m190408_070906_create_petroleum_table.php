<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%petroleum}}`.
 */
class m190408_070906_create_petroleum_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%petroleum}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%petroleum}}');
    }
}
