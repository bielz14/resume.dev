<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%shore_experience}}`.
 */
class m190408_084349_create_shore_experience_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%shore_experience}}', [
            'id' => $this->primaryKey(),
            'exp_from' => $this->string(),
            'exp_to' => $this->string(),
            'comments' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%shore_experience}}');
    }
}
