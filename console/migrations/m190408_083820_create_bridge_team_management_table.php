<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bridge_team_management}}`.
 */
class m190408_083820_create_bridge_team_management_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bridge_team_management}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bridge_team_management}}');
    }
}
