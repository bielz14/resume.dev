<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%internal_auditors_course}}`.
 */
class m190408_084122_create_internal_auditors_course_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%internal_auditors_course}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%internal_auditors_course}}');
    }
}
