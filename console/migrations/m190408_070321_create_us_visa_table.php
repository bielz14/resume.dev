<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%us_visa}}`.
 */
class m190408_070321_create_us_visa_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%us_visa}}', [
            'id' => $this->primaryKey(),
            'document_no' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
            'place_of_issue' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%us_visa}}');
    }
}
