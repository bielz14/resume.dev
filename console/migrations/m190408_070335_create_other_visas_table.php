<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%other_visas}}`.
 */
class m190408_070335_create_other_visas_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%other_visas}}', [
            'id' => $this->primaryKey(),
            'document_no' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
            'place_of_issue' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%other_visas}}');
    }
}
