<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%electical_electronic}}`.
 */
class m190408_084230_create_electical_electronic_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%electical_electronic}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%electical_electronic}}');
    }
}
