<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%seaman_book}}`.
 */
class m190408_070152_create_seaman_book_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%seaman_book}}', [
            'id' => $this->primaryKey(),
            'document_no' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
            'place_of_issue' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%seaman_book}}');
    }
}
