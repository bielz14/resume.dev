<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%certificate_panamanian}}`.
 */
class m190408_073317_create_certificate_panamanian_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%certificate_panamanian}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%certificate_panamanian}}');
    }
}
