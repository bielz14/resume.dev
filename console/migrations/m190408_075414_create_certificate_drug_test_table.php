<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%certificate_drug_test}}`.
 */
class m190408_075414_create_certificate_drug_test_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%certificate_drug_test}}', [
            'id' => $this->primaryKey(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
            'iss_at' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%certificate_drug_test}}');
    }
}
