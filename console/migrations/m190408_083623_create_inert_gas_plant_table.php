<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%inert_gas_plant}}`.
 */
class m190408_083623_create_inert_gas_plant_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%inert_gas_plant}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%inert_gas_plant}}');
    }
}
