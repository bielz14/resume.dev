<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%bridge_engine_room_resource_management}}`.
 */
class m190408_084032_create_bridge_engine_room_resource_management_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%bridge_engine_room_resource_management}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%bridge_engine_room_resource_management}}');
    }
}
