<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%travel_passport}}`.
 */
class m190408_065755_create_travel_passport_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%travel_passport}}', [
            'id' => $this->primaryKey(),
            'document_no' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
            'place_of_issue' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%travel_passport}}');
    }
}
