<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%craft_rescue}}`.
 */
class m190408_083304_create_craft_rescue_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%craft_rescue}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%craft_rescue}}');
    }
}
