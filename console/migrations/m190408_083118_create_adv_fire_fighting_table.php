<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%adv_fire_fighting}}`.
 */
class m190408_083118_create_adv_fire_fighting_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%adv_fire_fighting}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%adv_fire_fighting}}');
    }
}
