<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%crude_oil_washing}}`.
 */
class m190408_083558_create_crude_oil_washing_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%crude_oil_washing}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%crude_oil_washing}}');
    }
}
