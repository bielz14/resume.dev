<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%medical_first_aid}}`.
 */
class m190408_083159_create_medical_first_aid_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%medical_first_aid}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%medical_first_aid}}');
    }
}
