<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%pers_safety_resp}}`.
 */
class m190408_083243_create_pers_safety_resp_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%pers_safety_resp}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%pers_safety_resp}}');
    }
}
