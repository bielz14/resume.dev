<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%certificate_international}}`.
 */
class m190408_073234_create_certificate_international_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%certificate_international}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%certificate_international}}');
    }
}
