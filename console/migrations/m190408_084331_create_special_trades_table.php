<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%special_trades}}`.
 */
class m190408_084331_create_special_trades_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%special_trades}}', [
            'id' => $this->primaryKey(),
            'exp_from' => $this->string(),
            'exp_to' => $this->string(),
            'comments' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%special_trades}}');
    }
}
