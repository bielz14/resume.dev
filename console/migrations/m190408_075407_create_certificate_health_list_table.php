<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%certificate_health_list}}`.
 */
class m190408_075407_create_certificate_health_list_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%certificate_health_list}}', [
            'id' => $this->primaryKey(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
            'iss_at' => $this->string()
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%certificate_health_list}}');
    }
}
