<?php

use yii\db\Migration;

/**
 * Handles the creation of table `{{%fast_rescue_craft}}`.
 */
class m190408_083317_create_fast_rescue_craft_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%fast_rescue_craft}}', [
            'id' => $this->primaryKey(),
            'number' => $this->string(),
            'iss_date' => $this->string(),
            'exp_date' => $this->string(),
            'iss_by' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('{{%fast_rescue_craft}}');
    }
}
