<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\Gas;

class GasForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
        ];
    }

    public function addGas()
    {        
        $gas = new Gas();
        if ($this->validate()) {
            $gas->number = $this->number[2];
            $gas->iss_date = $this->iss_date[7];
            $gas->exp_date = $this->exp_date[7];
            if ($gas->save()) {
                return $gas;
            }
        }

        return null;
    }

    public function updateGas()
    {        
        $gas = Gas::findById($this->id[8]);
        if ($this->validate()) {
            $gas->number = $this->number[2];
            $gas->iss_date = $this->iss_date[7];
            $gas->exp_date = $this->exp_date[7];
            if ($gas->save()) {
                return $gas;
            }
        }

        return null;
    }
}

