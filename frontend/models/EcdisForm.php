<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\Ecdis;

class EcdisForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addEcdis()
    {        
        $ecdis = new Ecdis();
        if ($this->validate()) {
            $ecdis->number = $this->number[19];
            $ecdis->iss_date = $this->iss_date[25];
            $ecdis->exp_date = $this->exp_date[25];
            $ecdis->iss_by = $this->iss_by[24];
            if ($ecdis->save()) {
                return $ecdis;
            }
        }

        return null;
    }

    public function updateEcdis()
    {        
        $ecdis = Ecdis::findById($this->id[27]);
        if ($this->validate()) {
            $ecdis->number = $this->number[18];
            $ecdis->iss_date = $this->iss_date[26];
            $ecdis->exp_date = $this->exp_date[26];
            $ecdis->iss_by = $this->iss_by[23];
            if ($ecdis->save()) {
                return $ecdis;
            }
        }

        return null;
    }
}

