<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\DpSimulator;

class DpSimulatorForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addDpsimulator()
    {        
        $dpsimulator = new DpSimulator();
        if ($this->validate()) {
            $dpsimulator->number = $this->number[34];
            $dpsimulator->iss_date = $this->iss_date[40];
            $dpsimulator->exp_date = $this->exp_date[40];
            $dpsimulator->iss_by = $this->iss_by[39];
            if ($dpsimulator->save()) {
                return $dpsimulator;
            }
        }

        return null;
    }

    public function updateDpsimulator()
    {        
        $dpsimulator = DpSimulator::findById($this->id[42]);
        if ($this->validate()) {
            $dpsimulator->number = $this->number[33];
            $dpsimulator->iss_date = $this->iss_date[41];
            $dpsimulator->exp_date = $this->exp_date[41];
            $dpsimulator->iss_by = $this->iss_by[38];
            if ($dpsimulator->save()) {
                return $dpsimulator;
            }
        }

        return null;
    }
}

