<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\ChemicalTanker;

class ChemicalTankerForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addChemicaltanker()
    {        
        $chemicaltanker = new ChemicalTanker();
        if ($this->validate()) {
            $chemicaltanker->number = $this->number[24];
            $chemicaltanker->iss_date = $this->iss_date[30];
            $chemicaltanker->exp_date = $this->exp_date[30];
            $chemicaltanker->iss_by = $this->iss_by[29];
            if ($chemicaltanker->save()) {
                return $chemicaltanker;
            }
        }

        return null;
    }

    public function updateChemicalTanker()
    {        
        $chemicaltanker = ChemicalTanker::findById($this->id[32]);
        if ($this->validate()) {
            $chemicaltanker->number = $this->number[23];
            $chemicaltanker->iss_date = $this->iss_date[31];
            $chemicaltanker->exp_date = $this->exp_date[31];
            $chemicaltanker->iss_by = $this->iss_by[28];
            if ($chemicaltanker->save()) {
                return $chemicaltanker;
            }
        }

        return null;
    }
}

