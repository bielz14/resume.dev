<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\IsmCode;

class IsmCodeForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addIsmcode()
    {        
        $ismcode = new IsmCode();
        if ($this->validate()) {
            $ismcode->number = $this->number[29];
            $ismcode->iss_date = $this->iss_date[35];
            $ismcode->exp_date = $this->exp_date[35];
            $ismcode->iss_by = $this->iss_by[34];
            if ($ismcode->save()) {
                return $ismcode;
            }
        }

        return null;
    }

    public function updateIsmcode()
    {        
        $ismcode = IsmCode::findById($this->id[37]);
        if ($this->validate()) {
            $ismcode->number = $this->number[28];
            $ismcode->iss_date = $this->iss_date[36];
            $ismcode->exp_date = $this->exp_date[36];
            $ismcode->iss_by = $this->iss_by[33];
            if ($ismcode->save()) {
                return $ismcode;
            }
        }

        return null;
    }
}

