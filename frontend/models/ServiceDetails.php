<?php
namespace frontend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Resume model
 *
 * @property integer $id
 * @property string $rank;
 * @property string $vessel_name;
 * @property string $signed_on;
 * @property string $signed_off;
 * @property string $period_in_months;
 * @property string $type_of_vessel;
 * @property string $dwt;
 * @property string $engine_type;
 * @property string $bhp;
 * @property string $kw;

 */
class ServiceDetails extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return '{{%service_details}}';
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    /*public function getResume()
    {
        return $this->hasMany(Resume::className(), ['id' => 'service_details_id']);
    }*/
}