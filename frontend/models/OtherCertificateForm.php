<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\Certificate;

class OtherCertificateForm extends Model
{
    public $other_certificate_name;
    public $other_certificate_number;
    public $other_certificate_iss_date;
    public $other_certificate_exp_date;
    public $other_certificate_iss_by;

    public function rules()
    {
        return [     
            ['other_certificate_name', 'each', 'rule' => ['string']],
            ['other_certificate_name', 'required'],
            
            ['other_certificate_number', 'each', 'rule' => ['string']],
            
            ['other_certificate_iss_date', 'each', 'rule' => ['string']],
            
            ['other_certificate_exp_date', 'each', 'rule' => ['string']],
            
            ['other_certificate_iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addOthercertificate($index)
    {        
        $certificate = new Certificate();
        if ($this->validate()) {
            $certificate->name = $this->other_certificate_name[$index];
            $certificate->number = $this->other_certificate_number[$index];
            $certificate->iss_date = $this->other_certificate_iss_date[$index];
            $certificate->exp_date = $this->other_certificate_exp_date[$index];
            $certificate->iss_by = $this->other_certificate_iss_by[$index];
            if ($certificate->save()) {
                return $certificate;
            }
        }

        return null;
    }

    public function updateOthercertificate($index, $id)
    {      
        $certificate = Certificate::findById($id);
        if ($this->validate()) {
            $certificate->name = $this->other_certificate_name[$index];
            $certificate->number = $this->other_certificate_number[$index];
            $certificate->iss_date = $this->other_certificate_iss_date[$index];
            $certificate->exp_date = $this->other_certificate_exp_date[$index];
            $certificate->iss_by = $this->other_certificate_iss_by[$index];
            if ($certificate->save()) {
                return $certificate;
            }
        }

        return null;
    }
}

