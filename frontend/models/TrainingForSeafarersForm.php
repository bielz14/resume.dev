<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\TrainingForSeafarers;

class TrainingForSeafarersForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addTrainingforseafarers()
    {        
        $trainingforseafarers = new TrainingForSeafarers();
        if ($this->validate()) {
            $trainingforseafarers->number = $this->number[38];
            $trainingforseafarers->iss_date = $this->iss_date[44];
            $trainingforseafarers->exp_date = $this->exp_date[44];
            $trainingforseafarers->iss_by = $this->iss_by[43];
            if ($trainingforseafarers->save()) {
                return $trainingforseafarers;
            }
        }

        return null;
    }

    public function updateTrainingforseafarers()
    {        
        $trainingforseafarers = TrainingForSeafarers::findById($this->id[46]);
        if ($this->validate()) {
            $trainingforseafarers->number = $this->number[37];
            $trainingforseafarers->iss_date = $this->iss_date[45];
            $trainingforseafarers->exp_date = $this->exp_date[45];
            $trainingforseafarers->iss_by = $this->iss_by[42];
            if ($trainingforseafarers->save()) {
                return $trainingforseafarers;
            }
        }

        return null;
    }
}

