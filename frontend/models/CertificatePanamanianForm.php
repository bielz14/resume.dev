<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\CertificatePanamanian;

class CertificatePanamanianForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addCertificatepanamanian()
    {        
        $certificatepanamanian = new CertificatePanamanian();
        if ($this->validate()) {
            $certificatepanamanian->number = $this->number[7];
            $certificatepanamanian->iss_date = $this->iss_date[10];
            $certificatepanamanian->exp_date = $this->exp_date[10];
            $certificatepanamanian->iss_by = $this->iss_by[9];
            if ($certificatepanamanian->save()) {
                return $certificatepanamanian;
            }
        }

        return null;
    }

    public function updateCertificatepanamanian()
    {        
        $certificatepanamanian = CertificatePanamanian::findById($this->id[12]);
        if ($this->validate()) {
            $certificatepanamanian->number = $this->number[6];
            $certificatepanamanian->iss_date = $this->iss_date[9];
            $certificatepanamanian->exp_date = $this->exp_date[9];
            $certificatepanamanian->iss_by = $this->iss_by[10];
            if ($certificatepanamanian->save()) {
                return $certificatepanamanian;
            }
        }

        return null;
    }
}

