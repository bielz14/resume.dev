<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\Petroleum;

class PetroleumForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
        ];
    }

    public function addPetroleum()
    {        
        $petroleum = new Petroleum();
        if ($this->validate()) {
            $petroleum->number = $this->number[0];
            $petroleum->iss_date = $this->iss_date[5];
            $petroleum->exp_date = $this->exp_date[5];
            if ($petroleum->save()) {
                return $petroleum;
            }
        }

        return null;
    }

    public function updatePetroleum()
    {        
        $petroleum = Petroleum::findById($this->id[6]);
        if ($this->validate()) {
            $petroleum->number = $this->number[0];
            $petroleum->iss_date = $this->iss_date[5];
            $petroleum->exp_date = $this->exp_date[5];
            if ($petroleum->save()) {
                return $petroleum;
            }
        }

        return null;
    }
}

