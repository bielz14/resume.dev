<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\Resume;
use frontend\models\CertificateForm;
use yii\web\UploadedFile;

class ResumeForm extends Model
{
    public $id;
    public $image;
    public $created_at;
    public $salary;
    public $application_for_position;
    public $other_position;
    public $surname;
    public $firstname;
    public $other_names;
    public $sex;
    public $date_of_birth;
    public $place_of_birth;
    public $citizenship;
    public $marital_status;
    public $color_of_eyes;
    public $color_of_hair;
    public $height;
    public $weight;
    public $boilersuit_size;
    public $boots_size;
    public $language;
    public $level;
    public $country;
    public $city;
    public $post_code;
    public $mobile;
    public $email;
    public $skype_name;
    public $telegram;
    public $viber;
    public $whatsapp;
    public $next_of_kin;
    public $kin_adress;
    public $kin_mobile;
    public $school;
    public $school_from;
    public $school_to;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
       
            [['image'], 'file', 'extensions' => 'png, jpg'],

            ['created_at', 'string'],
            
            ['salary', 'integer'],
            
            ['application_for_position', 'string'],
            ['application_for_position', 'required'],
            ['other_position', 'string'],

            ['surname', 'string'],
            ['surname', 'required'],
            ['firstname', 'string'],
            ['firstname', 'required'],
            ['other_names', 'string'],
            ['other_names', 'required'],
            ['sex', 'string'],
            ['sex', 'required'],
            ['date_of_birth', 'string'],
            ['date_of_birth', 'required'],
            ['place_of_birth', 'string'],
            ['place_of_birth', 'required'],
            ['citizenship', 'string'],
            ['citizenship', 'required'],
            ['marital_status', 'string'],

            ['color_of_eyes', 'string'],
            
            ['color_of_hair', 'string'],
            
            ['height', 'integer'],
            
            ['weight', 'integer'],
            
            ['boilersuit_size', 'integer'],
            
            ['boots_size', 'integer'],
            
            ['language', 'each', 'rule' => ['string']],
            ['language', 'required'],
            ['level', 'each', 'rule' => ['string']],
            ['level', 'required'],
            ['country', 'string'],
            
            ['city', 'string'],
            
            ['post_code', 'string'],
            
            ['mobile', 'string'],
            ['mobile', 'required'],
            ['email', 'each', 'rule' => ['email']],
            ['email', 'required'],
            ['skype_name', 'string'],
            ['skype_name', 'required'],

            ['telegram', 'boolean'],
            
            ['viber', 'boolean'],
            
            ['whatsapp', 'boolean'],
            
            ['next_of_kin', 'string'],

            ['kin_adress', 'string'],

            ['kin_mobile', 'string'],

            ['school', 'string'],
            
            ['school_from', 'string'],
            
            ['school_to', 'string'],
        ];
    }

    public function addResume($otherData)
    {        
        if ($this->validate()) {
            $resume = new Resume();
            $resume->owner_id = Yii::$app->user->identity->id;
            $resume->created_at = $this->created_at;
            $resume->salary = $this->salary;
            $resume->application_for_position = $this->application_for_position;
            $resume->other_position = $this->other_position;
            $resume->surname = $this->surname;
            $resume->firstname = $this->firstname;
            $resume->other_names = $this->other_names;
            if ($this->sex == '0') {
                $resume->sex = 'Male';
            } else {
                $resume->sex = $this->sex;
            }
            $resume->date_of_birth = $this->date_of_birth;
            $resume->place_of_birth = $this->place_of_birth;
            $resume->citizenship = $this->citizenship;
            $resume->marital_status = $this->marital_status;
            $resume->color_of_eyes = $this->color_of_eyes;
            $resume->color_of_hair = $this->color_of_hair;
            $resume->height = $this->height;
            $resume->weight = $this->weight;
            $resume->boilersuit_size = $this->boilersuit_size;
            $resume->boots_size = $this->boots_size;
            if ($this->language && $this->level) {
                $resume->language = '';
                $resume->level = '';
                foreach ($this->language as $value) {
                    $resume->language .= ':' . $value . ':';
                }
                foreach ($this->level as $value) {
                    $resume->level .= ':' . $value . ':';
                }
            }
            $resume->country = $this->country;
            $resume->city = $this->city;
            $resume->post_code = $this->post_code;
            $resume->mobile = $this->mobile;
            $resume->email = $this->email[0];
            $resume->skype_name = $this->skype_name;
            $resume->telegram = $this->telegram;
            $resume->viber = $this->viber;
            $resume->whatsapp = $this->whatsapp;
            $resume->next_of_kin = $this->next_of_kin;
            $resume->kin_adress = $this->kin_adress;
            $resume->kin_mobile = $this->kin_mobile;
            $resume->school = $this->school;
            $resume->school_from = $this->school_from;
            $resume->school_to = $this->school_to;

            $image = UploadedFile::getInstance($resume, 'image');
            if ($image) {
                $fileName = $image->baseName . '.' . $image->extension;
                $filePath = 'uploads/' . $fileName;
                $image->saveAs($filePath);
                if (file_exists($filePath)) {
                    $resume->image = $fileName;
                }
            }

            $travelpassport = new TravelPassportForm();
            if ($travelpassport->load(Yii::$app->request->post(), '') && $travelpassport->validate()) {
                if ($newTravelpassport = $travelpassport->addTravelpassport()) {
                    $resume->travel_passport_id = $newTravelpassport->id;
                }
            }

            $seamanbook = new SeamanBookForm();
            if ($seamanbook->load(Yii::$app->request->post(), '') && $seamanbook->validate()) {
                if ($newSeamanbook = $seamanbook->addSeamanbook()) {
                    $resume->seaman_book_id = $newSeamanbook->id;
                } 
            }
            
            $usvisa = new UsVisaForm();
            if ($usvisa->load(Yii::$app->request->post(), '') && $usvisa->validate()) {
                if ($newUsvisa = $usvisa->addUsvisa()) {
                    $resume->us_visa_id = $newUsvisa->id;
                } 
            }

            $othervisas = new OtherVisasForm();
            if ($othervisas->load(Yii::$app->request->post(), '') && $othervisas->validate()) {
                if ($newOthervisas = $othervisas->addOthervisas()) {
                    $resume->other_visas_id = $newOthervisas->id;
                } 
            }


            $otherseamanbook = new OtherSeamanbookForm();
            if ($otherseamanbook->load(Yii::$app->request->post(), '') && $otherseamanbook->validate()) {
                if ($newOtherseamanbook = $otherseamanbook->addOtherseamanbook()) {
                    $resume->other_seaman_book_ids .= ':' . $newOtherseamanbook->id . ':';
                } 
            }

            $quantityOtherOtherSeamanBook = count(Yii::$app->request->post()['other_otherseamanbook_document_no']);
            for ($i = 0; $i < $quantityOtherOtherSeamanBook; $i++) {
                $otherOtherSeamanBook = new OtherOtherSeamanbookForm();
                if ($otherOtherSeamanBook->load(Yii::$app->request->post(), '') && $otherOtherSeamanBook->validate()) 
                {
                    if ($newOtherseamanbook = $otherOtherSeamanBook->addOtherseamanbook($i)) {
                        $resume->other_seaman_book_ids .= ':' . $newOtherseamanbook->id . ':';
                    }           
                }
            }

            $petroleum = new PetroleumForm();
            if ($petroleum->load(Yii::$app->request->post(), '') && $petroleum->validate()) {
                if ($newPetroleum = $petroleum->addPetroleum()) {
                    $resume->petroleum_id = $newPetroleum->id;
                } 
            }

            $chemical = new ChemicalForm();
            if ($chemical->load(Yii::$app->request->post(), '') && $chemical->validate()) {
                if ($newChemical = $chemical->addChemical()) {
                    $resume->chemical_id = $newChemical->id;
                } 
            }

            $gas = new GasForm();
            if ($gas->load(Yii::$app->request->post(), '') && $gas->validate()) {
                if ($newGas = $gas->addGas()) {
                    $resume->gas_id = $newGas->id;
                } 
            }

            $certificate = new CertificateForm();
            if ($certificate->load(Yii::$app->request->post(), '') && $certificate->validate()) {
                if ($newCertificate = $certificate->addCertificate()) {
                    $resume->certificate_ids .= ':' . $newCertificate->id . ':';
                } 
            }

            $quantityOtherCertificates = count(Yii::$app->request->post()['other_certificate_name']);

            for ($i = 0; $i < $quantityOtherCertificates; $i++) {
                $otherCertificate = new OtherCertificateForm();
                if ($otherCertificate->load(Yii::$app->request->post(), '') && $otherCertificate->validate()) 
                {
                    if ($newCertificate = $otherCertificate->addOthercertificate($i)) {
                        $resume->certificate_ids .= ':' . $newCertificate->id . ':';
                    }
                }
            }

            $certificateinternational = new CertificateInternationalForm();
            if ($certificateinternational->load(Yii::$app->request->post(), '') && $certificateinternational->validate()) {         
                if ($newCertificateinternational = $certificateinternational->addCertificateinternational()) {
                    $resume->certificate_international_id = $newCertificateinternational->id;
                } 
            }

            $certificateliberian = new CertificateLiberianForm();
            if ($certificateliberian->load(Yii::$app->request->post(), '') && $certificateliberian->validate()) {
                if ($newCertificateliberian = $certificateliberian->addCertificateliberian()) {
                    $resume->certificate_liberian_id = $newCertificateliberian->id;
                }  
            }

            $certificatenorwegian = new CertificateNorwegianForm();
            if ($certificatenorwegian->load(Yii::$app->request->post(), '') && $certificatenorwegian->validate()) {
                if ($newCertificatenorwegian = $certificatenorwegian->addCertificatenorwegian()) {
                    $resume->certificate_norwegian_id = $newCertificatenorwegian->id;
                } 
            }

            $certificatepanamanian = new CertificatePanamanianForm();
            if ($certificatepanamanian->load(Yii::$app->request->post(), '') && $certificatepanamanian->validate()) {
                if ($newCertificatepanamanian = $certificatepanamanian->addCertificatepanamanian()) {
                    $resume->certificate_panamanian_id = $newCertificatepanamanian->id;
                } 
            }

            $certificateyellowfever = new CertificateYellowFeverForm();
            if ($certificateyellowfever->load(Yii::$app->request->post(), '') && $certificateyellowfever->validate()) {
                if ($newCertificateyellowfever = $certificateyellowfever->addCertificateyellowfever()) {
                    $resume->certificate_yellow_fever_id = $newCertificateyellowfever->id;
                } 
            }

            $certificatehealthlis = new CertificateHealthListForm();
            if ($certificatehealthlis->load(Yii::$app->request->post(), '') && $certificatehealthlis->validate()) {
                if ($newCertificatehealthlis = $certificatehealthlis->addCertificatehealthlist()) {
                    $resume->certificate_health_list_id = $newCertificatehealthlis->id;
                } 
            }

            $certificatedrugtest = new CertificateDrugTestForm();
            if ($certificatedrugtest->load(Yii::$app->request->post(), '') && $certificatedrugtest->validate()) {
                if ($newCertificatedrugtest = $certificatedrugtest->addCertificatedrugtest()) {
                    $resume->certificate_drug_test_id = $newCertificatedrugtest->id;
                } 
            }

            $traininginstructions = new TrainingInstructionsForm();
            if ($traininginstructions->load(Yii::$app->request->post(), '') && $traininginstructions->validate()) {
                if ($newTraininginstructions = $traininginstructions->addTraininginstructions()) {
                    $resume->training_instructions_id = $newTraininginstructions->id;
                } 
            }

            $basicfirefighting = new BasicFireFightingForm();
            if ($basicfirefighting->load(Yii::$app->request->post(), '') && $basicfirefighting->validate()) {
                if ($newBasicfirefighting = $basicfirefighting->addBasicfirefighting()) {
                    $resume->basic_fire_fighting_id = $newBasicfirefighting->id;
                } 
            }

            $advfirefighting = new AdvFireFightingForm();
            if ($advfirefighting->load(Yii::$app->request->post(), '') && $advfirefighting->validate()) {
                if ($newAdvfirefighting = $advfirefighting->addAdvfirefighting()) {
                    $resume->adv_fire_fighting_id = $newAdvfirefighting->id;
                } 
            }

            $elementaryfirstaid = new ElementaryFirstAidForm();
            if ($elementaryfirstaid->load(Yii::$app->request->post(), '') && $elementaryfirstaid->validate()) {
                if ($newElementaryfirstaid = $elementaryfirstaid->addElementaryfirstaid()) {
                    $resume->elementary_first_aid_id = $newElementaryfirstaid->id;
                } 
            }

            $medicalfirstaid = new MedicalFirstAidForm();
            if ($medicalfirstaid->load(Yii::$app->request->post(), '') && $medicalfirstaid->validate()) {
                if ($newMedicalfirstaid = $medicalfirstaid->addMedicalfirstaid()) {
                    $resume->medical_first_aid_id = $newMedicalfirstaid->id;
                } 
            }
            $medicalcare = new MedicalCareForm();
            if ($medicalcare->load(Yii::$app->request->post(), '') && $medicalcare->validate()) {
                if ($newMedicalcare = $medicalcare->addMedicalcare()) {
                    $resume->medical_care_id = $newMedicalcare->id;
                } 
            }

            $perssafetyresp = new PersSafetyRespForm();
            if ($perssafetyresp->load(Yii::$app->request->post(), '') && $perssafetyresp->validate()) {
                if ($newPerssafetyresp = $perssafetyresp->addPerssafetyresp()) {
                    $resume->pers_safety_resp_id = $newPerssafetyresp->id;
                } 
            }

            $craftrescue = new CraftRescueForm();
            if ($craftrescue->load(Yii::$app->request->post(), '') && $craftrescue->validate()) {
                if ($newCraftrescue = $craftrescue->addCraftrescue()) {
                    $resume->craft_rescue_id = $newCraftrescue->id;
                } 
            }

            $fastrescuecraft = new FastRescueCraftForm();
            if ($fastrescuecraft->load(Yii::$app->request->post(), '') && $fastrescuecraft->validate()) {
                if ($newFastrescuecraftm = $fastrescuecraft->addFastrescuecraft()) {
                    $resume->fast_rescuecraft_id = $newFastrescuecraft->id;
                } 
            }

            $gmdsss = new GmdsssForm();
            if ($gmdsss->load(Yii::$app->request->post(), '') && $gmdsss->validate()) {
                if ($newGmdsss = $gmdsss->addGmdsss()) {
                    $resume->gmdsss_id = $newGmdsss->id;
                } 
            }

            $managementlevel = new ManagementLevelForm();
            if ($managementlevel->load(Yii::$app->request->post(), '') && $managementlevel->validate()) {
                if ($newManagementlevel = $managementlevel->addManagementlevel()) {
                    $resume->management_level_id = $newManagementlevel->id;
                } 
            }

            $ecdis = new EcdisForm();
            if ($ecdis->load(Yii::$app->request->post(), '') && $ecdis->validate()) {
                if ($newEcdis = $ecdis->addEcdis()) {
                    $resume->ecdis_id = $newEcdis->id;
                } 
            }

            $radarobservation = new RadarObservationForm();
            if ($radarobservation->load(Yii::$app->request->post(), '') && $radarobservation->validate()) {
                if ($newRadarobservation = $radarobservation->addRadarobservation()) {
                    $resume->radar_observation_id = $newRadarobservation->id;
                } 
            }

            $hazmat = new HazmatForm();
            if ($hazmat->load(Yii::$app->request->post(), '') && $hazmat->validate()) {
                if ($newHazmat = $hazmat->addHazmat()) {
                    $resume->hazmat_id = $newHazmat->id;
                } 
            }

            $oiltanker = new OilTankerForm();
            if ($oiltanker->load(Yii::$app->request->post(), '') && $oiltanker->validate()) {
                if ($newOiltanker = $oiltanker->addOiltanker()) {
                    $resume->oil_tanker_id = $newOiltanker->id;
                } 
            }

            $advanceoiltanker = new AdvanceOilTankerForm();
            if ($advanceoiltanker->load(Yii::$app->request->post(), '') && $advanceoiltanker->validate()) {
                if ($newAdvanceoiltanker = $advanceoiltanker->addAdvanceoiltanker()) {
                    $resume->advance_oil_tanker_id = $newAdvanceoiltanker->id;
                } 
            }

            $chemicaltanker = new ChemicalTankerForm();
            if ($chemicaltanker->load(Yii::$app->request->post(), '') && $chemicaltanker->validate()) {
                if ($newChemicaltanker = $chemicaltanker->addChemicaltanker()) {
                    $resume->chemical_tanker_id = $newChemicaltanker->id;
                } 
            }

            $gastanker = new GasTankerForm();
            if ($gastanker->load(Yii::$app->request->post(), '') && $gastanker->validate()) {
                if ($newGastanker = $gastanker->addGastanker()) {
                    $resume->gas_tanker_id = $newGastanker->id;
                } 
            }

            $advancegastanker = new AdvanceGasTankerForm();
            if ($advancegastanker->load(Yii::$app->request->post(), '') && $advancegastanker->validate()) {
                if ($newAdvancegastanker = $advancegastanker->addAdvancegastanker()) {
                    $resume->advance_gas_tanker_id = $newAdvancegastanker->id;
                } 
            }

            $crudeoilwashing = new CrudeOilWashingForm();
            if ($crudeoilwashing->load(Yii::$app->request->post(), '') && $crudeoilwashing->validate()) {
                if ($newCrudeoilwashing = $crudeoilwashing->addCrudeoilwashing()) {
                    $resume->crude_oil_washing_id = $newCrudeoilwashing->id;
                } 
            }

            $inertgasplant = new InertGasPlantForm();
            if ($inertgasplant->load(Yii::$app->request->post(), '') && $inertgasplant->validate()) {
                if ($newInertgasplant = $inertgasplant->addInertgasplant()) {
                    $resume->inert_gas_plant_id = $newInertgasplant->id;
                } 
            }

            $ismcode = new IsmCodeForm();
            if ($ismcode->load(Yii::$app->request->post(), '') && $ismcode->validate()) {
                if ($newIsmcode = $ismcode->addIsmcode()) {
                    $resume->ism_code_id = $newIsmcode->id;
                } 
            }

            $shipsecurityofficer = new ShipSecurityOfficerForm();
            if ($shipsecurityofficer->load(Yii::$app->request->post(), '') && $shipsecurityofficer->validate()) {
                if ($newShipsecurityofficer = $shipsecurityofficer->addShipsecurityofficer()) {
                    $resume->ship_security_officer_id = $newShipsecurityofficer->id;
                } 
            }

            $shipsafetyofficer = new ShipSafetyOfficerForm();
            if ($shipsafetyofficer->load(Yii::$app->request->post(), '') && $shipsafetyofficer->validate()) {
                if ($newShipsafetyofficer = $shipsafetyofficer->addShipsafetyofficer()) {
                    $resume->ship_safety_officer_id = $newShipsafetyofficer->id;
                } 
            }

            $bridgeteammanagement = new BridgeTeamManagementForm();
            if ($bridgeteammanagement->load(Yii::$app->request->post(), '') && $bridgeteammanagement->validate()) {
                if ($newBridgeteammanagement = $bridgeteammanagement->addBridgeteammanagement()) {
                    $resume->bridge_team_management_id = $newBridgeteammanagement->id;
                } 
            }

            $dpinduction = new DpInductionForm();
            if ($dpinduction->load(Yii::$app->request->post(), '') && $dpinduction->validate()) {
                if ($newDpinduction = $dpinduction->addDpinduction()) {
                    $resume->dp_induction_id = $newDpinduction->id;
                } 
            }

            $dpsimulator = new DpSimulatorForm();
            if ($dpsimulator->load(Yii::$app->request->post(), '') && $dpsimulator->validate()) {
                if ($newDpsimulator = $dpsimulator->addDpsimulator()) {
                    $resume->dp_simulator_id = $newDpsimulator->id;
                } 
            }

            $bridgeengineroomresourcemanagement = new BridgeEngineRoomResourceManagementForm();
            if ($bridgeengineroomresourcemanagement->load(Yii::$app->request->post(), '') && $bridgeengineroomresourcemanagement->validate()) {
                if ($newBridgeengineroomresourcemanagement = $bridgeengineroomresourcemanagement->addBridgeengineroomresourcemanagement()) {
                    $resume->bridge_engine_room_resource_management_id = $newBridgeengineroomresourcemanagement->id;
                } 
            }

            $shiphandling = new ShipHandlingForm();
            if ($shiphandling->load(Yii::$app->request->post(), '') && $shiphandling->validate()) {
                if ($newShiphandling = $shiphandling->addShiphandling()) {
                    $resume->ship_handling_id = $newShiphandling->id;
                } 
            }

            $internalauditorscourse = new InternalAuditorsCourseForm();
            if ($internalauditorscourse->load(Yii::$app->request->post(), '') && $internalauditorscourse->validate()) {
                if ($newInternalauditorscourse = $internalauditorscourse->addInternalauditorscourse()) {
                    $resume->internal_auditors_course_id = $newInternalauditorscourse->id;
                } 
            }

            $trainingforseafarers = new TrainingForSeafarersForm();
            if ($trainingforseafarers->load(Yii::$app->request->post(), '') && $trainingforseafarers->validate()) {
                if ($newTrainingforseafarers = $trainingforseafarers->addTrainingforseafarers()) {
                    $resume->training_for_seafarers_id = $newTrainingforseafarersForm->id;
                } 
            }

            $securityawareness = new SecurityAwarenessForm();
            if ($securityawareness->load(Yii::$app->request->post(), '') && $securityawareness->validate()) {
                if ($newSecurityawareness = $securityawareness->addSecurityawareness()) {
                    $resume->security_awareness_id = $newSecurityawareness->id;
                } 
            }

            $electicalelectronic = new ElecticalElectronicForm();
            if ($electicalelectronic->load(Yii::$app->request->post(), '') && $electicalelectronic->validate()) {
                if ($newElecticalelectronic = $electicalelectronic->addElecticalelectronic()) {
                    $resume->electical_electronic_id = $newElecticalelectronic->id;
                } 
            }

            $quantityOtherCourses = count(Yii::$app->request->post()['other_course_number']);
            for ($i = 0; $i < $quantityOtherCourses; $i++) {
                $otherCourse = new OtherCourseForm();
                if ($otherCourse->load(Yii::$app->request->post(), '') && $otherCourse->validate()) 
                {
                    if ($newOtherCourse = $otherCourse->addOthercourse($i)) {
                        $resume->other_course_ids .= ':' . $newOtherCourse->id . ':';
                    }
                }
            }

            $newbuilding = new NewBuildingForm();
            if ($newbuilding->load(Yii::$app->request->post(), '') && $newbuilding->validate()) {
                if ($newNewbuilding = $newbuilding->addNewBuilding()) {
                    $resume->new_building_id = $newNewbuilding->id;
                } 
            }

            $specialisedprojects = new SpecialisedProjectsForm();
            if ($specialisedprojects->load(Yii::$app->request->post(), '') && $specialisedprojects->validate()) {
                if ($newSpecialisedprojects = $specialisedprojects->addSpecialisedprojects()) {
                    $resume->specialised_projects_id = $newSpecialisedprojects->id;
                } 
            }

            $specialtrades = new SpecialTradesForm();
            if ($specialtrades->load(Yii::$app->request->post(), '') && $specialtrades->validate()) {
                if ($newSpecialtrades = $specialtrades->addSpecialtrades()) {
                    $resume->special_trades_id = $newSpecialtrades->id;
                } 
            }

            $shoreexperience = new ShoreExperienceForm();
            if ($shoreexperience->load(Yii::$app->request->post(), '') && $shoreexperience->validate()) {
                if ($newShoreexperience = $shoreexperience->addShoreexperience()) {
                    $resume->shore_experience_id = $newShoreexperience->id;
                } 
            }

            $quantityServicedetails = count(Yii::$app->request->post()['service_details_company_name']);

            for ($i = 0; $i < $quantityServicedetails; $i++) {
                $servicedetails = new ServiceDetailsForm();;
                if ($servicedetails->load(Yii::$app->request->post(), '') && $servicedetails->validate()) 
                {   
                    if ($newServicedetails = $servicedetails->addServicedetails($i)) {
                        $resume->service_details_ids .= ':' . $newServicedetails->id . ':';
                    }
                }
            }

            $quantityReferencedetails = count(Yii::$app->request->post()['reference_details_company_name']);
            for ($i = 0; $i < $quantityReferencedetails; $i++) {
                $referencedetails = new ReferenceDetailsForm();
                if ($referencedetails->load(Yii::$app->request->post(), '') && $referencedetails->validate()) 
                {
                    if ($newReferencedetails = $referencedetails->addReferencedetails($i)) {
                        $resume->reference_details_ids .= ':' . $newReferencedetails->id . ':';
                    }
                }
            }

            if ($resume->save()) {
                return $resume;
            }
        }

        return null;
    }

    public function updateResume($otherData)
    {                    
            $resume = Resume::findById($this->id[0]);
            if ($resume->owner_id == Yii::$app->user->identity->id) {
                $resume->created_at = $this->created_at;
                $resume->salary = $this->salary;
                $resume->application_for_position = $this->application_for_position;

                $resume->other_position = $this->other_position;
                $resume->surname = $this->surname;
                $resume->firstname = $this->firstname;
                $resume->other_names = $this->other_names;
                if ($this->sex == '0') {
                    $resume->sex = 'Male'; 
                } else {
                    $resume->sex = $this->sex;
                }
                $resume->date_of_birth = $this->date_of_birth;
                $resume->place_of_birth = $this->place_of_birth;
                $resume->citizenship = $this->citizenship;
                $resume->marital_status = $this->marital_status;
                $resume->color_of_eyes = $this->color_of_eyes;
                $resume->color_of_hair = $this->color_of_hair;
                $resume->height = $this->height;
                $resume->weight = $this->weight;
                $resume->boilersuit_size = $this->boilersuit_size;
                $resume->boots_size = $this->boots_size;
                if ($this->language && $this->level) {
                    $resume->language = '';
                    $resume->level = '';
                    foreach ($this->language as $value) {
                        $resume->language .= ':' . $value . ':';
                    }
                    foreach ($this->level as $value) {
                        $resume->level .= ':' . $value . ':';
                    }
                }
                $resume->country = $this->country;
                $resume->city = $this->city;
                $resume->post_code = $this->post_code;
                $resume->mobile = $this->mobile;
                $resume->email = $this->email[0];
                $resume->skype_name = $this->skype_name;
                $resume->telegram = $this->telegram;
                $resume->viber = $this->viber;
                $resume->whatsapp = $this->whatsapp;
                $resume->next_of_kin = $this->next_of_kin;
                $resume->kin_adress = $this->kin_adress;
                $resume->kin_mobile = $this->kin_mobile;
                $resume->school = $this->school;
                $resume->school_from = $this->school_from;
                $resume->school_to = $this->school_to;

                $image = UploadedFile::getInstance($resume, 'image');
                if ($image) {
                    $fileName = $resume->id . '.' . $image->extension;
                    $filePath = 'uploads/' . $fileName;   
                    $image->saveAs($filePath);  
                    $resume->image = $fileName;  
                }
        

                $travelpassport = new TravelPassportForm();
                if ($travelpassport->load(Yii::$app->request->post(), '') && $travelpassport->validate()) {
                    $travelpassport->updateTravelpassport();
                }

                $seamanbook = new SeamanBookForm();
                if ($seamanbook->load(Yii::$app->request->post(), '') && $seamanbook->validate()) {
                    $seamanbook->updateSeamanbook();
                }
                
                $usvisa = new UsVisaForm();
                if ($usvisa->load(Yii::$app->request->post(), '') && $usvisa->validate()) {
                    $usvisa->updateUsvisa();
                }

                $othervisas = new OtherVisasForm();
                if ($othervisas->load(Yii::$app->request->post(), '') && $othervisas->validate()) {
                    $othervisas->updateOthervisas();
                }

                $issueOtherSeamanbooks = preg_replace('/^:|:$/', '', $resume->other_seaman_book_ids);
                $issueOtherSeamanbooks = explode('::', $issueOtherSeamanbooks);
                $quantityIssueOtherSeamanbooks = count($issueOtherSeamanbooks);
                $quantityOtherSeamanbooks = count(Yii::$app->request->post()['other_otherseamanbook_id']);
                for ($i = 0; $i < $quantityOtherSeamanbooks + 1; $i++) {
                    if (!$quantityIssueOtherSeamanbooks) {
                        if ($i == 0) {
                            $otherSeamanbook = new OtherSeamanBookForm();
                            if ($otherSeamanbook->load(Yii::$app->request->post(), '') && $otherSeamanbook->validate()) {
                                if ($newOtherseamanbook = $otherSeamanbook->addOtherseamanbook()) {
                                    $resume->other_seaman_book_ids .= ':' . $newOtherseamanbook->id . ':';
                                }  
                            }
                        } else {
                            $otherSeamanbook = new OtherOtherSeamanBookForm();
                            if ($otherSeamanbook->load(Yii::$app->request->post(), '') && $otherSeamanbook->validate()) {
                                $dataCertificateId = Yii::$app->request->post()['other_otherseamanbook_id'][$i - 1];
                                if ($newOtherseamanbook = $otherSeamanbook->addOtherseamanbook($i - 1)) {
                                    $resume->other_seaman_book_ids .= ':' . $newOtherseamanbook->id . ':';
                                }  
                            }
                        }
                    } else {
                        if ($i == 0) {
                            $otherSeamanbook = new OtherSeamanBookForm();
                            if ($otherSeamanbook->load(Yii::$app->request->post(), '') && $otherSeamanbook->validate()) {
                                if ($newOtherseamanbook = $otherSeamanbook->updateOtherseamanbook()) {

                                }  
                            }
                        } else {
                            if ($quantityOtherSeamanbooks > $quantityIssueOtherSeamanbooks - 1) {
                                $firstIndexNewOtherSeamanbook = $quantityIssueOtherSeamanbooks;
                                if ($firstIndexNewOtherSeamanbook <= $i) {
                                    $otherSeamanbook = new OtherOtherSeamanBookForm();
                                    if ($otherSeamanbook->load(Yii::$app->request->post(), '') && $otherSeamanbook->validate()) {
                                        $dataCertificateId = Yii::$app->request->post()['other_otherseamanbook_id'][$i - 1];
                                        if ($newOtherseamanbook = $otherSeamanbook->addOtherseamanbook($i - 1)) {
                                            $resume->other_seaman_book_ids .= ':' . $newOtherseamanbook->id . ':';
                                        }  
                                    }
                                } else {
                                    $otherSeamanbook = new OtherOtherSeamanBookForm();
                                    if ($otherSeamanbook->load(Yii::$app->request->post(), '') && $otherSeamanbook->validate()) {
                                        $dataCertificateId = Yii::$app->request->post()['other_otherseamanbook_id'][$i - 1];
                                        if ($newOtherseamanbook = $otherSeamanbook->updateOtherseamanbook($i - 1, $dataCertificateId)) {

                                        }  
                                    }
                                }
                            } else {
                                $otherSeamanbook = new OtherOtherSeamanBookForm();
                                if ($otherSeamanbook->load(Yii::$app->request->post(), '') && $otherSeamanbook->validate()) {
                                    $dataCertificateId = Yii::$app->request->post()['other_otherseamanbook_id'][$i - 1];
                                    if ($newOtherseamanbook = $otherSeamanbook->updateOtherseamanbook($i - 1, $dataCertificateId)) {

                                    }  
                                }
                            }
                        }
                    }  
                }

                $petroleum = new PetroleumForm();
                if ($petroleum->load(Yii::$app->request->post(), '') && $petroleum->validate()) {
                    $petroleum->updatePetroleum();
                }

                $chemical = new ChemicalForm();
                if ($chemical->load(Yii::$app->request->post(), '') && $chemical->validate()) {
                    $chemical->updateChemical();
                }

                $gas = new GasForm();
                if ($gas->load(Yii::$app->request->post(), '') && $gas->validate()) {
                    $gas->updateGas();
                }

                $certificate = new CertificateForm();
                if ($certificate->load(Yii::$app->request->post(), '') && $certificate->validate()) {
                    $certificate->updateCertificate();
                }

                $issueOtherCertificates = preg_replace('/^:|:$/', '', $resume->certificate_ids);
                if ($issueOtherCertificates) {
                    $issueOtherCertificates = explode('::', $issueOtherCertificates);
                    $quantityIssueOtherCertificates = count($issueOtherCertificates);
                } else {
                    $quantityIssueOtherCertificates = 0;
                }   
                $quantityOtherCertificates = count(Yii::$app->request->post()['other_certificate_name']);
                for ($i = 0; $i < $quantityOtherCertificates; $i++) {
                    $otherCertificate = new OtherCertificateForm();
                    if ($otherCertificate->load(Yii::$app->request->post(), '') && $otherCertificate->validate()) {
                        if ($quantityOtherCertificates > $quantityIssueOtherCertificates) {
                            $firstIndexNewOtherCertificate = $quantityIssueOtherCertificates;
                            if ($firstIndexNewOtherCertificate <= $i) {
                                if ($newOtherCertificate = $otherCertificate->addOthercertificate($i)) {
                                    $resume->certificate_ids .= ':' . $newOtherCertificate->id . ':';
                                } 
                            } else {
                                $dataCertificateId = Yii::$app->request->post()['other_certificate_id'][$i];
                                $otherCertificate->updateOthercertificate($i, $dataCertificateId);
                            }
                        } else if (!$quantityIssueOtherCertificates) {
                            $otherCertificate= new Certificate();
                            if ($newOtherCertificate = $otherCertificate->addOthercertificate()) {
                                $resume->certificate_ids .= ':' . $newOtherCertificate->id . ':';
                            }    
                        } else if ($quantityOtherCertificates) {
                            $dataCertificateId = Yii::$app->request->post()['other_certificate_id'][$i];
                            $otherCertificate->updateOthercertificate($i, $dataCertificateId);
                        }
                    }
                }
                
                $certificateinternational = new CertificateInternationalForm();
                if ($certificateinternational->load(Yii::$app->request->post(), '') && $certificateinternational->validate()) {         
                    $certificateinternational->updateCertificateinternational();
                }

                $certificateliberian = new CertificateLiberianForm();
                if ($certificateliberian->load(Yii::$app->request->post(), '') && $certificateliberian->validate()) {
                    $certificateliberian->updateCertificateliberian();
                }

                $certificatenorwegian = new CertificateNorwegianForm();
                if ($certificatenorwegian->load(Yii::$app->request->post(), '') && $certificatenorwegian->validate()) {
                    $certificatenorwegian->updateCertificatenorwegian();
                }

                $certificatepanamanian = new CertificatePanamanianForm();
                if ($certificatepanamanian->load(Yii::$app->request->post(), '') && $certificatepanamanian->validate()) {
                    $certificatepanamanian->updateCertificatepanamanian(); 
                }

                $certificateyellowfever = new CertificateYellowFeverForm();
                if ($certificateyellowfever->load(Yii::$app->request->post(), '') && $certificateyellowfever->validate()) {
                    $certificateyellowfever->updateCertificateyellowfever(); 
                }

                $certificatehealthlis = new CertificateHealthListForm();
                if ($certificatehealthlis->load(Yii::$app->request->post(), '') && $certificatehealthlis->validate()) {
                    $certificatehealthlis->updateCertificatehealthlist();
                }

                $certificatedrugtest = new CertificateDrugTestForm();
                if ($certificatedrugtest->load(Yii::$app->request->post(), '') && $certificatedrugtest->validate()) {
                    $certificatedrugtest->updateCertificatedrugtest(); 
                }

                $traininginstructions = new TrainingInstructionsForm();
                if ($traininginstructions->load(Yii::$app->request->post(), '') && $traininginstructions->validate()) {
                    $traininginstructions->updateTraininginstructions(); 
                }

                $basicfirefighting = new BasicFireFightingForm();
                if ($basicfirefighting->load(Yii::$app->request->post(), '') && $basicfirefighting->validate()) {
                    $basicfirefighting->updateBasicfirefighting();
                }

                $advfirefighting = new AdvFireFightingForm();
                if ($advfirefighting->load(Yii::$app->request->post(), '') && $advfirefighting->validate()) {
                    $advfirefighting->updateAdvfirefighting();
                }

                $elementaryfirstaid = new ElementaryFirstAidForm();
                if ($elementaryfirstaid->load(Yii::$app->request->post(), '') && $elementaryfirstaid->validate()) {
                    $elementaryfirstaid->updateElementaryfirstaid(); 
                }

                $medicalfirstaid = new MedicalFirstAidForm();
                if ($medicalfirstaid->load(Yii::$app->request->post(), '') && $medicalfirstaid->validate()) {
                    $medicalfirstaid->updateMedicalfirstaid();
                }
                $medicalcare = new MedicalCareForm();
                if ($medicalcare->load(Yii::$app->request->post(), '') && $medicalcare->validate()) {
                    $medicalcare->updateMedicalcare();
                }

                $perssafetyresp = new PersSafetyRespForm();
                if ($perssafetyresp->load(Yii::$app->request->post(), '') && $perssafetyresp->validate()) {
                    $perssafetyresp->updatePerssafetyresp(); 
                }

                $craftrescue = new CraftRescueForm();
                if ($craftrescue->load(Yii::$app->request->post(), '') && $craftrescue->validate()) {
                    $craftrescue->updateCraftrescue();
                }

                $fastrescuecraft = new FastRescueCraftForm();
                if ($fastrescuecraft->load(Yii::$app->request->post(), '') && $fastrescuecraft->validate()) {
                    $fastrescuecraft->updateFastrescuecraft();
                }

                $gmdsss = new GmdsssForm();
                if ($gmdsss->load(Yii::$app->request->post(), '') && $gmdsss->validate()) {
                    $gmdsss->updateGmdsss(); 
                }

                $managementlevel = new ManagementLevelForm();
                if ($managementlevel->load(Yii::$app->request->post(), '') && $managementlevel->validate()) {
                    $managementlevel->updateManagementlevel();
                }

                $ecdis = new EcdisForm();
                if ($ecdis->load(Yii::$app->request->post(), '') && $ecdis->validate()) {
                    $ecdis->updateEcdis();
                }

                $radarobservation = new RadarObservationForm();
                if ($radarobservation->load(Yii::$app->request->post(), '') && $radarobservation->validate()) {
                    $radarobservation->updateRadarobservation(); 
                }

                $hazmat = new HazmatForm();
                if ($hazmat->load(Yii::$app->request->post(), '') && $hazmat->validate()) {
                    $hazmat->updateHazmat();
                }

                $oiltanker = new OilTankerForm();
                if ($oiltanker->load(Yii::$app->request->post(), '') && $oiltanker->validate()) {
                    $oiltanker->updateOiltanker(); 
                }

                $advanceoiltanker = new AdvanceOilTankerForm();
                if ($advanceoiltanker->load(Yii::$app->request->post(), '') && $advanceoiltanker->validate()) {
                    $advanceoiltanker->updateAdvanceoiltanker();
                }

                $chemicaltanker = new ChemicalTankerForm();
                if ($chemicaltanker->load(Yii::$app->request->post(), '') && $chemicaltanker->validate()) {
                    $chemicaltanker->updateChemicaltanker(); 
                }

                $gastanker = new GasTankerForm();
                if ($gastanker->load(Yii::$app->request->post(), '') && $gastanker->validate()) {
                    $gastanker->updateGastanker(); 
                }

                $advancegastanker = new AdvanceGasTankerForm();
                if ($advancegastanker->load(Yii::$app->request->post(), '') && $advancegastanker->validate()) {
                    $advancegastanker->updateAdvancegastanker();
                }

                $crudeoilwashing = new CrudeOilWashingForm();
                if ($crudeoilwashing->load(Yii::$app->request->post(), '') && $crudeoilwashing->validate()) {
                    $crudeoilwashing->updateCrudeoilwashing(); 
                }

                $inertgasplant = new InertGasPlantForm();
                if ($inertgasplant->load(Yii::$app->request->post(), '') && $inertgasplant->validate()) {
                    $inertgasplant->updateInertgasplant();
                }

                $ismcode = new IsmCodeForm();
                if ($ismcode->load(Yii::$app->request->post(), '') && $ismcode->validate()) {
                    $ismcode->updateIsmcode(); 
                }

                $shipsecurityofficer = new ShipSecurityOfficerForm();
                if ($shipsecurityofficer->load(Yii::$app->request->post(), '') && $shipsecurityofficer->validate()) {
                    $shipsecurityofficer->updateShipsecurityofficer(); 
                }

                $shipsafetyofficer = new ShipSafetyOfficerForm();
                if ($shipsafetyofficer->load(Yii::$app->request->post(), '') && $shipsafetyofficer->validate()) {
                    $shipsafetyofficer->updateShipsafetyofficer();
                }

                $bridgeteammanagement = new BridgeTeamManagementForm();
                if ($bridgeteammanagement->load(Yii::$app->request->post(), '') && $bridgeteammanagement->validate()) {
                    $bridgeteammanagement->updateBridgeteammanagement(); 
                }

                $dpinduction = new DpInductionForm();
                if ($dpinduction->load(Yii::$app->request->post(), '') && $dpinduction->validate()) {
                    $dpinduction->updateDpinduction(); 
                }

                $dpsimulator = new DpSimulatorForm();
                if ($dpsimulator->load(Yii::$app->request->post(), '') && $dpsimulator->validate()) {
                    $dpsimulator->updateDpsimulator();
                }

                $bridgeengineroomresourcemanagement = new BridgeEngineRoomResourceManagementForm();
                if ($bridgeengineroomresourcemanagement->load(Yii::$app->request->post(), '') && $bridgeengineroomresourcemanagement->validate()) {
                    $bridgeengineroomresourcemanagement->updateBridgeengineroomresourcemanagement();
                }

                $shiphandling = new ShipHandlingForm();
                if ($shiphandling->load(Yii::$app->request->post(), '') && $shiphandling->validate()) {
                    $shiphandling->updateShiphandling();
                }

                $internalauditorscourse = new InternalAuditorsCourseForm();
                if ($internalauditorscourse->load(Yii::$app->request->post(), '') && $internalauditorscourse->validate()) {
                    $internalauditorscourse->updateInternalauditorscourse();
                }

                $trainingforseafarers = new TrainingForSeafarersForm();
                if ($trainingforseafarers->load(Yii::$app->request->post(), '') && $trainingforseafarers->validate()) {
                    $trainingforseafarers->updateTrainingforseafarers(); 
                }

                $securityawareness = new SecurityAwarenessForm();
                if ($securityawareness->load(Yii::$app->request->post(), '') && $securityawareness->validate()) {
                    $securityawareness->updateSecurityawareness();
                }

                $electicalelectronic = new ElecticalElectronicForm();
                if ($electicalelectronic->load(Yii::$app->request->post(), '') && $electicalelectronic->validate()) {
                    $electicalelectronic->updateElecticalelectronic();
                }


                $issueOtherCourses = preg_replace('/^:|:$/', '', $resume->other_course_ids);
                if ($issueOtherCourses) {
                    $issueOtherCourses = explode('::', $issueOtherCourses);
                    $quantityIssueOtherCourses = count($issueOtherCourses);
                } else {
                    $quantityIssueOtherCourses = 0;
                }   
                $quantityOtherCourses = count(Yii::$app->request->post()['other_course_number']);
                for ($i = 0; $i < $quantityOtherCourses; $i++) {
                    $otherCourse = new OtherCourseForm();
                    if ($otherCourse->load(Yii::$app->request->post(), '') && $otherCourse->validate()) {
                        if ($quantityOtherCourses > $quantityIssueOtherCourses) {
                            $firstIndexNewOtherCourse = $quantityIssueOtherCourses;
                            if ($firstIndexNewOtherCourse <= $i) {
                                if ($newOtherCourse = $otherCourse->addOthercourse($i)) {
                                    $resume->other_course_ids .= ':' . $newOtherCourse->id . ':';
                                } 
                            } else {
                                $dataCourseId = Yii::$app->request->post()['other_course_id'][$i];
                                $otherCourse->updateOthercourse($i, $dataCourseId);
                            }
                        } else if (!$quantityIssueOtherCourses) {
                            $otherCourse= new OtherCourseForm();
                            if ($newOtherCourse = $otherCourse->addOthercourse()) {
                                $resume->other_course_ids .= ':' . $newOtherCourse->id . ':';
                            }    
                        } else if ($quantityOtherCourses) {
                            $dataCourseId = Yii::$app->request->post()['other_course_id'][$i];
                            $otherCourse->updateOthercourse($i, $dataCourseId);
                        }
                    }
                }

                $newbuilding = new NewBuildingForm();
                if ($newbuilding->load(Yii::$app->request->post(), '') && $newbuilding->validate()) {
                    $newbuilding->updateNewbuilding();
                }

                $specialisedprojects = new SpecialisedProjectsForm();
                if ($specialisedprojects->load(Yii::$app->request->post(), '') && $specialisedprojects->validate()) {
                    $specialisedprojects->updateSpecialisedprojects();
                }

                $specialtrades = new SpecialTradesForm();
                if ($specialtrades->load(Yii::$app->request->post(), '') && $specialtrades->validate()) {
                    $specialtrades->updateSpecialtrades();
                }

                $shoreexperience = new ShoreExperienceForm();
                if ($shoreexperience->load(Yii::$app->request->post(), '') && $shoreexperience->validate()) {
                    $shoreexperience->updateShoreexperience();
                }

                $issueServiceDetails = preg_replace('/^:|:$/', '', $resume->service_details_ids);
                if ($issueServiceDetails) {
                    $issueServiceDetails = explode('::', $issueServiceDetails);
                    $quantityIssueServiceDetails = count($issueServiceDetails);
                } else {
                    $quantityIssueServiceDetails = 0;
                }
                $quantityServiceDetails = count(Yii::$app->request->post()['service_details_company_name']);
                for ($i = 0; $i < $quantityServiceDetails; $i++) {
                    $serviceDetails = new ServiceDetailsForm();
                    if ($serviceDetails->load(Yii::$app->request->post(), '') && $serviceDetails->validate()) {
                        if ($quantityServiceDetails > $quantityIssueServiceDetails) {
                            $firstIndexNewServiceDetails = $quantityIssueServiceDetails;
                            if ($firstIndexNewServiceDetails <= $i) {
                                if ($newServiceDetails = $serviceDetails->addServicedetails($i)) {
                                    $resume->service_details_ids .= ':' . $newServiceDetails->id . ':';
                                } 
                            } else {
                                $dataDetailsId = Yii::$app->request->post()['service_details_id'][$i];
                                $serviceDetails->updateServicedetails($i, $dataDetailsId);
                            }
                        } else if (!$quantityIssueServiceDetails) {
                            $serviceDetails = new ServiceDetails();
                            if ($newServiceDetails = $serviceDetail->addServicedetails()) {
                                $resume->service_details_ids .= ':' . $newServiceDetails->id . ':';
                            }    
                        } else if ($quantityServiceDetails) {
                            $dataDetailsId = Yii::$app->request->post()['service_details_id'][$i];
                            $serviceDetails->updateServicedetails($i, $dataDetailsId);
                        }
                    }
                }

                $issueReferenceDetails = preg_replace('/^:|:$/', '', $resume->reference_details_ids);
                if ($issueReferenceDetails) {
                    $issueReferenceDetails = explode('::', $issueReferenceDetails);
                    $quantityIssueReferenceDetails = count($issueReferenceDetails);
                } else {
                    $quantityIssueReferenceDetails = 0;
                }
                $quantityReferenceDetails = count(Yii::$app->request->post()['reference_details_company_name']);
                for ($i = 0; $i < $quantityReferenceDetails; $i++) {
                    $referenceDetails = new ReferenceDetailsForm();
                    if ($referenceDetails->load(Yii::$app->request->post(), '') && $referenceDetails->validate()) {
                        if ($quantityReferenceDetails > $quantityIssueReferenceDetails) {
                            $firstIndexNewReferenceDetails = $quantityIssueReferenceDetails;
                            if ($firstIndexNewReferenceDetails <= $i) {
                                if ($newReferenceDetails = $referenceDetails->addReferencedetails($i)) {
                                    $resume->reference_details_ids .= ':' . $newReferenceDetails->id . ':';
                                } 
                            } else {
                                $dataDetailsId = Yii::$app->request->post()['reference_details_id'][$i];
                                $referenceDetails->updateReferencedetails($i, $dataDetailsId);
                            }
                        } else if (!$quantityIssueReferenceDetails) {
                            $referenceDetails = new ReferenceDetails();
                            if ($newReferenceDetails = $referenceDetail->addReferencedetails()) {
                                $resume->reference_details_ids .= ':' . $newReferenceDetails->id . ':';
                            }    
                        } else if ($quantityReferenceDetails) {
                            $dataDetailsId = Yii::$app->request->post()['reference_details_id'][$i];
                            $referenceDetails->updateReferencedetails($i, $dataDetailsId);
                        }
                    }
                }

                if ($resume->save()) {
                    return $resume;
                }
        }

        return null;
    }
}

