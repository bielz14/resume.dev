<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\TrainingInstructions;

class TrainingInstructionsForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addTraininginstructions()
    {        
        $traininginstructions = new TrainingInstructions();
        if ($this->validate()) {
            $traininginstructions->number = $this->number[8];
            $traininginstructions->iss_date = $this->iss_date[14];
            $traininginstructions->exp_date = $this->exp_date[14];
            $traininginstructions->iss_by = $this->iss_by[13];
            if ($traininginstructions->save()) {
                return $traininginstructions;
            }
        }

        return null;
    }

    public function updateTraininginstructions()
    {        
        $traininginstructions = TrainingInstructions::findById($this->id[16]);
        if ($this->validate()) {
            $traininginstructions->number = $this->number[7];
            $traininginstructions->iss_date = $this->iss_date[15];
            $traininginstructions->exp_date = $this->exp_date[15];
            $traininginstructions->iss_by = $this->iss_by[12];
            if ($traininginstructions->save()) {
                return $traininginstructions;
            }
        }

        return null;
    }
}

