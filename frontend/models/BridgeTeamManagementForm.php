<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\BridgeTeamManagement;

class BridgeTeamManagementForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addBridgeteammanagement()
    {        
        $bridgeteammanagement = new BridgeTeamManagement();
        if ($this->validate()) {
            $bridgeteammanagement->number = $this->number[32];
            $bridgeteammanagement->iss_date = $this->iss_date[38];
            $bridgeteammanagement->exp_date = $this->exp_date[38];
            $bridgeteammanagement->iss_by = $this->iss_by[37];
            if ($bridgeteammanagement->save()) {
                return $bridgeteammanagement;
            }
        }

        return null;
    }

    public function updateBridgeteammanagement()
    {        
        $bridgeteammanagement = BridgeTeamManagement::findById($this->id[40]);
        if ($this->validate()) {
            $bridgeteammanagement->number = $this->number[31];
            $bridgeteammanagement->iss_date = $this->iss_date[39];
            $bridgeteammanagement->exp_date = $this->exp_date[39];
            $bridgeteammanagement->iss_by = $this->iss_by[36];
            if ($bridgeteammanagement->save()) {
                return $bridgeteammanagement;
            }
        }

        return null;
    }
}

