<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\InertGasPlant;

class InertGasPlantForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addInertgasplant()
    {        
        $inertgasplant = new InertGasPlant();
        if ($this->validate()) {
            $inertgasplant->number = $this->number[28];
            $inertgasplant->iss_date = $this->iss_date[34];
            $inertgasplant->exp_date = $this->exp_date[34];
            $inertgasplant->iss_by = $this->iss_by[33];
            if ($inertgasplant->save()) {
                return $inertgasplant;
            }
        }

        return null;
    }

    public function updateInertgasplant()
    {        
        $inertgasplant = InertGasPlant::findById($this->id[36]);
        if ($this->validate()) {
            $inertgasplant->number = $this->number[27];
            $inertgasplant->iss_date = $this->iss_date[35];
            $inertgasplant->exp_date = $this->exp_date[35];
            $inertgasplant->iss_by = $this->iss_by[32];
            if ($inertgasplant->save()) {
                return $inertgasplant;
            }
        }

        return null;
    }
}

