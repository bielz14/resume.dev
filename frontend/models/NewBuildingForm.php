<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\NewBuilding;

class NewBuildingForm extends Model
{
    public $id;
    public $exp_from;
    public $exp_to;
    public $comments;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['exp_from', 'each', 'rule' => ['string']],
            
            ['exp_to', 'each', 'rule' => ['string']],
            
            ['comments', 'each', 'rule' => ['string']],
        ];
    }

    public function addNewbuilding()
    {        
        $newbuilding = new NewBuilding();
        if ($this->validate()) {
            $newbuilding->exp_from = $this->exp_from[0];
            $newbuilding->exp_to = $this->exp_to[0];
            $newbuilding->comments = $this->comments[0];
            if ($newbuilding->save()) {
                return $newbuilding;
            }
        }

        return null;
    }

    public function updateNewbuilding()
    {   
        $newbuilding = NewBuilding::findById($this->id[49]);
        if ($this->validate()) { 
            $newbuilding->exp_from = $this->exp_from[0];
            $newbuilding->exp_to = $this->exp_to[0];
            $newbuilding->comments = $this->comments[0];
            if ($newbuilding->save()) {
                return $newbuilding;
            }
        }
        
        return null;
    }
}