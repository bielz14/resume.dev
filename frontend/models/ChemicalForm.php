<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\Chemical;

class ChemicalForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
        ];
    }

    public function addChemical()
    {        
        $chemical = new Chemical();
        if ($this->validate()) {
            $chemical->number = $this->number[1];
            $chemical->iss_date = $this->iss_date[6];
            $chemical->exp_date = $this->exp_date[6];
            if ($chemical->save()) {
                return $chemical;
            }
        }

        return null;
    }

    public function updateChemical()
    {        
        $chemical = Chemical::findById($this->id[7]);
        if ($this->validate()) {
            $chemical->number = $this->number[1];
            $chemical->iss_date = $this->iss_date[6];
            $chemical->exp_date = $this->exp_date[6];
            if ($chemical->save()) {
                return $chemical;
            }
        }

        return null;
    }
}

