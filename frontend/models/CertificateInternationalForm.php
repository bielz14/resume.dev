<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\CertificateInternational;

class CertificateInternationalForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
                     
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addCertificateinternational()
    {        
        $certificateinternational = new CertificateInternational();
        if ($this->validate()) {
            $certificateinternational->number = $this->number[4];
            $certificateinternational->iss_date = $this->iss_date[7];
            $certificateinternational->exp_date = $this->exp_date[7];
            $certificateinternational->iss_by = $this->iss_by[6];
            if ($certificateinternational->save()) {
                return $certificateinternational;
            }
        }

        return null;
    }

    public function updateCertificateinternational()
    {        
        $certificateinternational = CertificateInternational::findById($this->id[9]);
        if ($this->validate()) {
            $certificateinternational->number = $this->number[3];
            $certificateinternational->iss_date = $this->iss_date[6];
            $certificateinternational->exp_date = $this->exp_date[6];
            $certificateinternational->iss_by = $this->iss_by[5];
            if ($certificateinternational->save()) {
                return $certificateinternational;
            }
        }

        return null;
    }
}

