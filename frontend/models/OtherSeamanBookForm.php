<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\OtherSeamanBook;

class OtherSeamanBookForm extends Model
{
    public $id;
    public $document_no;
    public $iss_date;
    public $exp_date;
    public $iss_by;
    public $place_of_issue;

    public function rules()
    {
        return [            
            ['id', 'each', 'rule' => ['integer']],
            ['document_no', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
            
            ['place_of_issue', 'each', 'rule' => ['string']],
        ];
    }

    public function addOtherseamanbook()
    {        

        $otherseamanbook = new OtherSeamanBook();
        if ($this->validate()) {
            $otherseamanbook->document_no = $this->document_no[4];
            $otherseamanbook->iss_date = $this->iss_date[4];
            $otherseamanbook->exp_date = $this->exp_date[4];
            $otherseamanbook->iss_by = $this->iss_by[4];
            $otherseamanbook->place_of_issue = $this->place_of_issue[4];
            if ($otherseamanbook->save()) {
                return $otherseamanbook;
            }
        }
    
        return null;
    }

    public function updateOtherseamanbook()
    {        
        $otherseamanbook = OtherSeamanBook::findById($this->id[5]);
        if ($this->validate()) {
            $otherseamanbook->document_no = $this->document_no[4];
            $otherseamanbook->iss_date = $this->iss_date[4];
            $otherseamanbook->exp_date = $this->exp_date[4];
            $otherseamanbook->iss_by = $this->iss_by[4];
            $otherseamanbook->place_of_issue = $this->place_of_issue[4];
            if ($otherseamanbook->save()) {
                return $otherseamanbook;
            }
        }

        return null;
    }
}

