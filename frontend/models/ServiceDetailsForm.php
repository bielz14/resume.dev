<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\ServiceDetails;

class ServiceDetailsForm extends Model
{
    public $id;
    public $service_details_company_name;
    public $rank;
    public $vessel_name;
    public $signed_on;
    public $signed_off;
    public $period_in_months;
    public $type_of_vessel;
    public $dwt;
    public $engine_type;
    public $bhp;
    public $kw;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['service_details_company_name', 'each', 'rule' => ['string']],
            
            ['rank', 'each', 'rule' => ['integer']],
            
            ['vessel_name', 'each', 'rule' => ['string']],
            
            ['signed_on', 'each', 'rule' => ['string']],
            
            ['signed_off', 'each', 'rule' => ['string']],
            
            ['period_in_months', 'each', 'rule' => ['string']],
            
            ['type_of_vessel', 'each', 'rule' => ['string']],
            
            ['dwt', 'each', 'rule' => ['string']],
            
            ['engine_type', 'each', 'rule' => ['string']],
            
            ['bhp', 'each', 'rule' => ['string']],
            
            ['kw', 'each', 'rule' => ['string']],
        ];
    }

    public function addServicedetails($index)
    {   
        $servicedetails = new ServiceDetails();
        if ($this->validate()) {
            $servicedetails->company_name = $this->service_details_company_name[$index];
            $servicedetails->rank = $this->rank[$index];
            $servicedetails->vessel_name = $this->vessel_name[$index];
            $servicedetails->signed_on = $this->signed_on[$index];
            $servicedetails->signed_off = $this->signed_off[$index];
            $servicedetails->period_in_months = $this->period_in_months[$index];
            $servicedetails->type_of_vessel = $this->type_of_vessel[$index];
            $servicedetails->dwt = $this->dwt[$index];
            $servicedetails->engine_type = $this->engine_type[$index];
            $servicedetails->bhp = $this->bhp[$index];
            $servicedetails->kw = $this->kw[$index];
            if ($servicedetails->save()) {
                return $servicedetails;
            }
        }

        return null;
    }

    public function updateServicedetails($index, $id)
    {        
        $servicedetails = ServiceDetails::findById($id);
        if ($this->validate()) {
            $servicedetails->company_name = $this->service_details_company_name[$index];
            $servicedetails->rank = $this->rank[$index];
            $servicedetails->vessel_name = $this->vessel_name[$index];
            $servicedetails->signed_on = $this->signed_on[$index];
            $servicedetails->signed_off = $this->signed_off[$index];
            $servicedetails->period_in_months = $this->period_in_months[$index];
            $servicedetails->type_of_vessel = $this->type_of_vessel[$index];
            $servicedetails->dwt = $this->dwt[$index];
            $servicedetails->engine_type = $this->engine_type[$index];
            $servicedetails->bhp = $this->bhp[$index];
            $servicedetails->kw = $this->kw[$index];
            if ($servicedetails->save()) {
                return $servicedetails;
            }
        }

        return null;
    }
}

