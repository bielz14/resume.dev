<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\CrudeOilWashing;

class CrudeOilWashingForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addCrudeoilwashing()
    {        
        $crudeoilwashing = new CrudeOilWashing();
        if ($this->validate()) {
            $crudeoilwashing->number = $this->number[27];
            $crudeoilwashing->iss_date = $this->iss_date[33];
            $crudeoilwashing->exp_date = $this->exp_date[33];
            $crudeoilwashing->iss_by = $this->iss_by[32];
            if ($crudeoilwashing->save()) {
                return $crudeoilwashing;
            }
        }

        return null;
    }

    public function updateCrudeoilwashing()
    {        
        $crudeoilwashing = CrudeOilWashing::findById($this->id[35]);
        if ($this->validate()) {
            $crudeoilwashing->number = $this->number[26];
            $crudeoilwashing->iss_date = $this->iss_date[34];
            $crudeoilwashing->exp_date = $this->exp_date[34];
            $crudeoilwashing->iss_by = $this->iss_by[31];
            if ($crudeoilwashing->save()) {
                return $crudeoilwashing;
            }
        }

        return null;
    }
}

