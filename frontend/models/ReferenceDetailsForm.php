<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\ReferenceDetails;

class ReferenceDetailsForm extends Model
{
    public $id;
    public $reference_details_company_name;
    public $address;
    public $phone_no;
    public $email;
    public $contact_person;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['reference_details_company_name', 'each', 'rule' => ['string']],
            
            ['address', 'each', 'rule' => ['string']],
            
            ['phone_no', 'each', 'rule' => ['string']],
            
            ['email', 'each', 'rule' => ['string']],
            
            ['contact_person', 'each', 'rule' => ['string']],
        ];
    }

    public function addReferencedetails($index)
    {        
        $referencedetails = new ReferenceDetails();
        if ($this->validate()) {
            $referencedetails->company_name = $this->reference_details_company_name[$index];
            $referencedetails->address = $this->address[$index];
            $referencedetails->phone_no = $this->phone_no[$index];
            $referencedetails->email = $this->email[$index];
            $referencedetails->contact_person = $this->contact_person[$index];
            if ($referencedetails->save()) {
                return $referencedetails;
            }
        }

        return null;
    }

    public function updateReferencedetails($index, $id)
    {        
        $referencedetails = ReferenceDetails::findById($id);
        if ($this->validate()) {
            $referencedetails->company_name = $this->reference_details_company_name[$index];
            $referencedetails->address = $this->address[$index];
            $referencedetails->phone_no = $this->phone_no[$index];
            $referencedetails->email = $this->email[$index];
            $referencedetails->contact_person = $this->contact_person[$index];
            if ($referencedetails->save()) {
                return $referencedetails;
            }
        }

        return null;
    }
}

