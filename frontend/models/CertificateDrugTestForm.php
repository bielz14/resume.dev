<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\CertificateDrugTest;

class CertificateDrugTestForm extends Model
{
    public $id;
    public $iss_date;
    public $exp_date;
    public $iss_by;
    public $iss_at;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
            
            ['iss_at', 'each', 'rule' => ['string']],
        ];
    }

    public function addCertificatedrugtest()
    {        
        $certificatedrugtest = new CertificateDrugTest();
        if ($this->validate()) {
            $certificatedrugtest->iss_date = $this->iss_date[15];
            $certificatedrugtest->exp_date = $this->exp_date[15];
            $certificatedrugtest->iss_by = $this->iss_by[12];
            $certificatedrugtest->iss_at = $this->iss_at[2];
            if ($certificatedrugtest->save()) {
                return $certificatedrugtest;
            }
        }

        return null;
    }

    public function updateCertificatedrugtest()
    {        
        $certificatedrugtest = CertificateDrugTest::findById($this->id[15]);
        if ($this->validate()) {
            $certificatedrugtest->iss_date = $this->iss_date[14];
            $certificatedrugtest->exp_date = $this->exp_date[14];
            $certificatedrugtest->iss_by = $this->iss_by[11];
            $certificatedrugtest->iss_at = $this->iss_at[2];
            if ($certificatedrugtest->save()) {
                return $certificatedrugtest;
            }
        }

        return null;
    }
}

