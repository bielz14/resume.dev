<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\AdvanceGasTanker;

class AdvanceGasTankerForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addAdvancegastanker()
    {        
        $advancegastanker = new AdvanceGasTanker();
        if ($this->validate()) {
            $advancegastanker->number = $this->number[26];
            $advancegastanker->iss_date = $this->iss_date[32];
            $advancegastanker->exp_date = $this->exp_date[32];
            $advancegastanker->iss_by = $this->iss_by[31];
            if ($advancegastanker->save()) {
                return $advancegastanker;
            }
        }

        return null;
    }

    public function updateAdvancegastanker()
    {        
        $advancegastanker = AdvanceGasTanker::findById($this->id[34]);
        if ($this->validate()) {
            $advancegastanker->number = $this->number[25];
            $advancegastanker->iss_date = $this->iss_date[33];
            $advancegastanker->exp_date = $this->exp_date[33];
            $advancegastanker->iss_by = $this->iss_by[30];
            if ($advancegastanker->save()) {
                return $advancegastanker;
            }
        }

        return null;
    }
}

