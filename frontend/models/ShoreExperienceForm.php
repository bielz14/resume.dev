<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\ShoreExperience;

class ShoreExperienceForm extends Model
{
    public $id;
    public $exp_from;
    public $exp_to;
    public $comments;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['exp_from', 'each', 'rule' => ['string']],
            
            ['exp_to', 'each', 'rule' => ['string']],
            
            ['comments', 'each', 'rule' => ['string']],
        ];
    }

    public function addShoreexperience()
    {        
        $shoreexperience = new ShoreExperience();
        if ($this->validate()) {
            $shoreexperience->exp_from = $this->exp_from[3];
            $shoreexperience->exp_to = $this->exp_to[3];
            $shoreexperience->comments = $this->comments[3];
            if ($shoreexperience->save()) {
                return $shoreexperience;
            }
        }

        return null;
    }

    public function updateShoreexperience()
    {        
        $shoreexperience = ShoreExperience::findById($this->id[52]);
        if ($this->validate()) {
            $shoreexperience->exp_from = $this->exp_from[3];
            $shoreexperience->exp_to = $this->exp_to[3];
            $shoreexperience->comments = $this->comments[3];
            if ($shoreexperience->save()) {
                return $shoreexperience;
            }
        }

        return null;
    }
}

