<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\MedicalFirstAid;

class MedicalFirstAidForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addMedicalfirstaid()
    {        
        $medicalfirstaid = new MedicalFirstAid();
        if ($this->validate()) {
            $medicalfirstaid->number = $this->number[12];
            $medicalfirstaid->iss_date = $this->iss_date[18];
            $medicalfirstaid->exp_date = $this->exp_date[18];
            $medicalfirstaid->iss_by = $this->iss_by[17];
            if ($medicalfirstaid->save()) {
                return $medicalfirstaid;
            }
        }

        return null;
    }

    public function updateMedicalfirstaid()
    {        
        $medicalfirstaid = MedicalFirstAid::findById($this->id[20]);
        if ($this->validate()) {
            $medicalfirstaid->number = $this->number[11];
            $medicalfirstaid->iss_date = $this->iss_date[19];
            $medicalfirstaid->exp_date = $this->exp_date[19];
            $medicalfirstaid->iss_by = $this->iss_by[16];
            if ($medicalfirstaid->save()) {
                return $medicalfirstaid;
            }
        }

        return null;
    }
}

