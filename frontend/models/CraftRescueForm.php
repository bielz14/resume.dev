<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\CraftRescue;

class CraftRescueForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addCraftrescue()
    {        
        $craftrescue = new CraftRescue();
        if ($this->validate()) {
            $craftrescue->number = $this->number[15];
            $craftrescue->iss_date = $this->iss_date[21];
            $craftrescue->exp_date = $this->exp_date[21];
            $craftrescue->iss_by = $this->iss_by[20];
            if ($craftrescue->save()) {
                return $craftrescue;
            }
        }

        return null;
    }

    public function updateCraftRescue()
    {        
        $craftrescue = CraftRescue::findById($this->id[23]);
        if ($this->validate()) {
            $craftrescue->number = $this->number[14];
            $craftrescue->iss_date = $this->iss_date[22];
            $craftrescue->exp_date = $this->exp_date[22];
            $craftrescue->iss_by = $this->iss_by[19];
            if ($craftrescue->save()) {
                return $craftrescue;
            }
        }

        return null;
    }
}

