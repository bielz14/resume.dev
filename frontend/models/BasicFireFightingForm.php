<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\BasicFireFighting;

class BasicFireFightingForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addBasicfirefighting()
    {        
        $basicfirefighting = new BasicFireFighting();
        if ($this->validate()) {
            $basicfirefighting->number = $this->number[9];
            $basicfirefighting->iss_date = $this->iss_date[15];
            $basicfirefighting->exp_date = $this->exp_date[15];
            $basicfirefighting->iss_by = $this->iss_by[14];
            if ($basicfirefighting->save()) {
                return $basicfirefighting;
            }
        }

        return null;
    }

    public function updateBasicfirefighting()
    {        
        $basicfirefighting = BasicFireFighting::findById($this->id[17]);
        if ($this->validate()) {
            $basicfirefighting->number = $this->number[8];
            $basicfirefighting->iss_date = $this->iss_date[16];
            $basicfirefighting->exp_date = $this->exp_date[16];
            $basicfirefighting->iss_by = $this->iss_by[13];
            if ($basicfirefighting->save()) {
                return $basicfirefighting;
            }
        }

        return null;
    }
}

