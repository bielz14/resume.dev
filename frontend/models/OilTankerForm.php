<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\OilTanker;

class OilTankerForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addOiltanker()
    {        
        $oiltanker = new OilTanker();
        if ($this->validate()) {
            $oiltanker->number = $this->number[22];
            $oiltanker->iss_date = $this->iss_date[28];
            $oiltanker->exp_date = $this->exp_date[28];
            $oiltanker->iss_by = $this->iss_by[27];
            if ($oiltanker->save()) {
                return $oiltanker;
            }
        }

        return null;
    }

    public function updateOiltanker()
    {        
        $oiltanker = OilTanker::findById($this->id[30]);
        if ($this->validate()) {
            $oiltanker->number = $this->number[21];
            $oiltanker->iss_date = $this->iss_date[29];
            $oiltanker->exp_date = $this->exp_date[29];
            $oiltanker->iss_by = $this->iss_by[26];
            if ($oiltanker->save()) {
                return $oiltanker;
            }
        }

        return null;
    }
}