<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\Hazmat;

class HazmatForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addHazmat()
    {        
        $hazmat = new Hazmat();
        if ($this->validate()) {
            $hazmat->number = $this->number[21];
            $hazmat->iss_date = $this->iss_date[27];
            $hazmat->exp_date = $this->exp_date[27];
            $hazmat->iss_by = $this->iss_by[26];
            if ($hazmat->save()) {
                return $hazmat;
            }
        }

        return null;
    }

    public function updateHazmat()
    {        
        $hazmat = Hazmat::findById($this->id[29]);
        if ($this->validate()) {
            $hazmat->number = $this->number[20];
            $hazmat->iss_date = $this->iss_date[28];
            $hazmat->exp_date = $this->exp_date[28];
            $hazmat->iss_by = $this->iss_by[25];
            if ($hazmat->save()) {
                return $hazmat;
            }
        }

        return null;
    }
}

