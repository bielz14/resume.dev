<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\TravelPassport;

class TravelPassportForm extends Model
{
    public $id;
    public $document_no;
    public $iss_date;
    public $exp_date;
    public $iss_by;
    public $place_of_issue;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['document_no', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
            
            ['place_of_issue', 'each', 'rule' => ['string']],
        ];
    }

    public function addTravelpassport()
    {   
        $travelpassport = new TravelPassport();
        if ($this->validate()) {
            $travelpassport->document_no = $this->document_no[0];
            $travelpassport->iss_date = $this->iss_date[0];
            $travelpassport->exp_date = $this->exp_date[0];
            $travelpassport->iss_by = $this->iss_by[0];
            $travelpassport->place_of_issue = $this->place_of_issue[0];
            if ($travelpassport->save()) {
                return $travelpassport;
            }
        }

        return null;
    }

    public function updateTravelpassport()
    {   
        $travelpassport = TravelPassport::findById($this->id[1]);
        if ($this->validate()) {
            $travelpassport->document_no = $this->document_no[0];
            $travelpassport->iss_date = $this->iss_date[0];
            $travelpassport->exp_date = $this->exp_date[0];
            $travelpassport->iss_by = $this->iss_by[0];
            $travelpassport->place_of_issue = $this->place_of_issue[0];
            if ($travelpassport->save()) {
                return $travelpassport;
            }
        }

        return null;
    }
}

