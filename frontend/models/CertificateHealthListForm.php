<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\CertificateHealthList;

class CertificateHealthListForm extends Model
{
    public $id;
    public $iss_date;
    public $exp_date;
    public $iss_by;
    public $iss_at;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
            
            ['iss_at', 'each', 'rule' => ['string']],
        ];
    }

    public function addCertificatehealthlist()
    {        
        $certificatehealthlist = new CertificateHealthList();
        if ($this->validate()) {
            $certificatehealthlist->iss_date = $this->iss_date[14];
            $certificatehealthlist->exp_date = $this->exp_date[14];
            $certificatehealthlist->iss_by = $this->iss_by[11];
            $certificatehealthlist->iss_at = $this->iss_at[1];
            if ($certificatehealthlist->save()) {
                return $certificatehealthlist;
            }
        }

        return null;
    }

    public function updateCertificatehealthlist()
    {        
        $certificatehealthlist = CertificateHealthList::findById($this->id[14]);
        if ($this->validate()) {
            $certificatehealthlist->iss_date = $this->iss_date[13];
            $certificatehealthlist->exp_date = $this->exp_date[13];
            $certificatehealthlist->iss_by = $this->iss_by[10];
            $certificatehealthlist->iss_at = $this->iss_at[1];
            if ($certificatehealthlist->save()) {
                return $certificatehealthlist;
            }
        }

        return null;
    }
}

