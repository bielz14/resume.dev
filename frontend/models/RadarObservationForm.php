<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\RadarObservation;

class RadarObservationForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addRadarobservation()
    {        
        $radarobservation = new RadarObservation();
        if ($this->validate()) {
            $radarobservation->number = $this->number[20];
            $radarobservation->iss_date = $this->iss_date[26];
            $radarobservation->exp_date = $this->exp_date[26];
            $radarobservation->iss_by = $this->iss_by[25];
            if ($radarobservation->save()) {
                return $radarobservation;
            }
        }

        return null;
    }

    public function updateRadarobservation()
    {        
        $radarobservation = RadarObservation::findById($this->id[28]);
        if ($this->validate()) {
            $radarobservation->number = $this->number[19];
            $radarobservation->iss_date = $this->iss_date[27];
            $radarobservation->exp_date = $this->exp_date[27];
            $radarobservation->iss_by = $this->iss_by[24];
            if ($radarobservation->save()) {
                return $radarobservation;
            }
        }

        return null;
    }
}

