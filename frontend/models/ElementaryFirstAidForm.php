<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\ElementaryFirstAid;

class ElementaryFirstAidForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addElementaryfirstaid()
    {        
        $elementaryfirstaid = new ElementaryFirstAid();
        if ($this->validate()) {
            $elementaryfirstaid->number = $this->number[11];
            $elementaryfirstaid->iss_date = $this->iss_date[17];
            $elementaryfirstaid->exp_date = $this->exp_date[17];
            $elementaryfirstaid->iss_by = $this->iss_by[16];
            if ($elementaryfirstaid->save()) {
                return $elementaryfirstaid;
            }
        }

        return null;
    }

    public function updateElementaryfirstaid()
    {        
        $elementaryfirstaid = ElementaryFirstAid::findById($this->id[19]);
        if ($this->validate()) {
            $elementaryfirstaid->number = $this->number[10];
            $elementaryfirstaid->iss_date = $this->iss_date[18];
            $elementaryfirstaid->exp_date = $this->exp_date[18];
            $elementaryfirstaid->iss_by = $this->iss_by[15];
            if ($elementaryfirstaid->save()) {
                return $elementaryfirstaid;
            }
        }

        return null;
    }
}

