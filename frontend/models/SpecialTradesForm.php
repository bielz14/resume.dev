<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\SpecialTrades;

class SpecialTradesForm extends Model
{
    public $id;
    public $exp_from;
    public $exp_to;
    public $comments;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['exp_from', 'each', 'rule' => ['string']],
            
            ['exp_to', 'each', 'rule' => ['string']],
            
            ['comments', 'each', 'rule' => ['string']],
        ];
    }

    public function addSpecialtrades()
    {        
        $specialtrades = new SpecialTrades();
        if ($this->validate()) {
            $specialtrades->exp_from = $this->exp_from[2];
            $specialtrades->exp_to = $this->exp_to[2];
            $specialtrades->comments = $this->comments[2];
            if ($specialtrades->save()) {
                return $specialtrades;
            }
        }

        return null;
    }

    public function updateSpecialtrades()
    {        
        $specialtrades = SpecialTrades::findById($this->id[51]);
        if ($this->validate()) {
            $specialtrades->exp_from = $this->exp_from[2];
            $specialtrades->exp_to = $this->exp_to[2];
            $specialtrades->comments = $this->comments[2];
            if ($specialtrades->save()) {
                return $specialtrades;
            }
        }

        return null;
    }
}

