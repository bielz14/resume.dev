<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\UsVisa;

class UsVisaForm extends Model
{
    public $id;
    public $document_no;
    public $iss_date;
    public $exp_date;
    public $iss_by;
    public $place_of_issue;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['document_no', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
            
            ['place_of_issue', 'each', 'rule' => ['string']],
        ];
    }

    public function addUsvisa()
    {        
        $usvisa = new UsVisa();
        if ($this->validate()) {
            $usvisa->document_no = $this->document_no[2];
            $usvisa->iss_date = $this->iss_date[2];
            $usvisa->exp_date = $this->exp_date[2];
            $usvisa->iss_by = $this->iss_by[2];
            $usvisa->place_of_issue = $this->place_of_issue[2];
            if ($usvisa->save()) {
                return $usvisa;
            }
        }

        return null;
    }

    public function updateUsvisa()
    {        
        $usvisa = UsVisa::findById($this->id[3]);
        if ($this->validate()) {
            $usvisa->document_no = $this->document_no[2];
            $usvisa->iss_date = $this->iss_date[2];
            $usvisa->exp_date = $this->exp_date[2];
            $usvisa->iss_by = $this->iss_by[2];
            $usvisa->place_of_issue = $this->place_of_issue[2];
            if ($usvisa->save()) {
                return $usvisa;
            }
        }

        return null;
    }
}

