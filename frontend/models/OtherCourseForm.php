<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\OtherCourse;

class OtherCourseForm extends Model
{
    public $other_course_id;
    public $other_course_number;
    public $other_course_iss_date;
    public $other_course_exp_date;
    public $other_course_iss_by;

    public function rules()
    {
        return [
            ['other_course_id', 'each', 'rule' => ['integer']],

            ['other_course_number', 'each', 'rule' => ['string']],
            
            ['other_course_iss_date', 'each', 'rule' => ['string']],
            
            ['other_course_exp_date', 'each', 'rule' => ['string']],
            
            ['other_course_iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addOthercourse($index)
    {        
        $othercourse = new OtherCourse();
        if ($this->validate()) {
            $othercourse->number = $this->other_course_number[$index];
            $othercourse->iss_date = $this->other_course_iss_date[$index];
            $othercourse->exp_date = $this->other_course_exp_date[$index];
            $othercourse->iss_by = $this->other_course_iss_by[$index];
            if ($othercourse->save()) {
                return $othercourse;
            }
        }

        return null;
    }

    public function updateOthercourse($index, $id)
    {        
        $othercourse = OtherCourse::findById($id);
        if ($this->validate()) {
            $othercourse->number = $this->other_course_number[$index];
            $othercourse->iss_date = $this->other_course_iss_date[$index];
            $othercourse->exp_date = $this->other_course_exp_date[$index];
            $othercourse->iss_by = $this->other_course_iss_by[$index];
            if ($othercourse->save()) {
                return $othercourse;
            }
        }

        return null;
    }
}

