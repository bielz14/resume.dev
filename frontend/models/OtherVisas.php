<?php
namespace frontend\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

/**
 * Resume model
 *
 * @property integer $id
 * @property integer $document_no
 * @property string $iss_date
 * @property string $exp_date
 * @property string $iss_by
 * @property integer $place_of_issue
 */
class OtherVisas extends ActiveRecord implements IdentityInterface
{
    public static function tableName()
    {
        return '{{%other_visas}}';
    }

    public function getId()
    {
        return $this->getPrimaryKey();
    }

    public function getAuthKey()
    {
        return $this->auth_key;
    }

    public function validateAuthKey($authKey)
    {
        return $this->getAuthKey() === $authKey;
    }

    public static function findById($id)
    {
        return static::findOne(['id' => $id]);
    }

    public static function findIdentity($id)
    {
        return static::findOne(['id' => $id, 'status' => self::STATUS_ACTIVE]);
    }

    public static function findIdentityByAccessToken($token, $type = null)
    {
        throw new NotSupportedException('"findIdentityByAccessToken" is not implemented.');
    }

    public function getResume()
    {
        return $this->hasMany(Resume::className(), ['id' => 'other_visas_id']);
    }
}

