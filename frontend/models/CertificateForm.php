<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\Certificate;

class CertificateForm extends Model
{
    public $id;
    public $name;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['name', 'each', 'rule' => ['string']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addCertificate()
    {        
        $certificate = new Certificate();
        if ($this->validate()) {
            if ($this->name[0]) {
                $certificate->name = $this->name[0];
                $certificate->number = $this->number[3];
                $certificate->iss_date = $this->iss_date[8];
                $certificate->exp_date = $this->exp_date[8];
                $certificate->iss_by = $this->iss_by[5];
                if ($certificate->save()) {
                    return $certificate;
                }
            }
        }

        return null;
    }

    public function updateCertificate()
    {        
        $certificate = Certificate::findById($this->id[9]);
        if ($this->validate()) {
            if ($this->name[0]) {
                $certificate->name = $this->name[0];
                $certificate->number = $this->number[3];
                $certificate->iss_date = $this->iss_date[8];
                $certificate->exp_date = $this->exp_date[8];
                $certificate->iss_by = $this->iss_by[5];
                if ($certificate->save()) {
                    return $certificate;
                }
            }
        }

        return null;
    }
}

