<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\SpecialisedProjects;

class SpecialisedProjectsForm extends Model
{
    public $id;
    public $exp_from;
    public $exp_to;
    public $comments;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['exp_from', 'each', 'rule' => ['string']],
            
            ['exp_to', 'each', 'rule' => ['string']],
            
            ['comments', 'each', 'rule' => ['string']],
        ];
    }

    public function addSpecialisedprojects()
    {        
        $specialisedproject = new SpecialisedProjects();
        if ($this->validate()) {
            $specialisedproject->exp_from = $this->exp_from[1];
            $specialisedproject->exp_to = $this->exp_to[1];
            $specialisedproject->comments = $this->comments[1];
            if ($specialisedproject->save()) {
                return $specialisedproject;
            }
        }

        return null;
    }

    public function updateSpecialisedprojects()
    {        
        $specialisedproject = SpecialisedProjects::findById($this->id[50]);
        if ($this->validate()) {
            $specialisedproject->exp_from = $this->exp_from[1];
            $specialisedproject->exp_to = $this->exp_to[1];
            $specialisedproject->comments = $this->comments[1];
            if ($specialisedproject->save()) {
                return $specialisedproject;
            }
        }

        return null;
    }
}

