<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\PersSafetyResp;

class PersSafetyRespForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addPerssafetyresp()
    {        
        $perssafetyresp = new PersSafetyResp();
        if ($this->validate()) {
            $perssafetyresp->number = $this->number[14];
            $perssafetyresp->iss_date = $this->iss_date[20];
            $perssafetyresp->exp_date = $this->exp_date[20];
            $perssafetyresp->iss_by = $this->iss_by[19];
            if ($perssafetyresp->save()) {
                return $perssafetyresp;
            }
        }

        return null;
    }

    public function updatePerssafetyresp()
    {        
        $perssafetyresp = PersSafetyResp::findById($this->id[22]);
        if ($this->validate()) {
            $perssafetyresp->number = $this->number[13];
            $perssafetyresp->iss_date = $this->iss_date[21];
            $perssafetyresp->exp_date = $this->exp_date[21];
            $perssafetyresp->iss_by = $this->iss_by[18];
            if ($perssafetyresp->save()) {
                return $perssafetyresp;
            }
        }

        return null;
    }
}

