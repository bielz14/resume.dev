<?php
namespace frontend\models;

use Yii;
use yii\base\Model;
use frontend\models\DpInductionForm;

class DpInductionForm extends Model
{
    public $id;
    public $number;
    public $iss_date;
    public $exp_date;
    public $iss_by;

    public function rules()
    {
        return [
            ['id', 'each', 'rule' => ['integer']],
            
            ['number', 'each', 'rule' => ['string']],
            
            ['iss_date', 'each', 'rule' => ['string']],
            
            ['exp_date', 'each', 'rule' => ['string']],
            
            ['iss_by', 'each', 'rule' => ['string']],
        ];
    }

    public function addDpinduction()
    {        
        $dpinduction = new DpInduction();
        if ($this->validate()) {
            $dpinduction->number = $this->number[33];
            $dpinduction->iss_date = $this->iss_date[39];
            $dpinduction->exp_date = $this->exp_date[39];
            $dpinduction->iss_by = $this->iss_by[38];
            if ($dpinduction->save()) {
                return $dpinduction;
            }
        }

        return null;
    }

    public function updateDpinduction()
    {        
        $dpinduction = DpInduction::findById($this->id[41]);
        if ($this->validate()) {
            $dpinduction->number = $this->number[32];
            $dpinduction->iss_date = $this->iss_date[40];
            $dpinduction->exp_date = $this->exp_date[40];
            $dpinduction->iss_by = $this->iss_by[37];
            if ($dpinduction->save()) {
                return $dpinduction;
            }
        }

        return null;
    }
}

