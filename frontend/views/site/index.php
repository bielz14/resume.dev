<?php

/* @var $this yii\web\View */
/* @var $form yii\bootstrap\ActiveForm */
/* @var $model \frontend\models\SignupForm */

use yii\helpers\Html;
use yii\bootstrap\ActiveForm;

?>
<?php $form = ActiveForm::begin(['action' => Yii::getAlias('@web') . '/site/createresume', 'id' => 'resume-form', 'options' => ['class' => 'modal-content animate']]); ?>
	<div class="buttons">
	    <button id="save" type="submit"><i class="icon"></i>
	        <p>Save CV</p>
	    </button>
	    <!--<button><i class="icon icon-Download"></i>
	        <p>Download PDF</p>
	    </button>
	    <button><i class="icon icon-Edit"></i>
	        <p>Edit</p>
	    </button>-->
	</div>
	<table class="main_table" cellpadding="0" cellspacing="0" border="0">
	    <tbody>
	        <tr>
	            <td style="padding:40px 0; border: 0px;">
	                <table class="first_table table"> 
				</td> 
				<td class=" first-box logo-box">
	                <img class="logo" src="images/logo.png">
	            </td>
	            <td align="center">
	                <h3>CrewMSG</h3>
	                <h4>Application Form</h4>
	            </td>
	            <td class="first-box avatar-box">
	                <a href="#" class="linkUplphoto" id="linkUplphoto"><i class="icon icon-Download"></i></a>
	                <?=  $form->field($resume, 'image')->fileInput(['name' => 'Resume[image]', 'class' => 'upload_photo', 'id' => 'upload-photo'])->label(false) ?>
	            </td>
	        </tr>
	    </tbody>
	</table>
	<table class="table" style="margin-bottom: 20px">
	    <tr>
	        <td>Ready from:</td>
	        <td>
	        	<?= $form->field($resume, 'created_at')->input('date', ['name' => 'created_at', 'class' => 'input', 'placeholder' => 'dd.mm.yyyy'])->label(false); ?>
	        </td>
	        <td>Salary $:</td>
	        <td>
	        	<?= $form->field($resume, 'salary')->input('text', ['name' => 'salary', 'class' => 'input'])->label(false); ?>
	        </td>
	    </tr>
	    <tr>
	        <td><span class="require_mark">*</span>Application for position as:</td>
	        <td>
	        	<?php 
		        	$items = [
				        'Catering' => [
		                    '0' => '2nd Cook',
		                    '1' => 'Camp Boss',
		                    '2' => 'Chief Cook',
		                    '3' => 'Chief Steward',
		                    '4' => 'Cook',
		                    '5' => 'Cook Assistant',
		                    '6' => 'Laundress',
		                    '7' => 'Messman',
		                    '8' => 'Stewardess'
				        ],
				        'Cruise Ships Positions' => [
				            '9' => 'Activities Supervisor',
		                    '10' => 'Art Auctioneer',
		                    '11' => 'Assistant Cook',
		                    '12' => 'Assistant Hotel Manager',
		                    '13' => 'Baker',
		                    '14' => 'Bar Manager',
		                    '15' => 'Bartender',
		                    '16' => 'Barwaiteress',
		                    '17' => 'Buffet attendant',
		                    '18' => 'Cabin Steward assistant',
		                    '19' => 'Cabin Stewardess',
		                    '20' => 'Chef de Cuisine',
		                    '21' => 'Chef de Rang',
		                    '22' => 'Cleaner',
		                    '23' => 'Cook Trainee',
		                    '24' => 'Deckhand',
		                    '25' => 'Dining Room Head Waiter',
		                    '26' => 'Dishwasher',
		                    '27' => 'First Cook',
		                    '28' => 'Galley Cleaner',
		                    '29' => 'General Cook',
		                    '30' => 'Head Waiter',
		                    '31' => 'Hotel Engineer',
		                    '32' => 'Hotel Manager',
		                    '33' => 'Laundry Man',
		                    '34' => 'Medical Officer',
		                    '35' => 'Mess Boy',
		                    '36' => 'Pastry Man',
		                    '37' => 'Photographer',
		                    '38' => 'Purser',
		                    '39' => 'Receptionist',
		                    '40' => 'Restaurant Steward',
		                    '41' => 'Second Cook',
		                    '42' => 'Shop staff',
		                    '43' => 'Shops Salesperson',
		                    '44' => 'Sommelier',
		                    '45' => 'Staff Engineer',
		                    '46' => 'Steward',
		                    '47' => 'Third Cook',
		                    '48' => 'Training and Development Manager',
		                    '49' => 'Utility',
		                    '50' => 'Waiter',
		                    '51' => 'Waitress',
		                    '52' => 'Wine Steward',
		                    '53' => 'Аdministrator'
				        ],
				        ' Deck' => [
	                        '54' => 'Master',
	                        '55' => '1st Officer',
	                        '56' => '2nd Officer',
	                        '57' => '3rd Officer',
	                        '58' => 'AB / Crane Operator',
	                        '59' => 'AB / Welder',
	                        '60' => 'AB Seaman',
	                        '61' => 'AB-Cook',
	                        '62' => 'Anchor Foreman',
	                        '63' => 'Barge Master',
	                        '64' => 'Boatswain',
	                        '65' => 'Carpenter',
	                        '66' => 'Chief Officer',
	                        '67' => 'Crane Operator',
	                        '68' => 'Deck Cadet',
	                        '69' => 'Deck Fitter',
	                        '70' => 'DP Officer',
	                        '71' => 'Fish-Processor',
	                        '72' => 'Gangway operator',
	                        '73' => 'Junior DP Officer',
	                        '74' => 'Junior officer',
	                        '75' => 'Navigation Officer',
	                        '76' => 'OIM',
	                        '77' => 'OS',
	                        '78' => 'OS / Welder',
	                        '79' => 'OS-Cook',
	                        '80' => 'Painter',
	                        '81' => 'Pilot',
	                        '82' => 'Radio Officer',
	                        '83' => 'Safety Officer',
	                        '84' => 'Senior DP Engineer',
	                        '85' => 'Senior DPO',
	                        '86' => 'Staff Captain',
	                        '87' => 'Trainee Officer',
	                        '88' => 'Trawler',
	                        '89' => 'Watchkeeping Officer',
	                        '90' => 'Yacht Master'
				        ],
				        ' Engine' => [
					         '91' => '1st Engineer',
		                     '92' => '2nd Electrical Engineer',
		                     '93' => '2nd Engineer',
		                     '94' => '2nd ETO',
		                     '95' => '3rd Electrical Engineer',
		                     '96' => '3rd Engineer',
		                     '97' => '4th Engineer',
		                     '98' => 'AB / Motorman',
		                     '99' => 'Barge Engineer',
		                     '100' => 'Cargo Engineer',
		                     '101' => 'Chief Electrical Engineer',
		                     '102' => 'Chief Engineer',
		                     '103' => 'El. Eng. Assistant',
		                     '104' => 'El. Engineer',
		                     '105' => 'El.Eng / 3rd Engineer',
		                     '106' => 'Electric Cadet',
		                     '107' => 'Electrician',
		                     '108' => 'Engine Cadet',
		                     '109' => 'Engineer Assistant',
		                     '110' => 'ETO',
		                     '111' => 'Fitter / Welder',
		                     '112' => 'Gas Engineer',
		                     '113' => 'Mechanic',
		                     '114' => 'Motorman',
		                     '115' => 'Motorman-electrician',
		                     '116' => 'Motorman-welder',
		                     '117' => 'Oiler',
		                     '118' => 'OOW Engine',
		                     '119' => 'Plumber',
		                     '120' => 'Pumpman',
		                     '121' => 'Ref. Engineer',
		                     '122' => 'Ref. Machinist',
		                     '123' => 'Single Engineer',
		                     '124' => 'Staff Chief Engineer',
		                     '125' => 'Storekeeper',
		                     '126' => 'Trainee Engineer',
		                     '127' => 'Turner',
		                     '128' => 'Wiper'
		                ],
		                'Rigs / FPSO / other units' => [
						     '129' => 'Ballast Control Operator',
		                     '130' => 'Cargo Operator',
		                     '131' => 'Cargo Superintendent',
		                     '132' => 'Cargo Supervisor',
		                     '133' => 'Control Room Operator',
		                     '134' => 'Diver',
		                     '135' => 'Elect & Instr Supervisor',
		                     '136' => 'Electrical Technician',
		                     '137' => 'Engine Room Supervisor',
		                     '138' => 'Foreman',
		                     '139' => 'Instrument Technician',
		                     '140' => 'Maintenance Engineer',
		                     '141' => 'Maintenance Operator',
		                     '142' => 'Maintenance Superintendent',
		                     '143' => 'Maintenance Supervisor',
		                     '144' => 'Mechanical Supervisor',
		                     '145' => 'Mechanical Technician',
		                     '146' => 'Plant Operator',
		                     '147' => 'Production Engineer',
		                     '148' => 'Production Operator',
		                     '149' => 'Production Technician',
		                     '150' => 'Roustabout',
		                     '151' => 'Superintendant'
		                ],
		                ' Safety / Security / Medical' => [
		                     '0' => 'Doctor',
		                     '1' => 'HSE Manager',
		                     '2' => 'Security Staff',
		                     '3' => 'Ship Security Officer'
		                ],
		                ' Safety / Security / Medical' => [
		                     '152' => 'Doctor',
		                     '153' => 'HSE Manager',
		                     '154' => 'Security Staff',
		                     '155' => 'Ship Security Officer'
		                ],
		                'Shore Positions' => [
		                     '156' => 'Auditor',
		                     '157' => 'Electrical inspector',
		                     '158' => 'Engineer',
		                     '159' => 'Health and Safety Officer',
		                     '160' => 'Mechanical Foreman',
		                     '161' => 'Motorist',
		                     '162' => 'Safety and Operations Manager',
		                     '163' => 'Sandblaster',
		                     '164' => 'Senior Crewing Executive',
		                     '165' => 'Service Technician',
		                     '166' => 'Technical Superintendent',
		                     '167' => 'Training and Competency Coordinator',
		                     '168' => 'Welder'
		                ]
				    ];
	        	?>
	        	<?= 
		        	$form->field($resume, 'application_for_position')->dropDownList($items, ['name' => 'application_for_position', 'class' => 'input require', 'required' => ''])->label(false);
				?>
	        </td>
	       	        <td>Other position (If any):</td>
	        <td>
	        	<?= 
		        	$form->field($resume, 'other_position')->dropDownList($items, ['name' => 'other_position', 'class' => 'input require', 'required' => ''])->label(false);
				?>
	        </td>
	    </tr>
	</table>
	<table class="table">
	    <tbody>
	        <caption>1. PERSONAL DETAILS</caption>
	        <tr>
	            <td><span class="require_mark">*</span>Surname:</td>
	            <td>
	            	<?= $form->field($resume, 'surname')->input('text', ['name' => 'surname', 'class' => 'input require', 'placeholder' => 'Last Name', 'minlength' => '2', 'maxlength' => '20', 'required' => ''])->label(false); ?>
	            </td>
	            <td><span class="require_mark">*</span>First name:</td>
	            <td>
	            	<?= $form->field($resume, 'firstname')->input('text', ['name' => 'firstname', 'class' => 'input require', 'placeholder' => 'Name', 'minlength' => '2', 'maxlength' => '20', 'required' => ''])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td><span class="require_mark">*</span>Others names:</td>
	            <td>
	            	<?= $form->field($resume, 'other_names')->input('text', ['name' => 'other_names', 'class' => 'input require', 'placeholder' => 'Others Name', 'minlength' => '2', 'maxlength' => '20', 'required' => ''])->label(false); ?>
	            </td>
	            <td>Sex:</td>
	            <td>
	            	<?php if ($resume->sex == 'male'): ?>
		            	<?= $form->field($resume, 'sex')->radio(['name' => 'sex', 'value' => 'Male', 'checked' => 'checked'])->label('Male'); ?>
		            		<?= $form->field($resume, 'sex')->radio(['name' => 'sex', 'value' => 'Female'])->label('Female'); ?>
		            <?php else: ?>
		            	<?= $form->field($resume, 'sex')->radio(['name' => 'sex', 'value' => 'Male'])->label('Male'); ?>
		            	<?= $form->field($resume, 'sex')->radio(['name' => 'sex', 'value' => 'Female', 'checked' => 'checked'])->label('Female'); ?>
		            <?php endif; ?>
	            </td>
	        </tr>
	        <tr>
	            <td><span class="require_mark">*</span>Date of birth:</td>
	            <td>
	            	<?= $form->field($resume, 'date_of_birth')->input('date', ['name' => 'date_of_birth', 'class' => 'input require', 'placeholder' => 'dd.mm.yyyy', 'required' => ''])->label(false); ?>
	            </td>
	            <td><span class="require_mark">*</span>Place of birth:</td>
	            <td>
	            	<?= $form->field($resume, 'place_of_birth')->input('text', ['name' => 'place_of_birth', 'class' => 'input require', 'placeholder' => 'Birth Place', 'minlength' => '2', 'maxlength' => '20', 'required' => ''])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	             <td><span class="require_mark">*</span>Citizenship:</td>
	            <td>
	            	<?php 
			        	$itemsCoutries = [
			        		'0' => 'Albania',
		                    '1' => 'Algeria',
		                    '2' => 'Andorra',
		                    '3' => 'Angola',
		                    '4' => 'Antigua and Barbuda',
		                    '5' => 'Argentina',
		                    '6' => 'Armenia',
		                    '7' => 'Aruba',
		                    '8' => 'Australia',
		                    '9' => 'Austria',
		                    '10' => 'Azerbaijan',
		                    '11' => 'Bahamas',
		                    '12' => 'Bahrain',
		                    '13' => 'Bangladesh',
		                    '14' => 'Barbados',
		                    '15' => 'Belarus',
		                    '16' => 'Belgium',
		                    '17' => 'Belize',
		                    '18' => 'Benin',
		                    '19' => 'Bermuda',
		                    '20' => 'Bhutan',
		                    '21' => 'Bolivia',
		                    '22' => 'Bosnia and Herzegovina',
		                    '23' => 'Botswana',
		                    '24' => 'Brazil',
		                    '25' => 'Brunei Darussalam',
		                    '26' => 'Bulgaria',
		                    '27' => 'Burkina Faso',
		                    '28' => 'Burma',
		                    '29' => 'Burundi',
		                    '30' => 'Cambodia',
		                    '31' => 'Cameroon',
		                    '32' => 'Canada',
		                    '33' => 'Cape Verde Islands',
		                    '34' => 'Cayman Islands',
		                    '35' => 'Central African Republic',
		                    '36' => 'Chad',
		                    '37' => 'Chile',
		                    '38' => 'China',
		                    '39' => 'Colombia',
		                    '40' => 'Comoros',
		                    '41' => 'Congo',
		                    '42' => 'Cook Islands',
		                    '43' => 'Costa Rica',
		                    '44' => 'Cote d Ivoire',
		                    '45' => 'Croatia',
		                    '46' => 'Cuba',
		                    '47' => 'Cyprus',
		                    '48' => 'Czech Republic',
		                    '49' => 'Denmark',
		                    '50' => 'Djibouti',
		                    '51' => 'Dominica',
		                    '52' => 'Dominicana',
		                    '53' => 'Ecuador',
		                    '54' => 'Egypt',
		                    '55' => 'El Salvador',
		                    '56' => 'Equatorial Guinea',
		                    '57' => 'Eritrea',
		                    '58' => 'Estonia',
		                    '59' => 'Ethiopia',
		                    '60' => 'Faroe Islands',
		                    '61' => 'Fiji',
		                    '62' => 'Finland',
		                    '63' => 'France',
		                    '64' => 'Gabon',
		                    '65' => 'Gambia',
		                    '66' => 'Georgia',
		                    '67' => 'Germany',
		                    '68' => 'Ghana',
		                    '69' => 'Gibraltar',
		                    '70' => 'Greece',
		                    '71' => 'Grenada',
		                    '72' => 'Guatemala',
		                    '73' => 'Guinea',
		                    '74' => 'Guinea-Bissau',
		                    '75' => 'Guyana',
		                    '76' => 'Haiti',
		                    '77' => 'Honduras',
		                    '78' => 'Hong Kong',
		                    '79' => 'Hungary',
		                    '80' => 'Iceland',
		                    '81' => 'India',
		                    '82' => 'Indonesia',
		                    '83' => 'Iran',
		                    '84' => 'Iraq',
		                    '85' => 'Ireland',
		                    '86' => 'Isle of Man',
		                    '87' => 'Israel',
		                    '88' => 'Italy',
		                    '89' => 'Jamaica',
		                    '90' => 'Japan',
		                    '91' => 'Jordan',
		                    '92' => 'Kazakhstan',
		                    '93' => 'Kenya',
		                    '94' => 'Kiribati',
		                    '95' => 'Korea South',
		                    '96' => 'Korea, North',
		                    '97' => 'Kuwait',
		                    '98' => 'Kyrgyzstan',
		                    '99' => 'Latvia',
		                    '100' => 'Lebanon',
		                    '101' => 'Liberia',
		                    '102' => 'Libya',
		                    '103' => 'Liechtenstein',
		                    '104' => 'Lithuania',
		                    '105' => 'Luxembourg',
		                    '106' => 'Macau',
		                    '107' => 'Macedonia',
		                    '108' => 'Madagascar',
		                    '109' => 'Malawi',
		                    '110' => 'Malaysia',
		                    '111' => 'Maldives',
		                    '112' => 'Mali',
		                    '113' => 'Malta',
		                    '114' => 'Marshall Islands',
		                    '115' => 'Mauritania',
		                    '116' => 'Mauritius',
		                    '117' => 'Mexico',
		                    '118' => 'Micronesia',
		                    '119' => 'Moldova',
		                    '120' => 'Monaco',
		                    '121' => 'Mongolia',
		                    '122' => 'Montenegro',
		                    '123' => 'Morocco',
		                    '124' => 'Mozambique',
		                    '125' => 'Myanmar',
		                    '126' => 'Namibia',
		                    '127' => 'Nauru',
		                    '128' => 'Nepal',
		                    '129' => 'Netherlands',
		                    '130' => 'Netherlands Antilles',
		                    '131' => 'New Zealand',
		                    '132' => 'Nicaragua',
		                    '133' => 'Niger',
		                    '134' => 'Nigeria',
		                    '135' => 'Norway',
		                    '136' => 'Oman',
		                    '137' => 'Pakistan',
		                    '138' => 'Panama',
		                    '139' => 'Papua New Guinea',
		                    '140' => 'Paraguay',
		                    '141' => 'Peru',
		                    '142' => 'Philippines',
		                    '143' => 'Poland',
		                    '144' => 'Portugal',
		                    '145' => 'Qatar',
		                    '146' => 'Romania',
		                    '147' => 'Russia',
		                    '148' => 'Rwanda',
		                    '149' => 'Saint Kitts and Nevis',
		                    '150' => 'Saint Lucia',
		                    '151' => 'Saint Vincent and the Grenadines',
		                    '152' => 'Samoa',
		                    '153' => 'San Marino',
		                    '154' => 'Sao Tome and Principe',
		                    '155' => 'Saudi Arabia',
		                    '156' => 'Senegal',
		                    '157' => 'Serbia',
		                    '158' => 'Seychelles',
		                    '159' => 'Sierra Leone',
		                    '160' => 'Singapore',
		                    '161' => 'Slovakia',
		                    '162' => 'Slovenia',
		                    '163' => 'Solomon Islands',
		                    '164' => 'Somalia',
		                    '165' => 'South Africa',
		                    '166' => 'Spain',
		                    '167' => 'Srb',
		                    '168' => 'Sri Lanka',
		                    '169' => 'St. Vincent, the Grenadines',
		                    '170' => 'Sudan',
		                    '171' => 'Suriname',
		                    '172' => 'Swaziland',
		                    '173' => 'Sweden',
		                    '174' => 'Switzerland',
		                    '175' => 'Syria',
		                    '176' => 'Taiwan',
		                    '177' => 'Tajikistan',
		                    '178' => 'Tanzania',
		                    '179' => 'Thailand',
		                    '181' => 'Timor-Leste',
		                    '182' => 'Togo',
		                    '183' => 'Tonga',
		                    '184' => 'Trinidad and Tobago',
		                    '185' => 'Tunisia',
		                    '186' => 'Turkey',
		                    '187' => 'Turkmenistan',
		                    '188' => 'Tuvalu',
		                    '189' => 'Uganda',
		                    '190' => 'Ukraine',
		                    '191' => 'United Arab Emirates',
		                    '192' => 'United Kingdom',
		                    '193' => 'United States',
		                    '194' => 'Uruguay',
		                    '195' => 'Uzbekistan',
		                    '196' => 'Vanuatu',
		                    '197' => 'Venezuela',
		                    '198' => 'Vietnam',
		                    '199' => 'Yemen',
		                    '200' => 'Zambia',
		                    '201' => 'Zimbabwe',
					    ];
		        	?>
	            	<?= 
			        	$form->field($resume, 'citizenship')->dropDownList($itemsCoutries, ['name' => 'citizenship', 'class' => 'input require', 'required' => ''])->label(false);
					?>
	            </td>
	            <td>Marital Status:</td>
	            <td>
	            	<?php 
		        		$itemsMaritalStatus = [
		        			'0' => 'Married',
		        			'1' => 'Single',
		        			'2' => 'Divorced',
		        			'3' => 'Widowed'
		        		];
		        	?>
		        	<?= 
			        	$form->field($resume, 'marital_status')->dropDownList($itemsMaritalStatus, ['name' => 'marital_status', 'class' => 'input require', 'required' => ''])->label(false);
					?>
	            </td>
	        </tr>
	        <tr>
	            <td>Colour of eyes:</td>
	            <td>
	            	<?= $form->field($resume, 'color_of_eyes')->input('text', ['name' => 'color_of_eyes', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>Colour of hair:</td>
	            <td>
	            	<?= $form->field($resume, 'color_of_hair')->input('text', ['name' => 'color_of_hair', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Heigth:</td>
	            <td>
	            	<?= $form->field($resume, 'height')->input('text', ['name' => 'height', 'class' => 'input', 'minlength' => '2', 'maxlength' => '3'])->label(' cm'); ?>
	            </td>
	            <td>Weight:</td>
	            <td>
	                <?= $form->field($resume, 'weight')->input('text', ['name' => 'weight', 'class' => 'input', 'minlength' => '2', 'maxlength' => '3'])->label(' kg'); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Boilersuit size:</td>
	            <td>
	            	<?= $form->field($resume, 'boilersuit_size')->input('text', ['name' => 'boilersuit_size', 'class' => 'input', 'minlength' => '2', 'maxlength' => '3'])->label(false); ?>
	            </td>
	            <td>Boots size:</td>
	            <td>
	            	<?= $form->field($resume, 'boots_size')->input('text', ['name' => 'boots_size', 'class' => 'input', 'minlength' => '2', 'maxlength' => '3'])->label(false); ?>
	            </td>
	        </tr>
	        <tr class="tbhold">
	            <td><span class="require_mark">*</span>Language:</td>
	            <td>
	            	<?= $form->field($resume, 'language')->input('text', ['name' => 'language[]', 'class' => 'input require', 'required' => '', 'minlength' => '2', 'maxlength' => '20', 'required' => ''])->label(false); ?>
	            </td>
	            <td><span class="require_mark">*</span>level:</td>
	            <td><select name="level[]" class="input require" required>
	                    <option value=""></option>
	                    <option>Poor</option>
	                    <option>Satisfactory</option>
	                    <option>Medium</option>
	                    <option>Good</option>
	                    <option>Fluent</option>
	                </select></td>
	        </tr>
	    </tbody>
	</table>
	<div class="add_btns">
	    <button class="btn_more add_new_language">+ one more language</button>
	    <button class="btn_more remove_new_language">remove</button>
	</div>
	<table class="table" style="margin-bottom: 20px">
	    <tbody>
	        <caption>2. ADRESS</caption>
	        <tr>
	            <td>Country:</td>
	            <td>
	            	<?= $form->field($resume, 'country')->input('text', ['name' => 'country', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>City:</td>
	            <td>
	            	<?= $form->field($resume, 'city')->input('text', ['name' => 'city', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>Post code:</td>
	            <td>
	            	<?= $form->field($resume, 'post_code')->input('text', ['name' => 'post_code', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	        </tr>
	        <!-- 							<tr> 
									<td colspan="3">Nearest international airport:</td>
									<td colspan="3">
										<input class="input" type="text" name="" minlength="2" maxlength="20">
									</td>
								</tr> -->
	        <tr>
	            <td>
	                <span class="require_mark">*</span>Mobile:
	            </td>
	            <td>
	            	<?= $form->field($resume, 'mobile')->input('text', ['name' => 'mobile', 'class' => 'input require', 'minlength' => '7', 'maxlength' => '15', 'required' => ''])->label(false); ?>
	            </td>
	            <td>
	                <span class="require_mark">*</span>E-mail:</td>
	            <td>
	                <?= $form->field($resume, 'email')->input('email', ['name' => 'email[]', 'class' => 'input require', 'minlength' => '5', 'maxlength' => '30', 'required' => ''])->label(false); ?>
	            </td>
	            <td>
	                <span class="require_mark">*</span>Skype name:</td>
	            <td>
	            	<?= $form->field($resume, 'skype_name')->input('text', ['name' => 'skype_name', 'class' => 'input require', 'minlength' => '2', 'maxlength' => '30', 'required' => ''])->label(false); ?>
	            </td>
	        </tr>
	        <tr class="content_center">
	            <td colspan="2">Telegram <?= $form->field($resume, 'telegram')->input('checkbox', ['name' => 'telegram', 'value' => 0])->label(false); ?></td>
	            <td colspan="2">Viber <?= $form->field($resume, 'viber')->input('checkbox', ['name' => 'viber', 'value' => 0])->label(false); ?></td>
	            <td colspan="2">WhatsApp <?= $form->field($resume, 'whatsapp')->input('checkbox', ['name' => 'whatsapp', 'value' => 0])->label(false); ?></td>
	        </tr>
	        <tr>
	            <td>
	                <span class="require_mark">*</span>Next of kin:
	            </td>
	            <td>
	            	<?= $form->field($resume, 'next_of_kin')->input('text', ['name' => 'next_of_kin', 'required' => '', 'class' => 'input require', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <span class="require_mark">*</span>Kin adress:</td>
	            <td>
	            	<?= $form->field($resume, 'kin_adress')->input('text', ['name' => 'kin_adress', 'required' => '', 'class' => 'input require', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <span class="require_mark">*</span>Kin mobile:</td>
	            <td>
	                <?= $form->field($resume, 'kin_mobile')->input('text', ['name' => 'kin_mobile', 'required' => '', 'class' => 'input require', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	        </tr>
	    </tbody>
	</table>
	<table class="table">
		<caption>3. TRAVEL DOCUMENTS</caption>
	    <tbody id="documents">       
	        <tr class="th">
	            <td>Type</td>
	            <td>Document No.</td>
	            <td>Iss.Date</td>
	            <td>Exp. Date</td>
	            <td>Iss. By (authority)</td>
	            <td>Place of issue</td>
	        </tr>
	        <tr>
	            <td>
	                <span class="require_mark">*</span>Travel passport
	            </td>
	            <td>
	            	<?= $form->field($travelpassport, 'document_no')->input('text', ['name' => 'document_no[]', 'class' => 'input require', 'required' => '', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($travelpassport, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input require', 'required' => '', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($travelpassport, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input require', 'required' => '', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($travelpassport, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input require', 'required' => '', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($travelpassport, 'place_of_issue')->input('text', ['name' => 'place_of_issue[]', 'class' => 'input require', 'required' => '', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>
	                <span class="require_mark">*</span>Seaman Book
	            </td>
	            <td>
	            	<?= $form->field($seamanbook, 'document_no')->input('text', ['name' => 'document_no[]', 'class' => 'input require', 'required' => '', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($seamanbook, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input require', 'required' => '', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($seamanbook, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input require', 'required' => '', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($seamanbook, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input require', 'required' => '', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($seamanbook, 'place_of_issue')->input('text', ['name' => 'place_of_issue[]', 'class' => 'input require', 'required' => '', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>US C1/D Visa</td>
	            <td>
	            	<?= $form->field($usvisa, 'document_no')->input('text', ['name' => 'document_no[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($usvisa, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($usvisa, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($usvisa, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($usvisa, 'place_of_issue')->input('text', ['name' => 'place_of_issue[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Other visas</td>
	            <td>
	            	<?= $form->field($othervisas, 'document_no')->input('text', ['name' => 'document_no[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($othervisas, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($othervisas, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($othervisas, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($othervisas, 'place_of_issue')->input('text', ['name' => 'place_of_issue[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	        </tr>
	        <tr class="seaman_book_row">
	            <td>Other seaman book</td>
	            <td>
	            	<?= $form->field($otherseamanbook, 'document_no')->input('text', ['name' => 'document_no[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($otherseamanbook, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($otherseamanbook, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($otherseamanbook, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($otherseamanbook, 'place_of_issue')->input('text', ['name' => 'place_of_issue[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '20'])->label(false); ?>
	            </td>
	        </tr>
	    </tbody>
	</table>
	<div class="add_btns">
	    <button class="btn_more add_new_seaman_book">+ one more seaman book</button>
	    <button class="btn_more remove_new_seaman_book">remove</button>
	</div>
	<table class="table" style="margin-bottom: 20px">
	    <tbody>
	        <caption>4. EDUCATION</caption>
	        <tr>
	            <td>School name:</td>
	            <td>
	                <?= $form->field($resume, 'school')->input('text', ['name' => 'school', 'class' => 'input', 'minlength' => '2', 'maxlength' => '40'])->label(false); ?>
	            </td>
	            <td>From:</td>
	            <td>
	            	<?= $form->field($resume, 'school_from')->input('date', ['name' => 'school_from', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>To:</td>
	            <td>
	                <?= $form->field($resume, 'school_to')->input('date', ['name' => 'school_to', 'class' => 'input'])->label(false); ?>
	            </td>
	        </tr>
	    </tbody>
	</table>
	<table class="table" id="certificate-table">
		<caption>5. PROFESSIONAL QUALIFICATION / CERTIFICATE OF COMPETENCY</caption>
	    <tbody>        
	        <tr class="th">
	            <td>Dangerous cargo endorsement</td>
	            <td>Number</td>
	            <td colspan="2">Issue date</td>
	            <td colspan="2">Expiry date</td>
	        </tr>
	        <tr>
	            <td>Petroleum:</td>
	            <td>
	            	<?= $form->field($petroleum, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '5'])->label(false); ?>
	            </td>
	            <td colspan="2">
	                <?= $form->field($petroleum, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td colspan="2">
	                <?= $form->field($petroleum, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Chemical:</td>
	            <td>
	            	<?= $form->field($chemical, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '5'])->label(false); ?>
	            </td>
	            <td colspan="2">
	                <?= $form->field($chemical, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td colspan="2">
	                <?= $form->field($chemical, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Gas:</td>
	            <td>
	            	<?= $form->field($gas, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '5'])->label(false); ?>
	            </td>
	            <td colspan="2">
	                <?= $form->field($gas, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td colspan="2">
	                <?= $form->field($gas, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	        </tr>
	        <!-- 							<tr> 
									<td>MARLIN’S TEST / LEVEL</td>
								   	<td></td>
								   	<td>RESULT %</td>
								   	<td></td>
								</tr>
								<tr> 
									<td>СES TEST / LEVEL</td>
								   	<td>RESULT %</td>
								   	<td></td>
								   	<td>Issued date</td>
								   	<td></td>
								   	<td>Issued at</td>
								   	<td></td>

								</tr>  -->
	        <tr class="th">
	            <td>Certificate name</td>
	            <td>Number</td>
	            <td>Issue date</td>
	            <td>Expiry date</td>
	            <td>Issued by (authority)</td>
	        </tr>
	        <tr class="certificate_row">
	        	<td>
	                <?= $form->field($certificate, 'name')->input('text', ['name' => 'name[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '5'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificate, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '5'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificate, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificate, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificate, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '40'])->label(false); ?>
	            </td>
	        </tr>
	    </tbody>
	</table>
	<div class="add_btns">
	    <button class="btn_more add_new_certificate">+ one more certificate</button>
	    <button class="btn_more remove_new_certificate">remove</button>
	</div>
	<table class="table" style="margin-bottom: 20px">
	    <tbody>
	        <caption>6. HEALTH CERTIFICATES & VACCINATIONS</caption>
	        <tr class="th">
	            <td>Flage state</td>
	            <td>Number</td>
	            <td>Issue date</td>
	            <td>Expiry date</td>
	            <td>Issued by (authority)</td>
	        </tr>
	        <tr>
	        	<td>International</td>
	            <td>
	                <?= $form->field($certificateInternational, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateInternational, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateInternational, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateInternational, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '40'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Liberian</td>
	            <td>
	                <?= $form->field($certificateLiberian, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateLiberian, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateLiberian, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateLiberian, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '40'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Norwegian</td>
	            <td>
	                <?= $form->field($certificateNorwegian, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateNorwegian, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateNorwegian, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateNorwegian, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '40'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Panamanian</td>
	            <td>
	                <?= $form->field($certificatePanamanian, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificatePanamanian, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificatePanamanian, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificatePanamanian, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '40'])->label(false); ?>
	            </td>
	        </tr>
	        <tr class="th">
	            <td>Name</td>
	            <td>Issue date</td>
	            <td>Expiry date</td>
	            <td>Issued by (authority)</td>
	            <td>Issued at</td>
	        </tr>
	        <tr>
	            <td>Yellow fever</td>
	            <td>
	                <?= $form->field($certificateYellowFever, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateYellowFever, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateYellowFever, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '40'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateYellowFever, 'iss_at')->input('text', ['name' => 'iss_at[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Health list</td>
	            <td>
	                <?= $form->field($certificateHealthList, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateHealthList, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateHealthList, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '40'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateHealthList, 'iss_at')->input('text', ['name' => 'iss_at[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Drug test</td>
	            <td>
	                <?= $form->field($certificateDrugTest, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateDrugTest, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateDrugTest, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '40'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($certificateDrugTest, 'iss_at')->input('text', ['name' => 'iss_at[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	    </tbody>
	</table>
	<table class="table" id="course-table">
	    <tbody>
	        <caption>7. MARINE COURSES</caption>
	        <tr class="th">
	            <td>Course name</td>
	            <td>Number</td>
	            <td>Issue date</td>
	            <td>Expiry date</td>
	            <td>Issued by (authority)</td>
	        </tr>
	        <tr>
	            <td>Basic safety training and instructions</td>
	            <td>
	                <?= $form->field($trainingInstructions, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($trainingInstructions, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($trainingInstructions, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($trainingInstructions, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Basic fire fighting</td>
	            <td>
	                <?= $form->field($basicFireFighting, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($basicFireFighting, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($basicFireFighting, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($basicFireFighting, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Adv. Fire fighting</td>
	            <td>
	                <?= $form->field($advFireFighting, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($advFireFighting, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($advFireFighting, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($advFireFighting, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Elementary First Aid</td>
	            <td>
	                <?= $form->field($elementaryFirstAid, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($elementaryFirstAid, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($elementaryFirstAid, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($elementaryFirstAid, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Medical first aid</td>
	            <td>
	                <?= $form->field($medicalFirstAid, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($medicalFirstAid, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($medicalFirstAid, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($medicalFirstAid, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Medical care</td>
	            <td>
	                <?= $form->field($medicalCare, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($medicalCare, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($medicalCare, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($medicalCare, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Pers. Safety & soc. Resp.</td>
	            <td>
	                <?= $form->field($persSafetyResp, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($persSafetyResp, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($persSafetyResp, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($persSafetyResp, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Prof. in survival craft & rescue boats</td>
	            <td>
	                <?= $form->field($craftRescue, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($craftRescue, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($craftRescue, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($craftRescue, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Fast rescue craft</td>
	            <td>
	                <?= $form->field($fastRescueCraft, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($fastRescueCraft, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($fastRescueCraft, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($fastRescueCraft, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>G.m.d.s.s.</td>
	            <td>
	                <?= $form->field($gmdsss, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($gmdsss, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($gmdsss, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($gmdsss, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>A.r.p.a. (management level)</td>
	            <td>
	                <?= $form->field($managementLevel, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($managementLevel, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($managementLevel, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($managementLevel, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>E.c.d.i.s.</td>
	            <td>
	                <?= $form->field($ecdis, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($ecdis, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($ecdis, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($ecdis, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Radar observation</td>
	            <td>
	                <?= $form->field($radarObservation, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($radarObservation, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($radarObservation, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($radarObservation, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Hazmat</td>
	            <td>
	                <?= $form->field($hazmat, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($hazmat, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($hazmat, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($hazmat, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Oil tanker</td>
	            <td>
	                <?= $form->field($oilTanker, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($oilTanker, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($oilTanker, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($oilTanker, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Advance oil tanker</td>
	            <td>
	                <?= $form->field($advanceOilTanker, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($advanceOilTanker, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($advanceOilTanker, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($advanceOilTanker, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Chemical tanker</td>
	            <td>
	                <?= $form->field($chemicalTanker, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($chemicalTanker, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($chemicalTanker, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($chemicalTanker, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Gas tanker</td>
	            <td>
	                <?= $form->field($gasTanker, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($gasTanker, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($gasTanker, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($gasTanker, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Advance gas tanker</td>
	            <td>
	                <?= $form->field($advanceGasTanker, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($advanceGasTanker, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($advanceGasTanker, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($advanceGasTanker, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Crude oil washing</td>
	            <td>
	                <?= $form->field($crudeOilWashing, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($crudeOilWashing, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($crudeOilWashing, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($crudeOilWashing, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Inert gas plant</td>
	            <td>
	                <?= $form->field($inertGasPlant, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($inertGasPlant, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($inertGasPlant, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($inertGasPlant, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Ism code</td>
	            <td>
	                <?= $form->field($ismCode, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($ismCode, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($ismCode, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($ismCode, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Ship security officer</td>
	            <td>
	                <?= $form->field($shipSecurityOfficer, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($shipSecurityOfficer, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($shipSecurityOfficer, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($shipSecurityOfficer, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Ship safety officer</td>
	            <td>
	                <?= $form->field($shipSafetyOfficer, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($shipSafetyOfficer, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($shipSafetyOfficer, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($shipSafetyOfficer, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Bridge team management</td>
	            <td>
	                <?= $form->field($bridgeTeamManagement, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($bridgeTeamManagement, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($bridgeTeamManagement, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($bridgeTeamManagement, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Dp induction</td>
	            <td>
	                <?= $form->field($dpInduction, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($dpInduction, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($dpInduction, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($dpInduction, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Dp simulator</td>
	            <td>
	                <?= $form->field($dpSimulator, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($dpSimulator, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($dpSimulator, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($dpSimulator, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Bridge / engine room resource management</td>
	            <td>
	                <?= $form->field($bridgeEngineRoomResourceManagement, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($bridgeEngineRoomResourceManagement, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($bridgeEngineRoomResourceManagement, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($bridgeEngineRoomResourceManagement, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Ship handling</td>
	            <td>
	                <?= $form->field($shipHandling, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($shipHandling, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($shipHandling, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($shipHandling, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Internal auditors course</td>
	            <td>
	                <?= $form->field($internalAuditorsCourse, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($internalAuditorsCourse, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($internalAuditorsCourse, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($internalAuditorsCourse, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Training for seafarers with designated secuirity duties</td>
	            <td>
	                <?= $form->field($trainingForSeafarers, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($trainingForSeafarers, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($trainingForSeafarers, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($trainingForSeafarers, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Secuirity awareness training for all seafarers</td>
	            <td>
	                <?= $form->field($securityAwareness, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($securityAwareness, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($securityAwareness, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($securityAwareness, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Electical & electronic equipment</td>
	            <td>
	                <?= $form->field($electicalElectronic, 'number')->input('text', ['name' => 'number[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '10'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($electicalElectronic, 'iss_date')->input('date', ['name' => 'iss_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($electicalElectronic, 'exp_date')->input('date', ['name' => 'exp_date[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($electicalElectronic, 'iss_by')->input('text', ['name' => 'iss_by[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	    </tbody>
	</table>
	<div class="add_btns">
	    <button class="btn_more add_new_course">+ one more course</button>
	    <button class="btn_more remove_new_course">remove</button>
	</div>
	<table class="table" style="margin-bottom: 20px">
	    <tbody>
	        <caption>8. SPECIALISED EXPERIENCE</caption>
	        <tr class="th">
	            <td>Type</td>
	            <td>From</td>
	            <td>To</td>
	            <td>Comments</td>
	        </tr>
	        <tr>
	            <td>New building</td>
	            <td>
	                <?= $form->field($newBuilding, 'exp_from')->input('date', ['name' => 'exp_from[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($newBuilding, 'exp_to')->input('date', ['name' => 'exp_to[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($newBuilding, 'comments')->input('text', ['name' => 'comments[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '60'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Specialised projects</td>
	            <td>
	                <?= $form->field($specialisedProjects, 'exp_from')->input('date', ['name' => 'exp_from[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($specialisedProjects, 'exp_to')->input('date', ['name' => 'exp_to[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($specialisedProjects, 'comments')->input('text', ['name' => 'comments[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '60'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Special trades</td>
	            <td>
	                <?= $form->field($specialTrades, 'exp_from')->input('date', ['name' => 'exp_from[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($specialTrades, 'exp_to')->input('date', ['name' => 'exp_to[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($specialTrades, 'comments')->input('text', ['name' => 'comments[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '60'])->label(false); ?>
	            </td>
	        </tr>
	        <tr>
	            <td>Shore experience</td>
	            <td>
	            	<?= $form->field($shoreExperience, 'id')->hiddenInput(['name' => 'id[]'])->label(false); ?>
	                <?= $form->field($shoreExperience, 'exp_from')->input('date', ['name' => 'exp_from[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($shoreExperience, 'exp_to')->input('date', ['name' => 'exp_to[]', 'class' => 'input'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($shoreExperience, 'comments')->input('text', ['name' => 'comments[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '60'])->label(false); ?>
	            </td>
	        </tr>
	    </tbody>
	</table>
	<table id="table-service_details" class="table wide">
	    <tbody>
	        <caption>COMPLETE SEA – SERVICE DETAILS (last vessel first)</caption>
	        <tr class="th">
	            <td>Company name</td>
	            <td>Rank</td>
	            <td>Vessel name (flag)</td>
	            <td>Signed on</td>
	            <td>Signed off</td>
	            <td>Period in months (eg 4.2)</td>
	            <td>Type of vessel</td>
	            <td>D.W.T.</td>
	            <td>Engine type (engineers only)</td>
	            <td>BHP</td>
	            <td>KW</td>
	        </tr>
	        <tr class="complete_row">
	            <td>
	            	<?= $form->field($serviceDetails, 'service_details_company_name')->input('text', ['name' => 'service_details_company_name[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	            <td>
					<?= 
						$form->field($serviceDetails, 'rank')->dropDownList($items, ['name' => 'rank[]', 'class' => 'input require', 'required' => ''])->label(false);
					?>
				</td>
	            <td>
	            	<?= $form->field($serviceDetails, 'vessel_name')->input('text', ['name' => 'vessel_name[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($serviceDetails, 'signed_on')->input('text', ['name' => 'signed_on[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($serviceDetails, 'signed_off')->input('text', ['name' => 'signed_off[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($serviceDetails, 'period_in_months')->input('text', ['name' => 'period_in_months[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($serviceDetails, 'type_of_vessel')->input('text', ['name' => 'type_of_vessel[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($serviceDetails, 'dwt')->input('text', ['name' => 'dwt[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($serviceDetails, 'engine_type')->input('text', ['name' => 'engine_type[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($serviceDetails, 'bhp')->input('text', ['name' => 'bhp[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($serviceDetails, 'kw')->input('text', ['name' => 'kw[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '30'])->label(false); ?>
	            </td>
	        </tr>
	    </tbody>
	</table>
	<div class="add_btns">
	    <button class="btn_more add_new_service_details">+ one more</button>
	    <button class="btn_more remove_new_service_details">remove</button>
	</div>
	<table id="table-reference_details" class="table">
	    <tbody>
	        <caption>REFERENCE CONTACT DETAILS</caption>
	        <tr class="th">
	            <td>Company name</td>
	            <td>Address</td>
	            <td>Phone No.</td>
	            <td>E-mail</td>
	            <td>Contact person</td>
	        </tr>
	        <tr class="reference_details_row">
	            <td>
	            	<?= $form->field($referenceDetails, 'reference_details_company_name')->input('text', ['name' => 'reference_details_company_name[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '60'])->label(false); ?>
	            </td>
	            <td>
	            	<?= $form->field($referenceDetails, 'address')->input('text', ['name' => 'address[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '60'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($referenceDetails, 'phone_no')->input('text', ['name' => 'phone_no[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '15'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($referenceDetails, 'email')->input('email', ['name' => 'email[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '35'])->label(false); ?>
	            </td>
	            <td>
	                <?= $form->field($referenceDetails, 'contact_person')->input('text', ['name' => 'contact_person[]', 'class' => 'input', 'minlength' => '2', 'maxlength' => '25'])->label(false); ?>
	            </td>
	        </tr>
	    </tbody>
	</table>
<?php ActiveForm::end(); ?>
<div class="add_btns">
    <button class="btn_more add_new_reference_details">+ one more</button>
    <button class="btn_more remove_new_reference_details">remove</button>
</div>
</td>
</tr>
</tbody>
</table>
<script src="https://code.jquery.com/jquery-3.3.1.min.js" integrity="sha256-FgpCb/KJQlLNfOu91ta32o/NMZxltwRo8QtmkMRdAu8=" crossorigin="anonymous"></script>