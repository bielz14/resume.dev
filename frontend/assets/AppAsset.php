<?php

namespace frontend\assets;

use yii\web\AssetBundle;

/**
 * Main frontend application asset bundle.
 */
class AppAsset extends AssetBundle
{
    public $basePath = '@webroot';
    public $baseUrl = '@web';
    public $css = [
        'css/site.css',
        'css/media.css',
        'css/stroke-gap-icons.css',
        'css/style.css',
    ];
    public $js = [
        'js/script.js',
        'js/main.js',
        //'js/jquery-validation/dist/jquery.validate.min.js'
    ];
    public $depends = [
        'yii\web\YiiAsset',
        //'yii\bootstrap\BootstrapAsset',
    ];
}
