<?php
namespace backend\controllers;

use Yii;
use yii\web\Controller;
use yii\filters\VerbFilter;
use yii\filters\AccessControl;
use common\models\LoginForm;
use backend\models\ResumeForm;
use frontend\models\TravelPassportForm;
use frontend\models\SeamanBookForm;
use frontend\models\UsVisaForm;
use frontend\models\OtherVisasForm;
use frontend\models\OtherSeamanbookForm;
use frontend\models\OtherOtherSeamanbookForm;
use frontend\models\PetroleumForm;
use frontend\models\ChemicalForm;
use frontend\models\GasForm;
use frontend\models\CertificateForm;
use frontend\models\CertificateInternationalForm;
use frontend\models\CertificateLiberianForm;
use frontend\models\CertificateNorwegianForm;
use frontend\models\CertificatePanamanianForm;
use frontend\models\CertificateYellowFeverForm;
use frontend\models\CertificateHealthListForm;
use frontend\models\CertificateDrugTestForm;
use frontend\models\TrainingInstructionsForm;
use frontend\models\BasicFireFightingForm;
use frontend\models\AdvFireFightingForm;
use frontend\models\ElementaryFirstAidForm;
use frontend\models\MedicalFirstAidForm;
use frontend\models\MedicalCareForm;
use frontend\models\PersSafetyRespForm;
use frontend\models\CraftRescueForm;
use frontend\models\FastRescueCraftForm;
use frontend\models\GmdsssForm;
use frontend\models\ManagementLevelForm;
use frontend\models\EcdisForm;
use frontend\models\RadarObservationForm;
use frontend\models\HazmatForm;
use frontend\models\OilTankerForm;
use frontend\models\AdvanceOilTankerForm;
use frontend\models\ChemicalTankerForm;
use frontend\models\GasTankerForm;
use frontend\models\AdvanceGasTankerForm;
use frontend\models\CrudeOilWashingForm;
use frontend\models\InertGasPlantForm;
use frontend\models\IsmCodeForm;
use frontend\models\ShipSecurityOfficerForm;
use frontend\models\ShipSafetyOfficerForm;
use frontend\models\BridgeTeamManagementForm;
use frontend\models\DpInductionForm;
use frontend\models\DpSimulatorForm;
use frontend\models\BridgeEngineRoomResourceManagementForm;
use frontend\models\ShipHandlingForm;
use frontend\models\InternalAuditorsCourseForm;
use frontend\models\TrainingForSeafarersForm;
use frontend\models\SecurityAwarenessForm;
use frontend\models\ElecticalElectronicForm;
use frontend\models\NewBuildingForm;
use frontend\models\SpecialisedProjectsForm;
use frontend\models\SpecialTradesForm;
use frontend\models\ShoreExperienceForm;
use frontend\models\ServiceDetailsForm;
use frontend\models\ReferenceDetailsForm;
use frontend\models\Resume;
use frontend\models\TravelPassport;
use frontend\models\SeamanBook;
use frontend\models\UsVisa;
use frontend\models\OtherVisas;
use frontend\models\OtherSeamanbook;
use frontend\models\OtherOtherSeamanbook;
use frontend\models\Petroleum;
use frontend\models\Chemical;
use frontend\models\Gas;
use frontend\models\Certificate;
use frontend\models\CertificateInternational;
use frontend\models\CertificateLiberian;
use frontend\models\CertificateNorwegian;
use frontend\models\CertificatePanamanian;
use frontend\models\CertificateYellowFever;
use frontend\models\CertificateHealthList;
use frontend\models\CertificateDrugTest;
use frontend\models\TrainingInstructions;
use frontend\models\BasicFireFighting;
use frontend\models\AdvFireFighting;
use frontend\models\ElementaryFirstAid;
use frontend\models\MedicalFirstAid;
use frontend\models\MedicalCare;
use frontend\models\PersSafetyResp;
use frontend\models\CraftRescue;
use frontend\models\FastRescueCraft;
use frontend\models\Gmdsss;
use frontend\models\ManagementLevel;
use frontend\models\Ecdis;
use frontend\models\RadarObservation;
use frontend\models\Hazmat;
use frontend\models\OilTanker;
use frontend\models\AdvanceOilTanker;
use frontend\models\ChemicalTanker;
use frontend\models\GasTanker;
use frontend\models\AdvanceGasTanker;
use frontend\models\CrudeOilWashing;
use frontend\models\InertGasPlant;
use frontend\models\IsmCode;
use frontend\models\ShipSecurityOfficer;
use frontend\models\ShipSafetyOfficer;
use frontend\models\BridgeTeamManagement;
use frontend\models\DpInduction;
use frontend\models\DpSimulator;
use frontend\models\BridgeEngineRoomResourceManagement;
use frontend\models\ShipHandling;
use frontend\models\InternalAuditorsCourse;
use frontend\models\TrainingForSeafarers;
use frontend\models\SecurityAwareness;
use frontend\models\ElecticalElectronic;
use frontend\models\OtherCourse;
use frontend\models\NewBuilding;
use frontend\models\SpecialisedProjects;
use frontend\models\SpecialTrades;
use frontend\models\ShoreExperience;
use frontend\models\ServiceDetails;
use frontend\models\ReferenceDetails;
use vendor\setasign\fpdf\FPDF;

/**
 * Site controller
 */
class SiteController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'access' => [
                'class' => AccessControl::className(),
                'rules' => [
                    [
                        'actions' => ['login', 'error', 'pdf'],
                        'allow' => true,
                    ],
                    [
                        'actions' => ['logout', 'index', 'updateresume', 'getpdf', 'pdf'],
                        'allow' => true,
                        'roles' => ['@'],
                    ],
                ],
            ],
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'logout' => ['post', 'get'],
                    'getpdf' => ['post', 'get'],
                    'pdf' => ['post', 'get']
                ],
            ],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function actions()
    {
        return [
            'error' => [
                'class' => 'yii\web\ErrorAction',
            ],
        ];
    }

    /**
     * Displays homepage.
     *
     * @return string
     */
    public function actionIndex()
    {
        $resumes = Resume::find()->all();
        return $this->render('index', ['resumes' => $resumes]);
    }

    /**
     * Login action.
     *
     * @return string
     */
    public function actionLogin()
    {
        if (!Yii::$app->user->isGuest) {
            return $this->goHome();
        }

        $model = new LoginForm();
        if ($model->load(Yii::$app->request->post()) && $model->login()) {
            return $this->goBack();
        } else {
            $model->password = '';

            return $this->render('login', [
                'model' => $model,
            ]);
        }
    }

    /**
     * Logout action.
     *
     * @return string
     */
    public function actionLogout()
    {
        Yii::$app->user->logout();

        return $this->redirect('/admin/site/login', 302);
    }

    public function actionUpdateresume()
    {
        if (!Yii::$app->user->isGuest) {
            $resume = new ResumeForm();
            if ($resume->load(Yii::$app->request->post(),'')) {
                $data = Yii::$app->request->post();
                foreach ($data  as $key => $value) {
                    if ($key == 'document_no') {
                        unset($data[$key]);
                        break;
                    }
                    unset($data[$key]);
                }
                if ($resume->updateResume($data)) {
                    return $this->redirect('/admin/site/');
                    //return $this->redirect('/admin/site/updateresume?id=' . $resume->id[0]);
                }
            } 

            $authenticationUserResume = Resume::find()
                                        ->where(['id' => Yii::$app->request->get('id')])
                                        ->one();
            if ($authenticationUserResume) {                        
                $travelpassport = TravelPassport::findById($authenticationUserResume->travel_passport_id);
                if (!$travelpassport) {
                    $travelpassport = TravelPassport::find()->one();
                }
                $seamanbook = SeamanBook::findById($authenticationUserResume->seaman_book_id);
                if (!$seamanbook) {
                    $seamanbook = SeamanBook::find()->one();
                }
                $usvisa = UsVisa::findById($authenticationUserResume->us_visa_id);
                if (!$usvisa) {
                    $usvisa = UsVisa::find()->one();
                }
                $othervisas = OtherVisas::findById($authenticationUserResume->other_visas_id);
                if (!$othervisas) {
                    $othervisas = OtherVisas::find()->one();
                }

                $otherseamanbookIds = preg_replace('/^:|:$/', '', $authenticationUserResume->other_seaman_book_ids);
                $otherseamanbookIds = explode('::', $otherseamanbookIds);
                $otherseamanbooks = [];
                foreach ($otherseamanbookIds as $otherseamanbookId) {
                    $otherseamanbook = OtherSeamanbook::findById($otherseamanbookId);
                    if ($otherseamanbook) {
                        array_push($otherseamanbooks, $otherseamanbook);
                    }
                    
                }

                /*$otherseamanbook = OtherSeamanbook::findById($otherseamanbookIds[0]);
                if (!$otherseamanbook) {
                    $otherseamanbook = OtherSeamanbook::find()->one();
                }*/

                $petroleum = Petroleum::findById($authenticationUserResume->petroleum_id);
                if (!$petroleum) {
                    $petroleum = Petroleum::find()->one();
                }
                $chemical = Chemical::findById($authenticationUserResume->chemical_id);
                if (!$chemical) {
                    $chemical = Chemical::find()->one();
                }
                $gas = Gas::findById($authenticationUserResume->gas_id);
                if (!$gas) {
                    $gas = Gas::find()->one();
                }

                $certificateIds = preg_replace('/^:|:$/', '', $authenticationUserResume->certificate_ids);
                $certificateIds = explode('::', $certificateIds);
                $certificates = [];
                foreach ($certificateIds as $certificateId) {
                    $certificate = Certificate::findById($certificateId);
                    if ($certificate) {
                        array_push($certificates, $certificate);
                    }
                }

                $certificateInternational = CertificateInternational::findById($authenticationUserResume->certificate_international_id);
                if (!$certificateInternational) {
                    $certificateInternational = CertificateInternational::find()->one();
                }
                $certificateLiberian = CertificateLiberian::findById($authenticationUserResume->certificate_liberian_id);
                if (!$certificateLiberian) {
                    $certificateLiberian = CertificateLiberian::find()->one();
                }
                $certificateNorwegian = CertificateNorwegian::findById($authenticationUserResume->certificate_norwegian_id);
                if (!$certificateNorwegian) {
                    $certificateNorwegian = CertificateNorwegian::find()->one();
                }
                $certificatePanamanian = CertificatePanamanian::findById($authenticationUserResume->certificate_panamanian_id);
                if (!$certificatePanamanian) {
                    $certificatePanamanian = CertificatePanamanian::find()->one();
                }
                $certificateYellowFever = CertificateYellowFever::findById($authenticationUserResume->certificate_yellow_fever_id);
                if (!$certificateYellowFever) {
                    $certificateYellowFever = CertificateYellowFever::find()->one();
                }
                $certificateHealthList = CertificateHealthList::findById($authenticationUserResume->certificate_health_list_id);
                if (!$certificateHealthList) {
                    $certificateHealthList = CertificateHealthList::find()->one();
                }
                $certificateDrugTest = CertificateDrugTest::findById($authenticationUserResume->certificate_drug_test_id);
                if (!$certificateDrugTest) {
                    $certificateDrugTest = CertificateDrugTest::find()->one();
                }
                $trainingInstructions = TrainingInstructions::findById($authenticationUserResume->training_instructions_id);
                if (!$trainingInstructions) {
                    $trainingInstructions = TrainingInstructions::find()->one();
                }
                $basicFireFighting = BasicFireFighting::findById($authenticationUserResume->basic_fire_fighting_id);
                if (!$basicFireFighting) {
                    $basicFireFighting = BasicFireFighting::find()->one();
                }
                $advFireFighting = AdvFireFighting::findById($authenticationUserResume->adv_fire_fighting_id);
                if (!$advFireFighting) {
                    $advFireFighting = AdvFireFighting::find()->one();
                }
                $elementaryFirstAid = ElementaryFirstAid::findById($authenticationUserResume->elementary_first_aid_id);
                if (!$elementaryFirstAid) {
                    $elementaryFirstAid = ElementaryFirstAid::find()->one();
                }
                $medicalFirstAid = MedicalFirstAid::findById($authenticationUserResume->medical_first_aid_id);
                if (!$medicalFirstAid) {
                    $medicalFirstAid = MedicalFirstAid::find()->one();
                }
                $medicalCare = MedicalCare::findById($authenticationUserResume->medical_care_id);
                if (!$medicalCare) {
                    $medicalCare = MedicalCare::find()->one();
                }
                $persSafetyResp = PersSafetyResp::findById($authenticationUserResume->pers_safety_resp_id);
                if (!$persSafetyResp) {
                    $persSafetyResp = PersSafetyResp::find()->one();
                }
                $craftRescue = CraftRescue::findById($authenticationUserResume->craft_rescue_id);
                if (!$craftRescue) {
                    $craftRescue = CraftRescue::find()->one();
                }
                $fastRescueCraft = FastRescueCraft::findById($authenticationUserResume->fast_rescuecraft_id);
                if (!$fastRescueCraft) {
                    $fastRescueCraft = FastRescueCraft::find()->one();
                }
                $gmdsss = Gmdsss::findById($authenticationUserResume->gmdsss_id);
                if (!$gmdsss) {
                    $gmdsss = Gmdsss::find()->one();
                }
                $managementLevel = ManagementLevel::findById($authenticationUserResume->management_level_id);
                if (!$managementLevel) {
                    $managementLevel = ManagementLevel::find()->one();
                }
                $ecdis = Ecdis::findById($authenticationUserResume->ecdis_id);
                if (!$ecdis) {
                    $ecdis = Ecdis::find()->one();
                }
                $radarObservation = RadarObservation::findById($authenticationUserResume->radar_observation_id);
                if (!$radarObservation) {
                    $radarObservation = RadarObservation::find()->one();
                }
                $hazmat = Hazmat::findById($authenticationUserResume->hazmat_id);
                if (!$hazmat) {
                    $hazmat = Hazmat::find()->one();
                }
                $oilTanker = OilTanker::findById($authenticationUserResume->oil_tanker_id);
                if (!$oilTanker) {
                    $oilTanker = OilTanker::find()->one();
                }
                $advanceOilTanker = AdvanceOilTanker::findById($authenticationUserResume->advance_oil_tanker_id);
                if (!$advanceOilTanker) {
                    $advanceOilTanker = AdvanceOilTanker::find()->one();
                }
                $chemicalTanker = ChemicalTanker::findById($authenticationUserResume->chemical_tanker_id);
                if (!$chemicalTanker) {
                    $chemicalTanker = ChemicalTanker::find()->one();
                }
                $gasTanker = GasTanker::findById($authenticationUserResume->gas_tanker_id);
                if (!$gasTanker) {
                    $gasTanker = GasTanker::find()->one();
                }
                $advanceGasTanker = AdvanceGasTanker::findById($authenticationUserResume->advance_gas_tanker_id);
                if (!$advanceGasTanker) {
                    $advanceGasTanker = AdvanceGasTanker::find()->one();
                }
                $crudeOilWashing = CrudeOilWashing::findById($authenticationUserResume->crude_oil_washing_id);
                if (!$crudeOilWashing) {
                    $crudeOilWashing = CrudeOilWashing::find()->one();
                }
                $inertGasPlant = InertGasPlant::findById($authenticationUserResume->inert_gas_plant_id);
                if (!$inertGasPlant) {
                    $inertGasPlant = InertGasPlant::find()->one();
                }
                $ismCode = IsmCode::findById($authenticationUserResume->ism_code_id);
                if (!$ismCode) {
                    $ismCode = IsmCode::find()->one();
                }
                $shipSecurityOfficer = ShipSecurityOfficer::findById($authenticationUserResume->ship_security_officer_id);
                if (!$shipSecurityOfficer) {
                    $shipSecurityOfficer = ShipSecurityOfficer::find()->one();
                }
                $shipSafetyOfficer = ShipSafetyOfficer::findById($authenticationUserResume->ship_safety_officer_id);
                if (!$shipSafetyOfficer) {
                    $shipSafetyOfficer = ShipSafetyOfficer::find()->one();
                }
                $bridgeTeamManagement = BridgeTeamManagement::findById($authenticationUserResume->bridge_team_management_id);
                if (!$bridgeTeamManagement) {
                    $bridgeTeamManagement = BridgeTeamManagement::find()->one();
                }
                $dpInduction = DpInduction::findById($authenticationUserResume->dp_induction_id);
                if (!$dpInduction) {
                    $dpInduction = DpInduction::find()->one();
                }
                $dpSimulator = DpSimulator::findById($authenticationUserResume->dp_simulator_id);
                if (!$dpSimulator) {
                    $dpSimulator = DpSimulator::find()->one();
                }
                $bridgeEngineRoomResourceManagement = BridgeEngineRoomResourceManagement::findById($authenticationUserResume->bridge_engine_room_resource_management_id);
                if (!$bridgeEngineRoomResourceManagement) {
                    $bridgeEngineRoomResourceManagement = BridgeEngineRoomResourceManagement::find()->one();
                }
                $shipHandling = ShipHandling::findById($authenticationUserResume->ship_handling_id);
                if (!$shipHandling) {
                    $shipHandling = ShipHandling::find()->one();
                }
                $internalAuditorsCourse = InternalAuditorsCourse::findById($authenticationUserResume->internal_auditors_course_id);
                if (!$internalAuditorsCourse) {
                    $internalAuditorsCourse = InternalAuditorsCourse::find()->one();
                }
                $trainingForSeafarers = TrainingForSeafarers::findById($authenticationUserResume->training_for_seafarers_id);
                if (!$trainingForSeafarers) {
                    $trainingForSeafarers = TrainingForSeafarers::find()->one();
                }
                $securityAwareness = SecurityAwareness::findById($authenticationUserResume->security_awareness_id);
                if (!$securityAwareness) {
                    $securityAwareness = SecurityAwareness::find()->one();
                }
                $electicalElectronic = ElecticalElectronic::findById($authenticationUserResume->electical_electronic_id);
                if (!$electicalElectronic) {
                    $electicalElectronic = ElecticalElectronic::find()->one();
                }
                $courseIds = preg_replace('/^:|:$/', '', $authenticationUserResume->other_course_ids);
                $courseIds = explode('::', $courseIds);
                $courses = [];
                foreach ($courseIds as $courseId) {
                    $course = OtherCourse::findById($courseId);
                    if ($course) {
                        array_push($courses, $course);
                    }
                }

                $newBuilding = NewBuilding::findById($authenticationUserResume->new_building_id);
                if (!$newBuilding) {
                    $newBuilding = NewBuilding::find()->one();
                }
                $specialisedProjects = SpecialisedProjects::findById($authenticationUserResume->specialised_projects_id);
                if (!$specialisedProjects) {
                    $specialisedProjects = SpecialisedProjects::find()->one();
                }
                $specialTrades = SpecialTrades::findById($authenticationUserResume->special_trades_id);
                if (!$specialTrades) {
                    $specialTrades = SpecialTrades::find()->one();
                }
                $shoreExperience = ShoreExperience::findById($authenticationUserResume->shore_experience_id);
                if (!$shoreExperience) {
                    $shoreExperience = ShoreExperience::find()->one();
                }

                $serviceDetailsIds = preg_replace('/^:|:$/', '', $authenticationUserResume->service_details_ids);
                $serviceDetailsIds = explode('::', $serviceDetailsIds);
                $servicesDetails = [];
                foreach ($serviceDetailsIds as $serviceDetailsId) {
                    $serviceDetails = ServiceDetails::findById($serviceDetailsId);
                    if ($serviceDetails) {
                        array_push($servicesDetails, $serviceDetails);
                    }
                }

                $referenceDetailsIds = preg_replace('/^:|:$/', '', $authenticationUserResume->reference_details_ids);
                $referenceDetailsIds = explode('::', $referenceDetailsIds);
                $referencesDetails = [];
                foreach ($serviceDetailsIds as $referenceDetailsId) {
                    $referenceDetails = ReferenceDetails::findById($referenceDetailsId);
                    if ($referenceDetails) {
                        array_push($referencesDetails, $referenceDetails);
                    }
                }

                return $this->render('update', [
                        'resume' => $authenticationUserResume,
                        'travelpassport' => $travelpassport,
                        'seamanbook' => $seamanbook,
                        'usvisa' => $usvisa,
                        'othervisas' => $othervisas,
                        'otherseamanbooks' => $otherseamanbooks,
                        'petroleum' => $petroleum,
                        'chemical' => $chemical,
                        'gas' => $gas,
                        'certificates' => $certificates,
                        'certificateInternational' => $certificateInternational,
                        'certificateLiberian' => $certificateLiberian,
                        'certificateNorwegian' => $certificateNorwegian,
                        'certificatePanamanian' => $certificatePanamanian,
                        'certificateYellowFever' => $certificateYellowFever,
                        'certificateHealthList' => $certificateHealthList,
                        'certificateDrugTest' => $certificateDrugTest,
                        'trainingInstructions' => $trainingInstructions,
                        'basicFireFighting' => $basicFireFighting,
                        'advFireFighting' => $advFireFighting,
                        'elementaryFirstAid' => $elementaryFirstAid,
                        'medicalFirstAid' => $medicalFirstAid,
                        'medicalCare' => $medicalCare,
                        'persSafetyResp' => $persSafetyResp,
                        'craftRescue' => $craftRescue,
                        'fastRescueCraft' => $fastRescueCraft,
                        'gmdsss' => $gmdsss,
                        'managementLevel' => $managementLevel,
                        'ecdis' => $ecdis,
                        'radarObservation' => $radarObservation,
                        'hazmat' => $hazmat,
                        'oilTanker' => $oilTanker,
                        'advanceOilTanker' => $advanceOilTanker,
                        'chemicalTanker' => $chemicalTanker,
                        'gasTanker' => $gasTanker,
                        'advanceGasTanker' => $advanceGasTanker,
                        'crudeOilWashing' => $crudeOilWashing,
                        'inertGasPlant' => $inertGasPlant,
                        'ismCode' => $ismCode,
                        'shipSecurityOfficer' => $shipSecurityOfficer,
                        'shipSafetyOfficer' => $shipSafetyOfficer,
                        'bridgeTeamManagement' => $bridgeTeamManagement,
                        'dpInduction' => $dpInduction,
                        'dpSimulator' => $dpSimulator,
                        'bridgeEngineRoomResourceManagement' => $bridgeEngineRoomResourceManagement,
                        'shipHandling' => $shipHandling,
                        'internalAuditorsCourse' => $internalAuditorsCourse,
                        'trainingForSeafarers' => $trainingForSeafarers,
                        'securityAwareness' => $securityAwareness,
                        'electicalElectronic' => $electicalElectronic,
                        'courses' => $courses,
                        'newBuilding' => $newBuilding,
                        'specialisedProjects' => $specialisedProjects,
                        'specialTrades' => $specialTrades,
                        'shoreExperience' => $shoreExperience,
                        'servicesDetails' => $servicesDetails,
                        'referencesDetails' => $referencesDetails
                ]);
            }
        }

        return $this->goHome();
    }

    public function actionGetpdf()
    {   
        $apikey = '63524223-6f15-467a-9de7-278a463fb3fb';
        $value = 'http://4cc49916.ngrok.io/admin/site/pdf?id=' . Yii::$app->request->get('id');
        $result = file_get_contents('http://api.html2pdfrocket.com/pdf?apikey=' . urlencode($apikey) . '&value=' . urlencode($value));
        $fileName = 'resume-userid-' . Yii::$app->request->get('id') . '.pdf';
        $filePath = 'pdf/' . $fileName;

        if (!file_exists($filePath)) {
            fopen($filePath . ".pdf", "a");
        }

        if (file_put_contents($filePath, $result)) {
            return $fileName;
        } 
    }

    public function actionPdf()
    {
        $this->layout = 'pdf';
        //if (!Yii::$app->user->isGuest) {
            $resume = new ResumeForm();
            if ($resume->load(Yii::$app->request->post(),'')) {
                $data = Yii::$app->request->post();
                foreach ($data  as $key => $value) {
                    if ($key == 'document_no') {
                        unset($data[$key]);
                        break;
                    }
                    unset($data[$key]);
                }
                if ($resume->updateResume($data)) {
                    return $this->redirect('/admin/site/');
                    //return $this->redirect('/admin/site/updateresume?id=' . $resume->id[0]);
                }
            } 

            $authenticationUserResume = Resume::find()
                                        ->where(['id' => Yii::$app->request->get('id')])
                                        ->one();
            if ($authenticationUserResume) {                        
                $travelpassport = TravelPassport::findById($authenticationUserResume->travel_passport_id);
                if (!$travelpassport) {
                    $travelpassport = TravelPassport::find()->one();
                }
                $seamanbook = SeamanBook::findById($authenticationUserResume->seaman_book_id);
                if (!$seamanbook) {
                    $seamanbook = SeamanBook::find()->one();
                }
                $usvisa = UsVisa::findById($authenticationUserResume->us_visa_id);
                if (!$usvisa) {
                    $usvisa = UsVisa::find()->one();
                }
                $othervisas = OtherVisas::findById($authenticationUserResume->other_visas_id);
                if (!$othervisas) {
                    $othervisas = OtherVisas::find()->one();
                }

                $otherseamanbookIds = preg_replace('/^:|:$/', '', $authenticationUserResume->other_seaman_book_ids);
                $otherseamanbookIds = explode('::', $otherseamanbookIds);
                $otherseamanbooks = [];
                foreach ($otherseamanbookIds as $otherseamanbookId) {
                    $otherseamanbook = OtherSeamanbook::findById($otherseamanbookId);
                    if ($otherseamanbook) {
                        array_push($otherseamanbooks, $otherseamanbook);
                    }
                    
                }

                /*$otherseamanbook = OtherSeamanbook::findById($otherseamanbookIds[0]);
                if (!$otherseamanbook) {
                    $otherseamanbook = OtherSeamanbook::find()->one();
                }*/

                $petroleum = Petroleum::findById($authenticationUserResume->petroleum_id);
                if (!$petroleum) {
                    $petroleum = Petroleum::find()->one();
                }
                $chemical = Chemical::findById($authenticationUserResume->chemical_id);
                if (!$chemical) {
                    $chemical = Chemical::find()->one();
                }
                $gas = Gas::findById($authenticationUserResume->gas_id);
                if (!$gas) {
                    $gas = Gas::find()->one();
                }

                $certificateIds = preg_replace('/^:|:$/', '', $authenticationUserResume->certificate_ids);
                $certificateIds = explode('::', $certificateIds);
                $certificates = [];
                foreach ($certificateIds as $certificateId) {
                    $certificate = Certificate::findById($certificateId);
                    if ($certificate) {
                        array_push($certificates, $certificate);
                    }
                }

                $certificateInternational = CertificateInternational::findById($authenticationUserResume->certificate_international_id);
                if (!$certificateInternational) {
                    $certificateInternational = CertificateInternational::find()->one();
                }
                $certificateLiberian = CertificateLiberian::findById($authenticationUserResume->certificate_liberian_id);
                if (!$certificateLiberian) {
                    $certificateLiberian = CertificateLiberian::find()->one();
                }
                $certificateNorwegian = CertificateNorwegian::findById($authenticationUserResume->certificate_norwegian_id);
                if (!$certificateNorwegian) {
                    $certificateNorwegian = CertificateNorwegian::find()->one();
                }
                $certificatePanamanian = CertificatePanamanian::findById($authenticationUserResume->certificate_panamanian_id);
                if (!$certificatePanamanian) {
                    $certificatePanamanian = CertificatePanamanian::find()->one();
                }
                $certificateYellowFever = CertificateYellowFever::findById($authenticationUserResume->certificate_yellow_fever_id);
                if (!$certificateYellowFever) {
                    $certificateYellowFever = CertificateYellowFever::find()->one();
                }
                $certificateHealthList = CertificateHealthList::findById($authenticationUserResume->certificate_health_list_id);
                if (!$certificateHealthList) {
                    $certificateHealthList = CertificateHealthList::find()->one();
                }
                $certificateDrugTest = CertificateDrugTest::findById($authenticationUserResume->certificate_drug_test_id);
                if (!$certificateDrugTest) {
                    $certificateDrugTest = CertificateDrugTest::find()->one();
                }
                $trainingInstructions = TrainingInstructions::findById($authenticationUserResume->training_instructions_id);
                if (!$trainingInstructions) {
                    $trainingInstructions = TrainingInstructions::find()->one();
                }
                $basicFireFighting = BasicFireFighting::findById($authenticationUserResume->basic_fire_fighting_id);
                if (!$basicFireFighting) {
                    $basicFireFighting = BasicFireFighting::find()->one();
                }
                $advFireFighting = AdvFireFighting::findById($authenticationUserResume->adv_fire_fighting_id);
                if (!$advFireFighting) {
                    $advFireFighting = AdvFireFighting::find()->one();
                }
                $elementaryFirstAid = ElementaryFirstAid::findById($authenticationUserResume->elementary_first_aid_id);
                if (!$elementaryFirstAid) {
                    $elementaryFirstAid = ElementaryFirstAid::find()->one();
                }
                $medicalFirstAid = MedicalFirstAid::findById($authenticationUserResume->medical_first_aid_id);
                if (!$medicalFirstAid) {
                    $medicalFirstAid = MedicalFirstAid::find()->one();
                }
                $medicalCare = MedicalCare::findById($authenticationUserResume->medical_care_id);
                if (!$medicalCare) {
                    $medicalCare = MedicalCare::find()->one();
                }
                $persSafetyResp = PersSafetyResp::findById($authenticationUserResume->pers_safety_resp_id);
                if (!$persSafetyResp) {
                    $persSafetyResp = PersSafetyResp::find()->one();
                }
                $craftRescue = CraftRescue::findById($authenticationUserResume->craft_rescue_id);
                if (!$craftRescue) {
                    $craftRescue = CraftRescue::find()->one();
                }
                $fastRescueCraft = FastRescueCraft::findById($authenticationUserResume->fast_rescuecraft_id);
                if (!$fastRescueCraft) {
                    $fastRescueCraft = FastRescueCraft::find()->one();
                }
                $gmdsss = Gmdsss::findById($authenticationUserResume->gmdsss_id);
                if (!$gmdsss) {
                    $gmdsss = Gmdsss::find()->one();
                }
                $managementLevel = ManagementLevel::findById($authenticationUserResume->management_level_id);
                if (!$managementLevel) {
                    $managementLevel = ManagementLevel::find()->one();
                }
                $ecdis = Ecdis::findById($authenticationUserResume->ecdis_id);
                if (!$ecdis) {
                    $ecdis = Ecdis::find()->one();
                }
                $radarObservation = RadarObservation::findById($authenticationUserResume->radar_observation_id);
                if (!$radarObservation) {
                    $radarObservation = RadarObservation::find()->one();
                }
                $hazmat = Hazmat::findById($authenticationUserResume->hazmat_id);
                if (!$hazmat) {
                    $hazmat = Hazmat::find()->one();
                }
                $oilTanker = OilTanker::findById($authenticationUserResume->oil_tanker_id);
                if (!$oilTanker) {
                    $oilTanker = OilTanker::find()->one();
                }
                $advanceOilTanker = AdvanceOilTanker::findById($authenticationUserResume->advance_oil_tanker_id);
                if (!$advanceOilTanker) {
                    $advanceOilTanker = AdvanceOilTanker::find()->one();
                }
                $chemicalTanker = ChemicalTanker::findById($authenticationUserResume->chemical_tanker_id);
                if (!$chemicalTanker) {
                    $chemicalTanker = ChemicalTanker::find()->one();
                }
                $gasTanker = GasTanker::findById($authenticationUserResume->gas_tanker_id);
                if (!$gasTanker) {
                    $gasTanker = GasTanker::find()->one();
                }
                $advanceGasTanker = AdvanceGasTanker::findById($authenticationUserResume->advance_gas_tanker_id);
                if (!$advanceGasTanker) {
                    $advanceGasTanker = AdvanceGasTanker::find()->one();
                }
                $crudeOilWashing = CrudeOilWashing::findById($authenticationUserResume->crude_oil_washing_id);
                if (!$crudeOilWashing) {
                    $crudeOilWashing = CrudeOilWashing::find()->one();
                }
                $inertGasPlant = InertGasPlant::findById($authenticationUserResume->inert_gas_plant_id);
                if (!$inertGasPlant) {
                    $inertGasPlant = InertGasPlant::find()->one();
                }
                $ismCode = IsmCode::findById($authenticationUserResume->ism_code_id);
                if (!$ismCode) {
                    $ismCode = IsmCode::find()->one();
                }
                $shipSecurityOfficer = ShipSecurityOfficer::findById($authenticationUserResume->ship_security_officer_id);
                if (!$shipSecurityOfficer) {
                    $shipSecurityOfficer = ShipSecurityOfficer::find()->one();
                }
                $shipSafetyOfficer = ShipSafetyOfficer::findById($authenticationUserResume->ship_safety_officer_id);
                if (!$shipSafetyOfficer) {
                    $shipSafetyOfficer = ShipSafetyOfficer::find()->one();
                }
                $bridgeTeamManagement = BridgeTeamManagement::findById($authenticationUserResume->bridge_team_management_id);
                if (!$bridgeTeamManagement) {
                    $bridgeTeamManagement = BridgeTeamManagement::find()->one();
                }
                $dpInduction = DpInduction::findById($authenticationUserResume->dp_induction_id);
                if (!$dpInduction) {
                    $dpInduction = DpInduction::find()->one();
                }
                $dpSimulator = DpSimulator::findById($authenticationUserResume->dp_simulator_id);
                if (!$dpSimulator) {
                    $dpSimulator = DpSimulator::find()->one();
                }
                $bridgeEngineRoomResourceManagement = BridgeEngineRoomResourceManagement::findById($authenticationUserResume->bridge_engine_room_resource_management_id);
                if (!$bridgeEngineRoomResourceManagement) {
                    $bridgeEngineRoomResourceManagement = BridgeEngineRoomResourceManagement::find()->one();
                }
                $shipHandling = ShipHandling::findById($authenticationUserResume->ship_handling_id);
                if (!$shipHandling) {
                    $shipHandling = ShipHandling::find()->one();
                }
                $internalAuditorsCourse = InternalAuditorsCourse::findById($authenticationUserResume->internal_auditors_course_id);
                if (!$internalAuditorsCourse) {
                    $internalAuditorsCourse = InternalAuditorsCourse::find()->one();
                }
                $trainingForSeafarers = TrainingForSeafarers::findById($authenticationUserResume->training_for_seafarers_id);
                if (!$trainingForSeafarers) {
                    $trainingForSeafarers = TrainingForSeafarers::find()->one();
                }
                $securityAwareness = SecurityAwareness::findById($authenticationUserResume->security_awareness_id);
                if (!$securityAwareness) {
                    $securityAwareness = SecurityAwareness::find()->one();
                }
                $electicalElectronic = ElecticalElectronic::findById($authenticationUserResume->electical_electronic_id);
                if (!$electicalElectronic) {
                    $electicalElectronic = ElecticalElectronic::find()->one();
                }
                $courseIds = preg_replace('/^:|:$/', '', $authenticationUserResume->other_course_ids);
                $courseIds = explode('::', $courseIds);
                $courses = [];
                foreach ($courseIds as $courseId) {
                    $course = OtherCourse::findById($courseId);
                    if ($course) {
                        array_push($courses, $course);
                    }
                }

                $newBuilding = NewBuilding::findById($authenticationUserResume->new_building_id);
                if (!$newBuilding) {
                    $newBuilding = NewBuilding::find()->one();
                }
                $specialisedProjects = SpecialisedProjects::findById($authenticationUserResume->specialised_projects_id);
                if (!$specialisedProjects) {
                    $specialisedProjects = SpecialisedProjects::find()->one();
                }
                $specialTrades = SpecialTrades::findById($authenticationUserResume->special_trades_id);
                if (!$specialTrades) {
                    $specialTrades = SpecialTrades::find()->one();
                }
                $shoreExperience = ShoreExperience::findById($authenticationUserResume->shore_experience_id);
                if (!$shoreExperience) {
                    $shoreExperience = ShoreExperience::find()->one();
                }

                $serviceDetailsIds = preg_replace('/^:|:$/', '', $authenticationUserResume->service_details_ids);
                $serviceDetailsIds = explode('::', $serviceDetailsIds);
                $servicesDetails = [];
                foreach ($serviceDetailsIds as $serviceDetailsId) {
                    $serviceDetails = ServiceDetails::findById($serviceDetailsId);
                    if ($serviceDetails) {
                        array_push($servicesDetails, $serviceDetails);
                    }
                }

                $referenceDetailsIds = preg_replace('/^:|:$/', '', $authenticationUserResume->reference_details_ids);
                $referenceDetailsIds = explode('::', $referenceDetailsIds);
                $referencesDetails = [];
                foreach ($serviceDetailsIds as $referenceDetailsId) {
                    $referenceDetails = ReferenceDetails::findById($referenceDetailsId);
                    if ($referenceDetails) {
                        array_push($referencesDetails, $referenceDetails);
                    }
                }

                return $this->render('pdf', [
                        'resume' => $authenticationUserResume,
                        'travelpassport' => $travelpassport,
                        'seamanbook' => $seamanbook,
                        'usvisa' => $usvisa,
                        'othervisas' => $othervisas,
                        'otherseamanbooks' => $otherseamanbooks,
                        'petroleum' => $petroleum,
                        'chemical' => $chemical,
                        'gas' => $gas,
                        'certificates' => $certificates,
                        'certificateInternational' => $certificateInternational,
                        'certificateLiberian' => $certificateLiberian,
                        'certificateNorwegian' => $certificateNorwegian,
                        'certificatePanamanian' => $certificatePanamanian,
                        'certificateYellowFever' => $certificateYellowFever,
                        'certificateHealthList' => $certificateHealthList,
                        'certificateDrugTest' => $certificateDrugTest,
                        'trainingInstructions' => $trainingInstructions,
                        'basicFireFighting' => $basicFireFighting,
                        'advFireFighting' => $advFireFighting,
                        'elementaryFirstAid' => $elementaryFirstAid,
                        'medicalFirstAid' => $medicalFirstAid,
                        'medicalCare' => $medicalCare,
                        'persSafetyResp' => $persSafetyResp,
                        'craftRescue' => $craftRescue,
                        'fastRescueCraft' => $fastRescueCraft,
                        'gmdsss' => $gmdsss,
                        'managementLevel' => $managementLevel,
                        'ecdis' => $ecdis,
                        'radarObservation' => $radarObservation,
                        'hazmat' => $hazmat,
                        'oilTanker' => $oilTanker,
                        'advanceOilTanker' => $advanceOilTanker,
                        'chemicalTanker' => $chemicalTanker,
                        'gasTanker' => $gasTanker,
                        'advanceGasTanker' => $advanceGasTanker,
                        'crudeOilWashing' => $crudeOilWashing,
                        'inertGasPlant' => $inertGasPlant,
                        'ismCode' => $ismCode,
                        'shipSecurityOfficer' => $shipSecurityOfficer,
                        'shipSafetyOfficer' => $shipSafetyOfficer,
                        'bridgeTeamManagement' => $bridgeTeamManagement,
                        'dpInduction' => $dpInduction,
                        'dpSimulator' => $dpSimulator,
                        'bridgeEngineRoomResourceManagement' => $bridgeEngineRoomResourceManagement,
                        'shipHandling' => $shipHandling,
                        'internalAuditorsCourse' => $internalAuditorsCourse,
                        'trainingForSeafarers' => $trainingForSeafarers,
                        'securityAwareness' => $securityAwareness,
                        'electicalElectronic' => $electicalElectronic,
                        'courses' => $courses,
                        'newBuilding' => $newBuilding,
                        'specialisedProjects' => $specialisedProjects,
                        'specialTrades' => $specialTrades,
                        'shoreExperience' => $shoreExperience,
                        'servicesDetails' => $servicesDetails,
                        'referencesDetails' => $referencesDetails
                ]);
            }
        //}
    }
}
