<?php

/* @var $this \yii\web\View */
/* @var $content string */

use yii\helpers\Html;
use yii\bootstrap\Nav;
use yii\bootstrap\NavBar;
use yii\widgets\Breadcrumbs;
use backend\assets\AppAsset;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="content-type" content="text/html;" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
    <link href="https://fonts.googleapis.com/css?family=Roboto:400,500,700,900" rel="stylesheet">
</head>
<body>
<?php $this->beginBody() ?>


    <div class="navbar" id="navbar">
        <div class="navbar-content">
            <div class="name">
                <h3><a href="http://crewmsg.com" target="_blank">Crew<span>MSG</span></a></h3>
            </div>
            <div class="nav" id="mynav">
              <a id="nav-companies" href="#offer-container" class="menu_item menu_item_first">
                <i class="icon icon-Settings"></i>
                <p class="lang" key="en_for_companies" >Settings</p>
              </a>
                
                <?php if (Yii::$app->user->isGuest): ?>
                    <?= 
                        Html::tag('a', '<p class="lang" key="en_contacts">
                                            Sign Up
                                        </p>', 
                        ['id' => 'nav-contacts', 'class' => 'menu_item', 'href' => '/site/signup']) 
                    ?>
                    <?= 
                        Html::tag('a', '<p class="lang" key="en_contacts">
                                            <i class="icon icon-Exit"></i>
                                            Login
                                        </p>', 
                        ['id' => 'nav-contacts', 'class' => 'menu_item', 'href' => '/site/login']) 
                    ?>
                <?php else: ?>
                    <?= 
                        Html::tag('a', '<p class="lang" key="en_contacts">
                                            <i class="icon icon-Exit"></i>
                                            Exit
                                        </p>', 
                        ['id' => 'nav-contacts', 'class' => 'menu_item', 'href' => '/admin/site/logout']) 
                    ?>
                <?php endif; ?>
              </a>
            </div>
        </div>
    </div>

    <?= $content ?>

<footer class="footer">
    <!--<div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>-->
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
