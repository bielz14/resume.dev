<?php

/* @var $this yii\web\View */

$this->title = 'My Yii Application';
?>
<div class="site-index">
    <div class="body-content">
        <div class="row" style="margin-top: 2.5%">
        	<?php if (isset($resumes) && count($resumes)): ?>
        	<h2>Список резюме</h2>           
		    <table class="table" style="margin-top: 2.5%">
		      <thead>
		        <tr>
		          <th>Firstname</th>
		          <th>Surname</th>
		          <th></th>
		        </tr>
		      </thead>
		      <tbody>
		      	<?php foreach ($resumes as $resume): ?>
		      		<tr>
			          <td><?= $resume->firstname ?></td>
			          <td><?= $resume->surname ?></td>
			          <td>
            			<a href="/admin/site/updateresume?id=<?= $resume->id ?>" style="color: blue">
            				Редактировать
            			</a>
			          </td>
			        </tr>
            	<?php endforeach; ?>
		      </tbody>
		    </table>
            <?php endif; ?>        	
        </div>

    </div>
</div>
