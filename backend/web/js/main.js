$(document).ready(function(){
	$('.add_new_language').on('click', function(event){
		event.preventDefault();
		$('.tbhold:last').parent().append(
			'<tr class="tbhold">' + 
				$('.tbhold:last').parent().children().last().html()
			+ '</tr>'
		);

		$('.tbhold:last > td > div > input').val(null);
		$('.tbhold:last > td:last > div > select').val(null);

		var elem = $('.tbhold:last').parent().children().last().prev();
		if(elem !== undefined && elem !== '') {
	        $('html').animate({ 
	            scrollTop: $(elem).offset().top
	        }, 500);
	    }
	});

	$('.remove_new_language').on('click', function(event){
		event.preventDefault();
		$('.tbhold').parent().children().last().remove();
	});






	$('.add_new_seaman_book').on('click', function(event){
		event.preventDefault();
		var prefixInputName = '';
		if ($('.seaman_book_row').length) {
			prefixInputName = 'other_otherseamanbook_';
		}
		$('#documents').append(
			'<tr class="seaman_book_row">' +
			    '<td>Other seaman book</td>' +
			    '<td>' +
			    	'<div class="form-group field-otherseamanbook-id">' +
						'<input type="hidden" id="otherseamanbook-id" class="form-control" name="other_otherseamanbook_id[]" value="5">' +
						'<p class="help-block help-block-error"></p>' +
					'</div>' +
			        '<div class="form-group field-otherseamanbook-document_no">' +
			            '<input type="text" class="input" name="' + prefixInputName + 'document_no[]" maxlength="20" minlength="2">' +
			            '<p class="help-block help-block-error"></p>' +
			        '</div>' +
			    '</td>' +
			    '<td>' +
			        '<div class="form-group field-otherseamanbook-iss_date">' +
			            '<input type="text" class="input" name="' + prefixInputName + '_iss_date[]" maxlength="20" minlength="2">' +
			            '<p class="help-block help-block-error"></p>' +
			        '</div>' +
			    '</td>' +
			    '<td>' +
			        '<div class="form-group field-otherseamanbook-exp_date">' +
			            '<input type="text" class="input" name="' + prefixInputName + 'exp_date[]" maxlength="20" minlength="2">' +
			            '<p class="help-block help-block-error"></p>' +
			        '</div>' +
			    '</td>' +
			    '<td>' +
			        '<div class="form-group field-otherseamanbook-iss_by">' +
			            '<input type="text" class="input" name="' + prefixInputName + 'iss_by[]" maxlength="20" minlength="2">' +
			            '<p class="help-block help-block-error"></p>' +
			        '</div>' +
			    '</td>' +
			    '<td>' +
			        '<div class="form-group field-otherseamanbook-place_of_issue">' +
			            '<input type="text" class="input" name="' + prefixInputName + 'place_of_issue[]" maxlength="20" minlength="2">' +
			            '<p class="help-block help-block-error"></p>' +
			        '</div>' +
			    '</td>' +
			+ '</tr>'
		);
		var elem = $('.seaman_book_row').parent().children().last().prev();
		if(elem !== undefined && elem !== '') {
	        $('html').animate({ 
	            scrollTop: $(elem).offset().top
	        }, 500);
	    }
	});

	$('.remove_new_seaman_book').on('click', function(event){
		event.preventDefault();
		$('.seaman_book_row').parent().children().last().remove();
	});




	$('.add_new_certificate').on('click', function(event){
		event.preventDefault();
		newContent = '<tr class="certificate_row">' +
						    '<td>' +
						    	/*'<div class="form-group field-certificate-id">' +
									'<input type="hidden" id="certificate-id" class="form-control" name="other_certificate_id[]">' +
									'<p class="help-block help-block-error"></p>' +
								'</div>' +*/
						        '<div class="form-group field-certificateform-other_certificate_name">' +
						            '<input type="text" id="certificateform-other_certificate_name" class="input" name="other_certificate_name[]" maxlength="5" minlength="2">' +
						            '<p class="help-block help-block-error"></p>' +
						        '</div>' +
						    '</td>' +
						    '<td>' +
						        '<div class="form-group field-certificateform-other_certificate_number">' +
						            '<input type="text" id="certificateform-other_certificate_number" class="input" name="other_certificate_number[]" maxlength="5" minlength="2">' +
						            '<p class="help-block help-block-error"></p>' +
						        '</div>' +
						    '</td>' +
						    '<td>' +
						        '<div class="form-group field-certificateform-other_certificate_iss_date">' +
						            '<input type="date" id="certificateform-other_certificate_iss_date" class="input" name="other_certificate_iss_date[]">' +
						            '<p class="help-block help-block-error"></p>' +
						        '</div>' +
						    '</td>' +
						    '<td>' +
						        '<div class="form-group field-certificateform-other_certificate_exp_date">' +
						            '<input type="date" id="certificateform-other_certificate_exp_date" class="input" name="other_certificate_exp_date[]">' +
						            '<p class="help-block help-block-error"></p>' +
						        '</div>' +
						    '</td>' +
						    '<td>' +
						        '<div class="form-group field-certificateform-other_certificate_iss_by">' +
						            '<input type="text" id="certificateform-other_certificate_iss_by" class="input" name="other_certificate_iss_by[]" maxlength="40" minlength="2">' +
						            '<p class="help-block help-block-error"></p>' +
						        '</div>' +
						    '</td>' +
						'</tr>';

		if (!$('.certificate_row').parent().children().last().html()) {
			newContent = '<tr class="th">' +
						    '<td>Certificate name</td>' +
						    '<td>Number</td>' +
						    '<td>Issue date</td>' +
						    '<td>Expiry date</td>' +
						    '<td>Issued by (authority)</td>' +
						'</tr>' +
						newContent;
		}

		$('#certificate-table > tbody:last').append(newContent);

		var elem = $('.certificate_row').parent().children().last().prev();
		if(elem !== undefined && elem !== '') {
	        $('html').animate({ 
	            scrollTop: $(elem).offset().top
	        }, 500);
	    }
	});

	$('.remove_new_certificate').on('click', function(event){
		event.preventDefault();
		$('.certificate_row').parent().children().last().remove();
	});





	$('.add_new_course').on('click', function(event){
		event.preventDefault();
		var prefixInputName = '';

		var quantity = $('.course_row').length + 1;
		newContent = '<tr class="course_row">' +
					    '<td>Other course №' + quantity + '</td>' +
					    '<td>' +
					        /*'<div class="form-group field-electicalelectronic-id">' +
					            '<input type="hidden" id="electicalelectronic-id" class="form-control" name="id">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +*/
					        '<div class="form-group field-electicalelectronic-number">' +
					            '<input type="text" id="electicalelectronic-number" class="input" name="other_course_number[]" maxlength="10" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-electicalelectronic-iss_date">' +
					            '<input type="date" id="electicalelectronic-iss_date" class="input" name="other_course_iss_date[]">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-electicalelectronic-exp_date">' +
					            '<input type="date" id="electicalelectronic-exp_date" class="input" name="other_course_exp_date[]">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-electicalelectronic-iss_by">' +
					            '<input type="text" id="electicalelectronic-iss_by" class="input" name="other_course_iss_by[]" maxlength="30" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					'</tr>';

		$('#course-table > tbody:last').append(newContent);

		var elem = $('.course_row').parent().children().last().prev();

		if(elem !== undefined && elem !== '') {
		    $('html').animate({ 
		        scrollTop: $(elem).offset().top
		    }, 500);
		}

	});

	$('.remove_new_course').on('click', function(event){
		event.preventDefault();
		$('.course_row').parent().children().last().remove();
	});



	$('.add_new_service_details').on('click', function(event){
		event.preventDefault();
		newContent = '<tr class="complete_row">' +   
						'<td>' +
					        '<div class="form-group field-servicedetails-id">' +
					            '<input type="hidden" id="servicedetails-id" class="form-control" name="service_details_id[]">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					        '<div class="form-group field-servicedetails-company_name">' +
					            '<input type="text" id="servicedetails-company_name" class="input" name="service_details_company_name[]" maxlength="30" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-servicedetails-rank">' +
					            '<select id="servicedetails-rank" class="input require" name="rank[]" required="">' +
					                '<optgroup label="Catering">' +
					                    '<option value="0">2nd Cook</option>' +
					                    '<option value="1">Camp Boss</option>' +
					                    '<option value="2">Chief Cook</option>' +
					                    '<option value="3">Chief Steward</option>' +
					                    '<option value="4">Cook</option>' +
					                    '<option value="5" selected="">Cook Assistant</option>' +
					                    '<option value="6">Laundress</option>' +
					                    '<option value="7">Messman</option>' +
					                    '<option value="8">Stewardess</option>' +
					                '</optgroup>' +
					                '<optgroup label="Cruise Ships Positions">' +
					                    '<option value="9">Activities Supervisor</option>' +
					                    '<option value="10">Art Auctioneer</option>' +
					                    '<option value="11">Assistant Cook</option>' +
					                    '<option value="12">Assistant Hotel Manager</option>' +
					                    '<option value="13">Baker</option>' +
					                    '<option value="14">Bar Manager</option>' +
					                    '<option value="15">Bartender</option>' +
					                    '<option value="16">Barwaiteress</option>' +
					                    '<option value="17">Buffet attendant</option>' +
					                    '<option value="18">Cabin Steward assistant</option>' +
					                    '<option value="19">Cabin Stewardess</option>' +
					                    '<option value="20">Chef de Cuisine</option>' +
					                    '<option value="21">Chef de Rang</option>' +
					                    '<option value="22">Cleaner</option>' +
					                    '<option value="23">Cook Trainee</option>' +
					                    '<option value="24">Deckhand</option>' +
					                    '<option value="25">Dining Room Head Waiter</option>' +
					                    '<option value="26">Dishwasher</option>' +
					                    '<option value="27">First Cook</option>' +
					                    '<option value="28">Galley Cleaner</option>' +
					                    '<option value="29">General Cook</option>' +
					                    '<option value="30">Head Waiter</option>' +
					                    '<option value="31">Hotel Engineer</option>' +
					                    '<option value="32">Hotel Manager</option>' +
					                    '<option value="33">Laundry Man</option>' +
					                    '<option value="34">Medical Officer</option>' +
					                    '<option value="35">Mess Boy</option>' +
					                    '<option value="36">Pastry Man</option>' +
					                    '<option value="37">Photographer</option>' +
					                    '<option value="38">Purser</option>' +
					                    '<option value="39">Receptionist</option>' +
					                    '<option value="40">Restaurant Steward</option>' +
					                    '<option value="41">Second Cook</option>' +
					                    '<option value="42">Shop staff</option>' +
					                    '<option value="43">Shops Salesperson</option>' +
					                    '<option value="44">Sommelier</option>' +
					                    '<option value="45">Staff Engineer</option>' +
					                    '<option value="46">Steward</option>' +
					                    '<option value="47">Third Cook</option>' +
					                    '<option value="48">Training and Development Manager</option>' +
					                    '<option value="49">Utility</option>' +
					                    '<option value="50">Waiter</option>' +
					                    '<option value="51">Waitress</option>' +
					                    '<option value="52">Wine Steward</option>' +
					                    '<option value="53">Аdministrator</option>' +
					                '</optgroup>' +
					                '<optgroup label=" Deck">' +
					                    '<option value="54">Master</option>' +
					                    '<option value="55">1st Officer</option>' +
					                    '<option value="56">2nd Officer</option>' +
					                    '<option value="57">3rd Officer</option>' +
					                    '<option value="58">AB / Crane Operator</option>' +
					                    '<option value="59">AB / Welder</option>' +
					                    '<option value="60">AB Seaman</option>' +
					                    '<option value="61">AB-Cook</option>' +
					                    '<option value="62">Anchor Foreman</option>' +
					                    '<option value="63">Barge Master</option>' +
					                    '<option value="64">Boatswain</option>' +
					                    '<option value="65">Carpenter</option>' +
					                    '<option value="66">Chief Officer</option>' +
					                    '<option value="67">Crane Operator</option>' +
					                    '<option value="68">Deck Cadet</option>' +
					                    '<option value="69">Deck Fitter</option>' +
					                    '<option value="70">DP Officer</option>' +
					                    '<option value="71">Fish-Processor</option>' +
					                    '<option value="72">Gangway operator</option>' +
					                    '<option value="73">Junior DP Officer</option>' +
					                    '<option value="74">Junior officer</option>' +
					                    '<option value="75">Navigation Officer</option>' +
					                    '<option value="76">OIM</option>' +
					                    '<option value="77">OS</option>' +
					                    '<option value="78">OS / Welder</option>' +
					                    '<option value="79">OS-Cook</option>' +
					                    '<option value="80">Painter</option>' +
					                    '<option value="81">Pilot</option>' +
					                    '<option value="82">Radio Officer</option>' +
					                    '<option value="83">Safety Officer</option>' +
					                    '<option value="84">Senior DP Engineer</option>' +
					                    '<option value="85">Senior DPO</option>' +
					                    '<option value="86">Staff Captain</option>' +
					                    '<option value="87">Trainee Officer</option>' +
					                    '<option value="88">Trawler</option>' +
					                    '<option value="89">Watchkeeping Officer</option>' +
					                    '<option value="90">Yacht Master</option>' +
					                '</optgroup>' +
					                '<optgroup label=" Engine">' +
					                    '<option value="91">1st Engineer</option>' +
					                    '<option value="92">2nd Electrical Engineer</option>' +
					                    '<option value="93">2nd Engineer</option>' +
					                    '<option value="94">2nd ETO</option>' +
					                    '<option value="95">3rd Electrical Engineer</option>' +
					                    '<option value="96">3rd Engineer</option>' +
					                    '<option value="97">4th Engineer</option>' +
					                    '<option value="98">AB / Motorman</option>' +
					                    '<option value="99">Barge Engineer</option>' +
					                    '<option value="100">Cargo Engineer</option>' +
					                    '<option value="101">Chief Electrical Engineer</option>' +
					                    '<option value="102">Chief Engineer</option>' +
					                    '<option value="103">El. Eng. Assistant</option>' +
					                    '<option value="104">El. Engineer</option>' +
					                    '<option value="105">El.Eng / 3rd Engineer</option>' +
					                    '<option value="106">Electric Cadet</option>' +
					                    '<option value="107">Electrician</option>' +
					                    '<option value="108">Engine Cadet</option>' +
					                    '<option value="109">Engineer Assistant</option>' +
					                    '<option value="110">ETO</option>' +
					                    '<option value="111">Fitter / Welder</option>' +
					                    '<option value="112">Gas Engineer</option>' +
					                    '<option value="113">Mechanic</option>' +
					                    '<option value="114">Motorman</option>' +
					                    '<option value="115">Motorman-electrician</option>' +
					                    '<option value="116">Motorman-welder</option>' +
					                    '<option value="117">Oiler</option>' +
					                    '<option value="118">OOW Engine</option>' +
					                    '<option value="119">Plumber</option>' +
					                    '<option value="120">Pumpman</option>' +
					                    '<option value="121">Ref. Engineer</option>' +
					                    '<option value="122">Ref. Machinist</option>' +
					                    '<option value="123">Single Engineer</option>' +
					                    '<option value="124">Staff Chief Engineer</option>' +
					                    '<option value="125">Storekeeper</option>' +
					                    '<option value="126">Trainee Engineer</option>' +
					                    '<option value="127">Turner</option>' +
					                    '<option value="128">Wiper</option>' +
					                '</optgroup>' +
					                '<optgroup label="Rigs / FPSO / other units">' +
					                    '<option value="129">Ballast Control Operator</option>' +
					                    '<option value="130">Cargo Operator</option>' +
					                    '<option value="131">Cargo Superintendent</option>' +
					                    '<option value="132">Cargo Supervisor</option>' +
					                    '<option value="133">Control Room Operator</option>' +
					                    '<option value="134">Diver</option>' +
					                    '<option value="135">Elect &amp; Instr Supervisor</option>' +
					                    '<option value="136">Electrical Technician</option>' +
					                    '<option value="137">Engine Room Supervisor</option>' +
					                    '<option value="138">Foreman</option>' +
					                    '<option value="139">Instrument Technician</option>' +
					                    '<option value="140">Maintenance Engineer</option>' +
					                    '<option value="141">Maintenance Operator</option>' +
					                    '<option value="142">Maintenance Superintendent</option>' +
					                    '<option value="143">Maintenance Supervisor</option>' +
					                    '<option value="144">Mechanical Supervisor</option>' +
					                    '<option value="145">Mechanical Technician</option>' +
					                    '<option value="146">Plant Operator</option>' +
					                    '<option value="147">Production Engineer</option>' +
					                    '<option value="148">Production Operator</option>' +
					                    '<option value="149">Production Technician</option>' +
					                    '<option value="150">Roustabout</option>' +
					                    '<option value="151">Superintendant</option>' +
					                '</optgroup>' +
					                '<optgroup label=" Safety / Security / Medical">' +
					                    '<option value="152">Doctor</option>' +
					                    '<option value="153">HSE Manager</option>' +
					                    '<option value="154">Security Staff</option>' +
					                    '<option value="155">Ship Security Officer</option>' +
					                '</optgroup>' +
					                '<optgroup label="Shore Positions">' +
					                    '<option value="156">Auditor</option>' +
					                    '<option value="157">Electrical inspector</option>' +
					                    '<option value="158">Engineer</option>' +
					                    '<option value="159">Health and Safety Officer</option>' +
					                    '<option value="160">Mechanical Foreman</option>' +
					                    '<option value="161">Motorist</option>' +
					                    '<option value="162">Safety and Operations Manager</option>' +
					                    '<option value="163">Sandblaster</option>' +
					                    '<option value="164">Senior Crewing Executive</option>' +
					                    '<option value="165">Service Technician</option>' +
					                    '<option value="166">Technical Superintendent</option>' +
					                    '<option value="167">Training and Competency Coordinator</option>' +
					                    '<option value="168">Welder</option>' +
					                '</optgroup>' +
					            '</select>' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-servicedetails-id">' +
					            '<input type="hidden" id="servicedetails-id" class="form-control" name="id[]">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					        '<div class="form-group field-servicedetails-vessel_name">' +
					            '<input type="text" id="servicedetails-vessel_name" class="input" name="vessel_name[]" maxlength="30" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-servicedetails-signed_on">' +
					            '<input type="text" id="servicedetails-signed_on" class="input" name="signed_on[]" maxlength="30" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-servicedetails-signed_off">' +
					            '<input type="text" id="servicedetails-signed_off" class="input" name="signed_off[]" maxlength="30" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-servicedetails-period_in_months">' +
					            '<input type="text" id="servicedetails-period_in_months" class="input" name="period_in_months[]" maxlength="30" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-servicedetails-type_of_vessel">' +
					            '<input type="text" id="servicedetails-type_of_vessel" class="input" name="type_of_vessel[]" maxlength="30" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-servicedetails-dwt">' +
					            '<input type="text" id="servicedetails-dwt" class="input" name="dwt[]" maxlength="30" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-servicedetails-engine_type">' +
					            '<input type="text" id="servicedetails-engine_type" class="input" name="engine_type[]" maxlength="30" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-servicedetails-bhp">' +
					            '<input type="text" id="servicedetails-bhp" class="input" name="bhp[]" value="" maxlength="30" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-servicedetails-kw">' +
					            '<input type="text" id="servicedetails-kw" class="input" name="kw[]" maxlength="30" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					'</tr>';

		if (!$('.complete_row').parent().children().last().html()) {
			newContent = '<tr class="th">' +
						    '<td>Company name</td>' +
						    '<td>Rank</td>' +
						    '<td>Vessel name (flag)</td>' +
						    '<td>Signed on</td>' +
						    '<td>Signed off</td>' +
						    '<td>Period in months (eg 4.2)</td>' +
						    '<td>Type of vessel</td>' +
						    '<td>D.W.T.</td>' +
						    '<td>Engine type (engineers only)</td>' +
						    '<td>BHP</td>' +
						    '<td>KW</td>' +
						'</tr>' +
						newContent;
		}

		$('#table-service_details > tbody:last-child').append(newContent);

		var elem = $('.complete_row').parent().children().last().prev();
		if(elem !== undefined && elem !== '') {
	        $('html').animate({ 
	            scrollTop: $(elem).offset().top
	        }, 500);
	    }
	});

	$('.remove_new_service_details').on('click', function(event){
		event.preventDefault();
		$('.complete_row').parent().children().last().remove();
	});




		$('.add_new_reference_details').on('click', function(event){
		event.preventDefault();
		newContent = '<tr>' +
					    '<td>' +
					        '<div class="form-group field-referencedetails-id">' +
					            '<input type="hidden" id="referencedetails-id" class="form-control" name="reference_details_id[]">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					        '<div class="form-group field-referencedetails-company_name">' +
					            '<input type="text" id="referencedetails-company_name" class="input" name="reference_details_company_name[]" maxlength="60" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-referencedetails-address">' +
					            '<input type="text" id="referencedetails-address" class="input" name="address[]" maxlength="60" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-referencedetails-phone_no">' +
					            '<input type="text" id="referencedetails-phone_no" class="input" name="phone_no[]" maxlength="15" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-referencedetails-email">' +
					            '<input type="email" id="referencedetails-email" class="input" name="email[]" maxlength="35" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					    '<td>' +
					        '<div class="form-group field-referencedetails-contact_person">' +
					            '<input type="text" id="referencedetails-contact_person" class="input" name="contact_person[]" maxlength="25" minlength="2">' +
					            '<p class="help-block help-block-error"></p>' +
					        '</div>' +
					    '</td>' +
					'</tr>';

		if (!$('.reference_details_row').parent().children().last().html()) {
			newContent = '<tr class="th">' +
						    '<td>Company name</td>' +
						    '<td>Address</td>' +
						    '<td>Phone No.</td>' +
						    '<td>E-mail</td>' +
						    '<td>Contact person</td>' +
						'</tr>';
						newContent;
		}

		$('#table-reference_details > tbody:last-child').append(newContent);

		var elem = $('.reference_details_row').parent().children().last().prev();
		if(elem !== undefined && elem !== '') {
	        $('html').animate({ 
	            scrollTop: $(elem).offset().top
	        }, 500);
	    }
	});

	$('.remove_new_reference_details').on('click', function(event){
		event.preventDefault();
		$('.reference_details_row').parent().children().last().remove();
	});




	$('#linkUplphoto').on('click', function(e) {
		$("#upload-photo").trigger('click');
	});

	$("#upload-photo").on('change', function(){
		previewFile();
	});

	function previewFile() {
	    var file = document.querySelector('input[type=file]').files[0];
	    var reader = new FileReader();

	   	if (file) {
	        reader.readAsDataURL(file);
	    }

	    var img = document.createElement('img');
		img.style.height = '100%';
		img.style.width = '100%';
		img.style.display = 'none';

        reader.onloadend = function () {
            img.src = reader.result;
            $('.avatar-box').append(img);
            if (img.src) {
            	$('.avatar-box').css('background', 'url(' + img.src + ')no-repeat center bottom / cover');
            } else {
            	$('.avatar-box').css('background', 'url(../images/user.png)no-repeat center bottom / cover');
            }
        }
	}

	$('#btn-download-pdf').on('click', function(event){
		event.preventDefault();
		//var pageHTML = document.documentElement.outerHTML;
		var id = decodeURIComponent(window.location.search.match(/(\?|&)id\=([^&]*)/)[2]);
		$.ajax({
            type: "POST",
            url: "/admin/site/getpdf?id=" + id,
            data: {'<?= Yii::$app->request->csrfParam ?>': '<?= \yii::$app->request->csrfToken ?>'}, 
            success: function(data) {
            	if (data) {
            		var link = document.createElement('a');
				    link.href = '/backend/web/pdf/' + data;
				    link.download = data;
				    link.click();
				    alert('Successful creating resume.');
            	}
            },  
            error: function(xhr, str) {
                
            }
        });
	});
});